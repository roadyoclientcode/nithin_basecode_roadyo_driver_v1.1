//
//  MannualInvoiceView.h
//  Roadyo
//
//  Created by Rahul Sharma on 12/07/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol MannualInvoiceViewDelegate <NSObject>
-(void)doneButtonSelectWithTag;

@end
@interface MannualInvoiceView : UIView 

@property (weak, nonatomic) id<MannualInvoiceViewDelegate>delegate;

@property (weak, nonatomic) IBOutlet UILabel *pickUpLocationLbl;
@property (weak, nonatomic) IBOutlet UILabel *dropOffLocationLbl;
@property (weak, nonatomic) IBOutlet UILabel *distanceLbl;
@property (weak, nonatomic) IBOutlet UILabel *durationLbl;
@property (weak, nonatomic) IBOutlet UILabel *totalAmtLbl;
@property(strong, nonatomic)IBOutlet UIView *contentView;
@property(nonatomic,strong) NSString *pickupAddressString;
@property(nonatomic,strong) NSString *dropOffAddressString;
@property(nonatomic,strong) NSString *TipLblString;
@property(nonatomic,strong) NSString *durationString;
@property(nonatomic,strong) NSString *distanceString;
@property(nonatomic,strong) NSString *amountString;
@property(nonatomic,strong) NSString *baseFeeString;
@property(nonatomic,strong) NSString *distanceFeeString;
@property(nonatomic,strong) NSString *airportFeeString;
@property(nonatomic,strong) NSString *timeFeeString;
@property (strong, nonatomic) UIWindow *currentWindow;
@property (strong,nonatomic) NSString *discountString;
@property (weak, nonatomic) IBOutlet UIButton *doneBtnClick;
@property (weak, nonatomic) IBOutlet UILabel *meterAmtLbl;
@property (weak, nonatomic) IBOutlet UILabel *airportFeeLbl;
@property (assign,nonatomic) BOOL  isPopUpOPen;

@property (weak, nonatomic) IBOutlet UILabel *timeFeeLbl;
@property (weak, nonatomic) IBOutlet UILabel *baseFee;
@property (weak, nonatomic) IBOutlet UILabel *basefeeAmtLbl;
@property (weak, nonatomic) IBOutlet UILabel *distanceFeeLabel;
@property (weak, nonatomic) IBOutlet UILabel *tipLbl;
@property (weak, nonatomic) IBOutlet UILabel *bidLbl;
@property(nonatomic,strong) NSString *bidString;

-(void)onWindow:(UIWindow *)onWindow
    pickAddress:(NSString*)Pick
    dropAddress:(NSString*)drop commingFromMannual:(BOOL)mannual;
@property (weak, nonatomic) IBOutlet UITableView *fareTableView;
@property (weak, nonatomic) IBOutlet UILabel *timeFeeLabel;
@property (weak, nonatomic) IBOutlet UILabel *airportFeeLabel;
@property (weak, nonatomic) IBOutlet UILabel *tiplabel;
@property (weak, nonatomic) IBOutlet UILabel *totalFeeLabel;

@end
