//
//  CustomNavigationBar.m
//  privMD
//
//  Created by Surender Rathore on 15/04/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "CustomNavigationBar.h"
#import "Helper.h"
#import "Fonts.h"

@interface CustomNavigationBar()


@end
@implementation CustomNavigationBar
@synthesize labelTitle;
@synthesize rightbarButton;
@synthesize delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        if(IS_IOS7)
        {
            [self setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:APP_NAVIGATION_BAR]]];
        }
        else
        {
            [self setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"login_navigationbr.png"]]];
        }
        
        //title
        labelTitle = [[UILabel alloc] initWithFrame:CGRectMake(0,20, 320, 40)];
        labelTitle.textAlignment = NSTextAlignmentCenter;
        [Helper setToLabel:labelTitle Text:@"SUPPORT" WithFont:Robot_Regular FSize:16 Color:[UIColor whiteColor]];
        [self addSubview:labelTitle];
        
        
    
        
        _leftbarButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _leftbarButton.frame = CGRectMake(5,20,44,44);
//        [_leftbarButton setTitle:@"MENU" forState:UIControlStateNormal];
//        [_leftbarButton setTitle:@"MENU" forState:UIControlStateSelected];
        [_leftbarButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
        [_leftbarButton setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateHighlighted];
        _leftbarButton.titleLabel.font = [UIFont fontWithName:Robot_Light size:11];
        UIImage *buttonImage = [UIImage imageNamed:@"signup_btn_back_bg_on.png"];
        [_leftbarButton setBackgroundImage:[UIImage imageNamed:@"appclndrscreen_menu_icon_off.png"] forState:UIControlStateNormal];
        [_leftbarButton addTarget:self action:@selector(leftBarButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_leftbarButton];
        
        
        //rightbutton
      //  UIImage *buttonImage = [UIImage imageNamed:@"signup_btn_back_bg_on.png"];
        //rightbutton
        rightbarButton = [UIButton buttonWithType:UIButtonTypeCustom];
        rightbarButton.frame = CGRectMake(220, 20,100, buttonImage.size.height);
        rightbarButton.titleLabel.font = [UIFont fontWithName:Robot_Light size:11];
//        [rightbarButton setTitle:@"SCHEDULE" forState:UIControlStateNormal];
//        [rightbarButton setTitle:@"SCHEDULE" forState:UIControlStateSelected];
        [rightbarButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
        [rightbarButton setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateSelected];
        [rightbarButton setBackgroundImage:buttonImage forState:UIControlStateHighlighted];
        rightbarButton.tag = 100;
        [rightbarButton addTarget:self action:@selector(rightBarButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:rightbarButton];
        [rightbarButton setHidden:isLaterBookingHidden];
        
        
//        self.layer.shadowColor = [UIColor blackColor].CGColor;
//        self.layer.shadowOpacity = 1;
//        
//        self.layer.shadowOffset = CGSizeMake(0,0);
//        CGRect shadowPath = CGRectMake(self.layer.bounds.origin.x - 10, self.layer.bounds.size.height+1, self.layer.bounds.size.width + 20, 1.3);
//        self.layer.shadowPath = [UIBezierPath bezierPathWithRect:shadowPath].CGPath;
//        self.layer.shouldRasterize = YES;
        
    }
    return self;
}

-(void)addButtonCenter {
    
    _centerButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _centerButton.frame = CGRectMake(self.frame.size.width/2 - 20, 20, 40, 40);
    
    [_centerButton addTarget:self action:@selector(centerButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_centerButton];

    
}


-(void)setTitle:(NSString*)title{
    labelTitle.text = title;
    //[self setNeedsDisplay];
}
-(void)setRightBarButtonTitle:(NSString*)title{
    [rightbarButton setTitle:title forState:UIControlStateNormal];
    //[self setNeedsDisplay];
}
-(void)setLeftBarButtonTitle:(NSString*)title{
    [_leftbarButton setTitle:title forState:UIControlStateNormal];
    
    //[self setNeedsDisplay];
}
-(void)rightBarButtonClicked:(UIButton*)sender{
    if (delegate && [delegate respondsToSelector:@selector(rightBarButtonClicked:)]) {
        [delegate rightBarButtonClicked:sender];
    }
}
-(void)leftBarButtonClicked:(UIButton*)sender {
    if (delegate && [delegate respondsToSelector:@selector(leftBarButtonClicked:)]) {
        [delegate leftBarButtonClicked:sender];
    }
}
-(void)centerButtonClicked: (UIButton*)sender {
    if (delegate && [delegate respondsToSelector:@selector(centerButtonClicked:)]) {
        [delegate centerButtonClicked:sender];
    }
    
}
-(void)rightBarButtonEnabled:(BOOL)isRightBarButtonEnabled {
    [rightbarButton setEnabled:isRightBarButtonEnabled];
    if (isRightBarButtonEnabled == YES) {
        [rightbarButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }else {
        [rightbarButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    }
}
-(void)leftBarButtonEnabled:(BOOL)isLeftBarButtonEnabled {
    [_leftbarButton setEnabled:isLeftBarButtonEnabled];
}
@end
