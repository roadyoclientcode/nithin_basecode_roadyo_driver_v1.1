//
//  acceptRejectView.h
//  Roadyo
//
//  Created by Rahul Sharma on 22/04/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PICircularProgressView.h"
#import <AVFoundation/AVFoundation.h>

@protocol DismissDelegate <NSObject>

-(void)didButtonSelectWithTag:(NSInteger)StatusTag;
@optional
-(void)PopUpDismiss;
-(void)didTimerExpire;
@end

@interface acceptRejectView : UIView


@property (weak, nonatomic) id<DismissDelegate>delegate;


-(void) onWindow:(UIWindow *)onWindow;

-(IBAction)acceptBtn:(id)sender;
-(IBAction)rejectBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *bookingIdLbl;
@property (weak, nonatomic) IBOutlet UILabel *bookinhgDateLbl;
@property (weak, nonatomic) IBOutlet UILabel *timeLbl;
@property (weak, nonatomic) IBOutlet UILabel *pickUpAddressLbl;
@property (strong, nonatomic) IBOutlet PICircularProgressView *progressView;
@property (weak, nonatomic) IBOutlet UILabel *timerLbl;
@property(strong,nonatomic)NSTimer * timer;
@property(strong, nonatomic)IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UILabel *BookingID;
@property (weak, nonatomic) IBOutlet UILabel *PickupLocationAddress;
@property (weak, nonatomic) IBOutlet UILabel *bookingDate;
@property (weak, nonatomic) IBOutlet UILabel *BookingTime;
@property (assign, nonatomic) float expireTime;
@property (assign, nonatomic) int remainingCounts;
-(IBAction)tapGestureAction:(id)sender;
-(void)showPopUpWithDetailedDict:(NSDictionary *)dict Onwindow:(UIWindow *)window;
@property (strong, nonatomic) IBOutlet UILabel *timeLeft;



@end
