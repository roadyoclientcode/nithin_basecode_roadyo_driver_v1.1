//
//  NewBookingHistoryViewController.h
//  Roadyo
//
//  Created by Rahul Sharma on 2/7/15.
//  Copyright (c) 2015 3Embed. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AXRatingView/AXRatingView.h>

@interface NewBookingHistoryViewController : UIViewController<UITextFieldDelegate,UIGestureRecognizerDelegate>
@property (strong, nonatomic) NSMutableDictionary *passDetails;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic) IBOutlet UILabel *systemCalculatedFare;
@property (strong, nonatomic) IBOutlet UILabel *labelDistance;
@property (strong, nonatomic) IBOutlet UILabel *labelDuration;
@property (strong, nonatomic) IBOutlet UILabel *labelWaitingTime;

@property (weak, nonatomic) IBOutlet UILabel *surgePriceLbl;



@property (strong, nonatomic) IBOutlet UIView *viewPickup;
@property (strong, nonatomic) IBOutlet UILabel *labelPickupTime;
@property (strong, nonatomic) IBOutlet UIView *viewPickupAddress;
@property (strong, nonatomic) IBOutlet UILabel *labelPickupAddress;



@property (strong, nonatomic) IBOutlet UIView *viewDropOff;
@property (strong, nonatomic) IBOutlet UILabel *labelDropOffTime;
@property (strong, nonatomic) IBOutlet UIView *viewDropOffAddress;
@property (strong, nonatomic) IBOutlet UILabel *labelDropOffAddress;

@property (strong, nonatomic) IBOutlet UIView *viewFareDetails;

@property (strong, nonatomic) IBOutlet UIView *viewRateYourCustomer;
@property (strong, nonatomic) IBOutlet UIView *ratingView;

@property (strong, nonatomic) IBOutlet AXRatingView *ratingStars;

@property(nonatomic,strong)AXRatingView *ratingViewStars;



@property (strong, nonatomic) IBOutlet UITextField *meterCharges;
@property (strong, nonatomic) IBOutlet UITextField *parkingCharges;
@property (strong, nonatomic) IBOutlet UITextField *airportCharges;
@property (strong, nonatomic) IBOutlet UITextField *tollCharges;
@property (strong, nonatomic) IBOutlet UILabel *totalCharges;

@property (strong, nonatomic) IBOutlet UILabel *currencyLabel1;

@property (strong, nonatomic) IBOutlet UILabel *currencyLabel2;
@property (strong, nonatomic) IBOutlet UILabel *currencyLabel3;
@property (strong, nonatomic) IBOutlet UILabel *currencyLabel4;

@property (strong, nonatomic) IBOutlet UILabel *tipLabel;
@property (strong, nonatomic) IBOutlet UILabel *totalNewLabel;
@property (strong, nonatomic) IBOutlet UILabel *subTotalLabel;
@property (strong, nonatomic) IBOutlet UILabel *subTotalPrice;
@property (strong, nonatomic) IBOutlet UILabel *tipPriceLabel;
@property (strong, nonatomic) IBOutlet UILabel *totalNewPrice;

@property (strong, nonatomic) IBOutlet UITextField *tipCharges;
@property (strong, nonatomic) IBOutlet UILabel *totalChargesNew;
@property (strong, nonatomic) IBOutlet UILabel *currentLabel5;


@end
