//
//  HelpViewController.m
//  privMD
//
//  Created by Rahul Sharma on 11/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "HelpViewController.h"
#import "SignInViewController.h"
#import "SignUpViewController.h"
#import "Helper.h"
#import "Fonts.h"
#import "LocationServicesViewController.h"

//#import <Canvas/CSAnimationView.h>

@interface HelpViewController ()

@end

@implementation HelpViewController
@synthesize bottomContainerView;
@synthesize signInBtn;
@synthesize registerBtn;
@synthesize mainScrollView;
@synthesize pageControl;
@synthesize orLbl;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    
    //[[UIApplication sharedApplication] setStatusBarHidden:YES];
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    
    //[self createScrollingView];
    [self.view bringSubviewToFront:bottomContainerView];
    [self.view bringSubviewToFront:pageControl];
    
    bottomContainerView.frame = CGRectMake(0,568, 320,65);
    [self.view bringSubviewToFront:bottomContainerView];
    // [self.view bringSubviewToFront:middleView];
    
    [Helper setButton:signInBtn Text:NSLocalizedString(@"LOGIN", @"LOGIN") WithFont:Robot_Regular FSize:15 TitleColor:UIColorFromRGB(0xfdd210) ShadowColor:nil];
    signInBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    [signInBtn setTitleColor:UIColorFromRGB(0x000000) forState:UIControlStateHighlighted];
    signInBtn.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    [signInBtn setBackgroundImage:[UIImage imageNamed:@"register_on"] forState:UIControlStateHighlighted];
    
    
    [Helper setButton:registerBtn Text:NSLocalizedString(@"REGISTER", @"REGISTER") WithFont:Robot_Regular FSize:15 TitleColor:UIColorFromRGB(0x000000) ShadowColor:nil];
    [registerBtn setTitleColor:UIColorFromRGB(0x666666) forState:UIControlStateHighlighted];
    registerBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    registerBtn.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    [registerBtn setBackgroundImage:[UIImage imageNamed:@"register_on"] forState:UIControlStateHighlighted];
    [self.view bringSubviewToFront:_swpLabel];
    [Helper setToLabel:_swpLabel Text:NSLocalizedString(@"swipe up", @"swipe up") WithFont:Robot_Regular FSize:14 Color:UIColorFromRGB(0xffffff)];
}

-(void)viewDidAppear:(BOOL)animated;
{
    [self startAnimationUp];
}

-(void)startAnimationDown
{
    [UIView animateWithDuration:0.1
                          delay:0.0
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^
     {
         CGRect frame = bottomContainerView.frame;
         frame.origin.y = [[UIScreen mainScreen] bounds].size.height;
         frame.origin.x = 0;
         bottomContainerView.frame = frame;
     }
                     completion:^(BOOL finished)
     {
         NSLog(@"Completed");
         
     }];
}
-(void)startAnimationUp
{
    
    bottomContainerView.frame = CGRectMake(0, [[UIScreen mainScreen] bounds].size.height, 320, 65);

    [UIView animateWithDuration:0.1
                          delay:0.0
                        options: UIViewAnimationOptionCurveEaseOut
                     animations:^
     {
         CGRect frame = bottomContainerView.frame;
         frame.origin.y = [[UIScreen mainScreen] bounds].size.height-65;
         frame.origin.x = 0;
         bottomContainerView.frame = frame;
     }
    completion:^(BOOL finished)
     {
         NSLog(@"Completed");
         
     }];
}


#pragma mark-scrollView delegate

//- (void)scrollViewDidScroll:(UIScrollView *)sender
//{
//    CGFloat pageHeight = self.mainScrollView.frame.size.height;
//    // you need to have a **iVar** with getter for scrollView
//    
//    float fractionalPage = self.mainScrollView.contentOffset.y / pageHeight;
//    NSInteger page = lround(fractionalPage);
//    self.pageControl.currentPage = page;
//}

- (void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = YES;
    self.navigationItem.hidesBackButton = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)signInButtonClicked:(id)sender {
    [self startAnimationDown];
    [self performSegueWithIdentifier:@"SignIn" sender:self];
}

//- (IBAction)pageControllerButtonClicked:(id)sender {
//    NSLog(@"pageControl position %ld", (long)[self.pageControl currentPage]);
//    //Call to remove the current view off to the left
//    unsigned long int page = self.pageControl.currentPage;
//    
//    CGRect frame = mainScrollView.frame;
//    frame.origin.x = 0;
//    frame.origin.y = frame.size.width * page;
//    [mainScrollView scrollRectToVisible:frame animated:YES];
//}


- (IBAction)registerButtonClicked:(id)sender {
    [self startAnimationDown];
    [self performSegueWithIdentifier:@"SignUp" sender:self];
}


/*
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"SignIn"])
    {
        SignInViewController *SVC = segue.destinationViewController;
        
        SVC =[segue destinationViewController];

    }
    else
    {
        SignUpViewController *SVC = segue.destinationViewController;
        
        SVC =[segue destinationViewController];
    }
    
}
*/
-(void)callPush
{
   // LoginViewController *LVC=[[LoginViewController alloc] init];
    [UIView  beginAnimations:nil context:NULL];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:1.0];
  //  [self.navigationController pushViewController:LVC animated:NO];
    [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:self.navigationController.view cache:NO];
    [UIView commitAnimations];
}
//   Call Pop

-(void)callPop
{
    [UIView  beginAnimations:nil context:NULL];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:0.75];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:self.navigationController.view cache:NO];
    [UIView commitAnimations];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelay:0.375];
    [self.navigationController popViewControllerAnimated:NO];
    [UIView commitAnimations];
}

@end
