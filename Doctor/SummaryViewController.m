//
//  SummaryViewController.m
//  Roadyo
//
//  Created by Rahul Sharma on 05/04/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

#import "SummaryViewController.h"
#import "XDKAirMenuController.h"
#import "CustomNavigationBar.h"
#import "HomeViewController.h"
#import "User.h"
#import "LocationTracker.h"
#import "SplashViewController.h"
#import "Helper.h"


@interface SummaryViewController ()<CustomNavigationBarDelegate,UITableViewDataSource,UITableViewDelegate,UserDelegate,UserDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITableView *tableView1;
@property (weak, nonatomic) IBOutlet UIView *scrolViewContentView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIButton *TripsButtonButton;
@property (weak, nonatomic) IBOutlet UIButton *PAymentsLocsButton;
@property (strong, nonatomic) NSDictionary *TripDetails;

@end

@implementation SummaryViewController
@synthesize TripsButtonButton;
@synthesize PAymentsLocsButton;
@synthesize TripDetails;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    PAymentsLocsButton.selected = YES;
    [self sendRequestForMasterTripDetails];
    // Do any additional setup after loading the view.
    _tableView.backgroundColor = UIColorFromRGB(0xe9e9e8);
    self.view.backgroundColor = UIColorFromRGB(0xe9e9e8);
    //    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
    
    //    [self.view addGestureRecognizer:tapGesture];
    [self addCustomNavigationBar];
    //    self.tableView1.tag = 100;
    //    self.tableView.tag = 200;
    [self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width *2, self.scrollView.frame.size.height)];
}
-(void)leftBarButtonClicked:(UIButton *)sender{
    [self menuButtonPressedAccount];
}

/**
 *  Handle Current Button Action
 *
 *  @param sender Button Sender
 */
- (IBAction)PAymentsLocsButtonAction:(id)sender
{
    CGRect frame = self.scrollView.bounds;
    frame.origin.x = 0;
    [self.scrollView scrollRectToVisible:frame animated:YES];
    
    PAymentsLocsButton.selected = YES;
    TripsButtonButton.selected = NO;
}
/**
 *  Handle Past Button Action
 *
 *  @param sender Button Sender
 */
- (IBAction)TripsButtonAction:(id)sender
{
    CGRect frame = self.scrollView.bounds;
    frame.origin.x = self.view.frame.size.width;
    [self.scrollView scrollRectToVisible:frame animated:YES];
    
    PAymentsLocsButton.selected = NO;
    TripsButtonButton.selected = YES;
}
/*---------------------------------*/
#pragma mark - Scrollview Delegate
/*---------------------------------*/
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if ([scrollView isEqual:self.scrollView]) {
        
        CGPoint tableScrollPosition = [scrollView.panGestureRecognizer velocityInView:self.scrollView];
        CGPoint offset = scrollView.contentOffset;
        
        float widthOfView = self.view.frame.size.width;
        
        if (tableScrollPosition.x > 0.0f)
        {
            if (offset.x < widthOfView/2)
            {
                [self PAymentsLocsButtonAction:nil];
            }
        }
        else if (tableScrollPosition.x < 0.0f)
        {
            if (offset.x > widthOfView/2)
            {
                [self TripsButtonAction:nil];
            }
        }
        CGFloat minOffsetX = 0;
        CGFloat maxOffsetX = widthOfView;
        
        // Check if current offset is within limit and adjust if it is not
        if (offset.x < minOffsetX) offset.x = minOffsetX;
        if (offset.x > maxOffsetX) offset.x = maxOffsetX;
        
        // Set offset to adjusted value
        scrollView.contentOffset = offset;
        
    }
}
#pragma mark ButtonAction Methods -

- (void)menuButtonPressedAccount
{
    XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
    
    if (menu.isMenuOpened)
        [menu closeMenuAnimated];
    else
        [menu openMenuAnimated];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void) addCustomNavigationBar
{
    CustomNavigationBar *customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, 320, 64)];
    customNavigationBarView.tag = 78;
    customNavigationBarView.delegate = self;
    [customNavigationBarView setTitle:NSLocalizedString(@"ACCOUNTS", @"ACCOUNTS")];
    // [customNavigationBarView setRightBarButtonTitle:@"Edit"];
    // [customNavigationBarView setRightBarButtonTitle:@""];
    [self.view addSubview:customNavigationBarView];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
//    SplashViewController *SVC = [segue destinationViewController];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.tableView1) {
        if (section == 0)
        {
            return 6;
        }
        else if(section == 1)
        {
            return 2;
        }
        
    }
    else
    {
        if (section == 0) {
            return 3;
        }
        else if(section == 1){
            return 3;
        }
        else if (section == 2){
            return 3;
        }
    }
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    if (tableView == self.tableView1) {
        return 2;
    }
    else
    {
        return 3;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.tableView1){
        static NSString *CellIdentifier = @"Cell";
        UITableViewCell *cell = nil;//[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            UIImageView *line = [[UIImageView alloc] initWithFrame:CGRectMake(0, 39, 320, 1)];
            line.image = [UIImage imageNamed:@"signup_bg_namedetails_divider"];
            line.tag = 200;
            [cell.contentView addSubview:line];
        }
        UILabel *textField = [[UILabel alloc] initWithFrame:CGRectMake(200, 5, 200, 30)];
        textField.textAlignment = NSTextAlignmentLeft;
        textField.font = [UIFont fontWithName:lato_regular_webfont size:12];
        [textField setTintColor:[UIColor blueColor]];
        [cell.contentView addSubview:textField];
        UILabel *labelHelperText = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, 150, 30)];
        [Helper setToLabel:labelHelperText Text:@"" WithFont:lato_regular_webfont FSize:9 Color:UIColorFromRGB(0x5d5d5d)];
        [cell.contentView addSubview:labelHelperText];
        
        
        if (indexPath.section == 0)
        {
            if (indexPath.row == 0)
            {
                textField.tag = 202;
                labelHelperText.text = NSLocalizedString(@"Cash Earnings", @" Cash Earnings");
                textField.text = [NSString stringWithFormat:@"%@ %.02f",[Helper getCurrencyUnit],[TripDetails[@"paymentLoc"][@"DriverCashEarning"]doubleValue]];
            }
            if (indexPath.row == 1)
            {
                textField.tag = 203;
                labelHelperText.text = NSLocalizedString(@"Card Earnings", @"Card Earnings");
                textField.text = [NSString stringWithFormat:@"%@ %.02f",[Helper getCurrencyUnit],[TripDetails[@"paymentLoc"][@"DriverCardEarning"]doubleValue]];
                
                
            }
            if (indexPath.row == 2) {
                
                labelHelperText.text = NSLocalizedString(@"Total Earnings", @"Total Earnings");
                textField.text = [NSString stringWithFormat:@"%@ %.02f",[Helper getCurrencyUnit],[TripDetails[@"paymentLoc"][@"DriverTotalEarning"]doubleValue]];
                
            }
            if (indexPath.row == 3) {
                labelHelperText.text = NSLocalizedString(@"Cash Collected", @"Cash Collected");
                textField.text =[NSString stringWithFormat:@"%@ %.02f", [Helper getCurrencyUnit],[TripDetails[@"paymentLoc"][@"DriverCashColleted"] doubleValue]];
            }
            if (indexPath.row == 4) {
                
                textField.tag = 203;
                labelHelperText.text = NSLocalizedString(@"Total Settled", @"Total Settled");
                textField.text =[NSString stringWithFormat:@"%@ %.02f", [Helper getCurrencyUnit],[TripDetails[@"paymentLoc"][@"TotalReceived"] doubleValue]];
                
            }
            if (indexPath.row == 5) {
                
                labelHelperText.text = NSLocalizedString(@"BALANCE", @"BALANCE");
                textField.text = [NSString stringWithFormat:@"%@ %.02f",[Helper getCurrencyUnit],[TripDetails[@"paymentLoc"][@"pending"]doubleValue]];
            }
        }
        else if (indexPath.section == 1){
            
            if (indexPath.row == 0) {
                labelHelperText.text = NSLocalizedString(@"Amount", @"Amount");
                textField.text =[NSString stringWithFormat:@"%@ %.02f", [Helper getCurrencyUnit],[TripDetails[@"paymentLoc"][@"LastPayAmount"] doubleValue]];
            }
            if (indexPath.row == 1) {
                labelHelperText.text = NSLocalizedString(@"Date", @"Date");
                textField.text =[self changeDateFormate:TripDetails[@"paymentLoc"][@"LastPayDate"]];// TripDetails[@"paymentLoc"][@"LastPayDate"];
            }
        }
        return cell;
    }
    else
    {
        static NSString *CellIdentifier = @"Cell1";
        UITableViewCell *cell1 = nil;//[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell1 == nil) {
            cell1 = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            
            
            UIImageView *line = [[UIImageView alloc] initWithFrame:CGRectMake(0, 39, 320, 1)];
            line.image = [UIImage imageNamed:@"signup_bg_namedetails_divider"];
            line.tag = 200;
            [cell1.contentView addSubview:line];
        }
        
        UILabel *textField = [[UILabel alloc] initWithFrame:CGRectMake(200, 5, 180, 30)];
        textField.textAlignment = NSTextAlignmentLeft;
        textField.font = [UIFont fontWithName:lato_regular_webfont size:12];
        [textField setTintColor:[UIColor blueColor]];
        [cell1.contentView addSubview:textField];
        
        
        
        UILabel *labelHelperText = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, 150, 30)];
        [Helper setToLabel:labelHelperText Text:@"" WithFont:lato_regular_webfont FSize:9 Color:UIColorFromRGB(0x5d5d5d)];
        [cell1.contentView addSubview:labelHelperText];
        
        if (indexPath.section == 0) {
            
            if (indexPath.row == 0)
            {
                labelHelperText.text = NSLocalizedString(@"Completed Trips", @"Completed Trips");
                textField.text = TripDetails[@"Trips"][@"cmpltApts_toady"];
                
            }
            if (indexPath.row == 1) {
                labelHelperText.text = NSLocalizedString(@"cancelled Trips", @"cancelled Trips");
                textField.text = TripDetails[@"Trips"][@"cancelled_toadys"];
            }
            if (indexPath.row == 2) {
                labelHelperText.text = NSLocalizedString(@"Today's Earnings", @"Today's Earnings");
                textField.text = [NSString stringWithFormat:@"%@ %.02f",[Helper getCurrencyUnit],[TripDetails[@"Trips"][@"toadys_earning"]doubleValue]];
            }
        }
        else if (indexPath.section == 1){
            
            if (indexPath.row == 0)
            {
                labelHelperText.text = NSLocalizedString(@"Completed Trips", @"Completed Trips");
                textField.text = TripDetails[@"Trips"][@"cmpltApts_week"];
                
            }
            if (indexPath.row == 1) {
                
                labelHelperText.text = NSLocalizedString(@"cancelled Trips", @"cancelled Trips");
                textField.text = TripDetails[@"Trips"][@"cancelled_week"];
                
            }
            if (indexPath.row == 2) {
                
                labelHelperText.text = NSLocalizedString(@"Weekly Earnings", @"Weekly Earnings");
                textField.text = [NSString stringWithFormat:@"%@ %.02f",[Helper getCurrencyUnit],[TripDetails[@"Trips"][@"week_earnings"]doubleValue]];
            }
        }
        else if (indexPath.section == 2){
            
            if (indexPath.row == 0)
            {
                labelHelperText.text = NSLocalizedString(@"Completed Trips", @"Completed Trips");
                textField.text = TripDetails[@"Trips"][@"cmpltApts_month"];
            }
            if (indexPath.row == 1) {
                
                labelHelperText.text = NSLocalizedString(@"cancelled Trips", @"cancelled Trips");
                textField.text = TripDetails[@"Trips"][@"canceld_month"];
                
                
            }
            if (indexPath.row == 2) {
                
                labelHelperText.text = NSLocalizedString(@"Monthly Earnings", @"Monthly Earnings");
                textField.text = [NSString stringWithFormat:@"%@ %.02f",[Helper getCurrencyUnit],[TripDetails[@"Trips"][@"month_earnings"]doubleValue]];
                
            }
        }
        
        return cell1;
    }
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    if (tableView == self.tableView1) {
        
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 30)];
        
        UILabel *labelTitle = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, 200, 30)];
        [Helper setToLabel:labelTitle Text:@"" WithFont:lato_bold FSize:12 Color:UIColorFromRGB(0x000000)];
        [headerView addSubview:labelTitle];
        
        if (section == 0) {
            [self addShadowToView:self.tableView1];
            labelTitle.text = NSLocalizedString(@"PENDING PAYMENT", @"PENDING PAYMENT");
        }
        else if(section == 1){
            [self addShadowToView:self.tableView1];
            labelTitle.text = NSLocalizedString(@"LAST PAYMENT", @"LAST PAYMENT");
        }
        return headerView;
    }
    else
    {
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 30)];
        
        UILabel *labelTitle = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, 200, 30)];
        [Helper setToLabel:labelTitle Text:@"" WithFont:lato_bold FSize:12 Color:UIColorFromRGB(0x000000)];
        [headerView addSubview:labelTitle];
        
        if (section == 0) {
            [self addShadowToView:self.tableView];
            labelTitle.text = NSLocalizedString(@"TODAY'S TRIPS", @"TODAY'S TRIPS");
        }
        else if(section == 1){
            labelTitle.text = NSLocalizedString(@"WEEKLY TRIPS", @"WEEKLY TRIPS");
        }
        else if(section == 2){
            labelTitle.text = NSLocalizedString(@"MONTHLY TRIPS", @"MONTHLY TRIPS");
        }
        return headerView;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell* tableViewCell = [tableView cellForRowAtIndexPath:indexPath];
    [tableViewCell setSelected:NO animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 30;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40;
}
-(void)userDidFailedToLogout:(NSError *)error {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    [[LocationTracker sharedInstance] stopLocationTracking];
    [[[XDKAirMenuController sharedMenu] view] removeFromSuperview];
    
    if ([XDKAirMenuController relese]) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                    @"Main" bundle:[NSBundle mainBundle]];
        
        SplashViewController *splah = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
        
        self.navigationController.viewControllers = [NSArray arrayWithObjects:splah, nil];
    }
}
-(void)accountDeactivated
{
    User *user = [User sharedInstance];
    user.delegate = self;
    [user logout];
}

-(void)sendRequestForMasterTripDetails {
    
    PMDReachabilityWrapper * reachability = [PMDReachabilityWrapper sharedInstance];
    if ([reachability isNetworkAvailable])
    {
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi showPIOnView:self.view withMessage:NSLocalizedString(@"Please wait..", @"Please wait..")];
    
    //setup parameters
    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    
    
    NSString  *deviceID = [[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey];
    NSString *currentDate = [Helper getCurrentDateTime];
    
    NSDictionary *params = @{@"ent_sess_token":sessionToken,
                             @"ent_dev_id":deviceID,
                             @"ent_date_time":currentDate,
                             };
    
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"getMasterTripDetails"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   if (!response) {
                                       return ;
                                   }
                                   if (success) { //handle success response
                                       [self parseMasterTripDetailsResponse:response];
                                   }
                                   else
                                   {
                                       [pi hideProgressIndicator];
                                   }
                               }];
    }
    else
    {
        ProgressIndicator * pi = [ProgressIndicator sharedInstance];
        [pi showMessage:kNetworkErrormessage On:self.view];
    }
}

-(void)parseMasterTripDetailsResponse:(NSDictionary *)response
{
    if (response == nil)
    {
        return;
    }
    else if ([response objectForKey:@"Error"])
    {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error") Message:[response objectForKey:@"errMsg"]];
    }
    else
    {
        if ([[response objectForKey:@"errFlag"] integerValue] == 0)
        {
            ProgressIndicator *pi = [ProgressIndicator sharedInstance];
            [pi hideProgressIndicator];
            [self.tableView reloadData];
            [self.tableView1 reloadData];
            TripDetails = response;
        }
    }
}

- (void) addShadowToView:(UIView *)view
{
    // border radius
    [view.layer setCornerRadius:10.0f];
    
    // border
    [view.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [view.layer setBorderWidth:1.0f];
    
    // drop shadow
    [view.layer setShadowColor:[UIColor blackColor].CGColor];
    [view.layer setShadowOpacity:0.5];
    [view.layer setShadowRadius:2.0];
    [view.layer setShadowOffset:CGSizeMake(1.0, 2.0)];
}
- (NSString *)changeDateFormate:(NSString *)originalDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];//2012-11-22 10:19:04
    
    NSDate *date = [dateFormatter dateFromString:originalDate];
    
    [dateFormatter setDateFormat:@"dd/MMM/yyyy"];//22-Nov-2012
    NSString *formattedDateString = [dateFormatter stringFromDate:date];
    
    NSLog(@"Date in new format : %@", formattedDateString);
    return formattedDateString;
}
@end
