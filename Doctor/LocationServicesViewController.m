    //
//  LocationServicesViewController.m
//  Roadyo
//
//  Created by Rahul Sharma on 5/12/15.
//  Copyright (c) 2015 3Embed. All rights reserved.
//

#import "LocationServicesViewController.h"

@interface LocationServicesViewController ()
@property (strong, nonatomic) IBOutlet UILabel *headerTitle;
@property (strong, nonatomic) IBOutlet UILabel *middleLabel;
@property (strong, nonatomic) IBOutlet UILabel *bottonLabel;

@end

@implementation LocationServicesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkLocationServices) name:@"LocationServices" object:nil];
    _headerTitle.text = [NSString stringWithFormat:@"\"%@\" Would Like to Use Your Current Location",APP_NAME];
    _middleLabel.text = [NSString stringWithFormat:@"%@ would like to use your current location so we can send you more accurate results.",APP_NAME];
    _bottonLabel.text = [NSString stringWithFormat:@"Settings -> Privacy ->  Location Services -> Enable Location Services for %@",APP_NAME];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)checkLocationServices {
    
    
    if([CLLocationManager locationServicesEnabled] && [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways)
    {

        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
