//
//  AboutViewController.m
//  Roadyo
//
//  Created by Rahul Sharma on 25/06/14.
//  Copyright (c) 2014 3Embed. All rights reserved.
//

#import "AboutViewController.h"
#import "XDKAirMenuController.h"
#import "CustomNavigationBar.h"
#import "SplashViewController.h"
#import "LocationTracker.h"
#import "User.h"

@interface AboutViewController () <CustomNavigationBarDelegate,UserDelegate>
@property (weak, nonatomic) IBOutlet UIButton *websiteButtonOutlet;

@end

@implementation AboutViewController
@synthesize topView;
@synthesize topLabel;
@synthesize bottomLabel;
@synthesize likeButton;
@synthesize rateButton;
@synthesize legalButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self addCustomNavigationBar];
}

-(void)viewWillAppear:(BOOL)animated
{
    
    [Helper setToLabel:topLabel Text:NSLocalizedString(@"'EVERYONE'S PRIVATE TAXI'", @"'EVERYONE'S PRIVATE TAXI'") WithFont:Robot_Regular FSize:13 Color:UIColorFromRGB(0x333333)];
    [Helper setToLabel:bottomLabel Text:APP_WEBSITE WithFont:Robot_Regular FSize:11 Color:UIColorFromRGB(0x3399ff)];
    [Helper setButton:rateButton Text:NSLocalizedString(@"RATE US IN THE APP STORE", @"RATE US IN THE APP STORE") WithFont:Robot_Regular FSize:15 TitleColor:UIColorFromRGB(0x000000) ShadowColor:nil];
    [_websiteButtonOutlet setTitle:APP_WEBSITE forState:UIControlStateNormal];
    rateButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    rateButton.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    [rateButton setTitleColor:UIColorFromRGB(0x666666) forState:UIControlStateHighlighted];
    // [rateButton setBackgroundImage:[UIImage imageNamed:@"about_cell_selector"] forState:UIControlStateHighlighted];
    
    [Helper setButton:likeButton Text:NSLocalizedString(@"LIKE US ON FACEBOOK", @"LIKE US ON FACEBOOK") WithFont:Robot_Regular FSize:15 TitleColor:UIColorFromRGB(0x333333) ShadowColor:nil];
    likeButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    likeButton.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    [likeButton setTitleColor:UIColorFromRGB(0x666666) forState:UIControlStateHighlighted];
    // [likeButton setBackgroundImage:[UIImage imageNamed:@"about_cell_selector"] forState:UIControlStateHighlighted];
    
    [Helper setButton:legalButton Text:NSLocalizedString(@"LEGAL", @"LEGAL") WithFont:Robot_Regular FSize:15 TitleColor:UIColorFromRGB(0x333333) ShadowColor:nil];
    legalButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    legalButton.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    [legalButton setTitleColor:UIColorFromRGB(0x666666) forState:UIControlStateHighlighted];
    // [legalButton setBackgroundImage:[UIImage imageNamed:@"about_cell_selector"] forState:UIControlStateHighlighted];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(accountDeactivated) name:@"accountDeactivated" object:nil];
    
}

-(void)viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
#pragma mark Custom Methods -

- (void) addCustomNavigationBar{
    
    
    CustomNavigationBar *customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, 320, 64)];
    customNavigationBarView.tag = 78;
    customNavigationBarView.delegate = self;
    [customNavigationBarView setTitle:NSLocalizedString(@"ABOUT", @"ABOUT")];
    [self.view addSubview:customNavigationBarView];
    
    
    
}
-(void)leftBarButtonClicked:(UIButton *)sender{
    [self menuButtonPressedAccount];
}

- (void)menuButtonPressedAccount
{
    XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
    
    if (menu.isMenuOpened)
        [menu closeMenuAnimated];
    else
        [menu openMenuAnimated];
}

- (IBAction)rateButtonClicked:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"itms-apps://%@",APP_DRIVER_ITUNES_LINK]]];
}
- (IBAction)likeonFBButtonClicked:(id)sender {
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:APP_FACEBOOK_PAGE]];
}

- (IBAction)legalButtonClicked:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:APP_PRIVACY_POLICY]];
}
- (IBAction)websiteButton:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@",APP_WEBSITE]]];
}


-(void)userDidLogoutSucessfully:(BOOL)sucess {
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    [[LocationTracker sharedInstance] stopLocationTracking];
    [[[XDKAirMenuController sharedMenu] view] removeFromSuperview];
    
    if ([XDKAirMenuController relese]) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                    @"Main" bundle:[NSBundle mainBundle]];
        
        SplashViewController *splah = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
        
        self.navigationController.viewControllers = [NSArray arrayWithObjects:splah, nil];
    }
}
-(void)userDidFailedToLogout:(NSError *)error {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    [[LocationTracker sharedInstance] stopLocationTracking];
    [[[XDKAirMenuController sharedMenu] view] removeFromSuperview];
    
    if ([XDKAirMenuController relese]) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                    @"Main" bundle:[NSBundle mainBundle]];
        
        SplashViewController *splah = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
        
        self.navigationController.viewControllers = [NSArray arrayWithObjects:splah, nil];
    }
}

-(void)accountDeactivated {
    
    User *user = [User sharedInstance];
    user.delegate = self;
    [user logout];
}



@end
