//
//  PriveMdAppDelegate.m
//  Doctor
//
//  Created by Rahul Sharma on 17/04/14.
//  Copyright (c) 2014 3Embed. All rights reserved.
//

#import "PriveMdAppDelegate.h"
#import "HelpViewController.h"
#import "SplashViewController.h"
#import "Reachability.h"
#import <Crashlytics/Crashlytics.h>
#import "MenuViewController.h"
#import "HomeViewController.h"
#import <AudioToolbox/AudioToolbox.h>
#import "NSCalendar+Ranges.h"
#import "VNHPubNubWrapper.h"
#import "CheckBookingStatus.h"
#import "PMDReachabilityWrapper.h"
#import "OnBookingViewController.h"
#import "UpdateBookingStatus.h"


@interface PriveMdAppDelegate()<VNHPubNubWrapperDelegate>

@property (nonatomic,strong) HelpViewController *helpViewController;
@property (nonatomic,strong) SplashViewController *splashViewController;
//@property (nonatomic,strong) MapViewController *mapViewContoller;

@property (nonatomic, strong) Reachability *hostReach;
@property (nonatomic, strong) Reachability *internetReach;
@property (nonatomic, strong) Reachability *wifiReach;

@end

@implementation PriveMdAppDelegate
@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
@synthesize hostReach;
@synthesize internetReach;
@synthesize wifiReach;


/**
 *  Navigation bar appearance
 */
-(void)customizeNavigationBar {
    
    
    if (IS_IOS7) {
        [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:APP_NAVIGATION_BAR] forBarMetrics:UIBarMetricsDefault];
    }
    else{
        [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"login_navigationbr"] forBarMetrics:UIBarMetricsDefault];
    }
    
    
    
    
    [[UINavigationBar appearance] setTitleTextAttributes:@{
                                                           UITextAttributeTextColor: [UIColor whiteColor],
                                                           UITextAttributeTextShadowColor: [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8],
                                                           UITextAttributeTextShadowOffset: [NSValue valueWithUIOffset:UIOffsetMake(0, 0)],
                                                           UITextAttributeFont: [UIFont fontWithName:Robot_Regular size:16.0],
                                                           }];
    
}

void uncaughtExceptionHandler(NSException *exception) {
    //[Flurry startSession:@"VX2Q9RPNNFQGCD5KDP86"];
    //[Flurry logError:@"Uncaught" message:@"Crash!" exception:exception];
    NSLog(@"The app has encountered an unhandled exception: %@", [exception debugDescription]);
    NSLog(@"Stack trace: %@", [exception callStackSymbols]);
    NSLog(@"desc: %@", [exception description]);
    NSLog(@"name: %@", [exception name]);
    NSLog(@"user info: %@", [exception userInfo]);
}
//use NSSetUncaughtExceptionHandler(&uncaughtExceptionHandler);

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else
    {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
    }
    NSSetUncaughtExceptionHandler(&uncaughtExceptionHandler);
    
    //LumberJack init
    [GMSServices provideAPIKey:kPMDGoogleMapsAPIKey];
    [[Crashlytics sharedInstance] setDebugMode:NO];
    [Crashlytics startWithAPIKey:kPMDCrashLyticsAPIKey];
    [self customizeNavigationBar];
    
    if(launchOptions)
    NSLog(@"Launch Options in didFinishLaunchWithOption:");
    [self handlePushNotificationWithLaunchOption:launchOptions];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
    [reachability monitorReachability];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 0];
    //    [[UIApplication sharedApplication] cancelAllLocalNotifications];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DismissNewBookingVC" object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"removePopUp" object:nil];

    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
    [reachability monitorReachability];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 1];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 0];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"LocationServices" object:nil];
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken]) {
        
        CheckBookingStatus *status = [[CheckBookingStatus alloc] init];
        [status checkOngoingAppointmentStatus:nil];
        status.callblock = ^(int status,NSDictionary *dictionary){
            if (status == 1) {
                
                NSString *email = dictionary[@"email"];
                NSString *appointmentDate = dictionary[@"apptDt"];
                NSString *nt = dictionary[@"nt"];
                if([nt length] == 0) {
                    nt = @"";
                }
                CGFloat pickLat = [dictionary[@"pickLat"] floatValue];
                CGFloat pickLong = [dictionary[@"pickLong"] floatValue];
                NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
                NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
                [dict setObject:email forKey:@"e"];
                [dict setObject:appointmentDate forKey:@"dt"];
                [dict setObject:nt forKey:@"nt"];
                [dict setObject:[NSString stringWithFormat:@"%.4f,%.4f",pickLat,pickLong] forKey:@"ltg"];
                [dict setObject:dictionary[@"bid"] forKey:@"bid"];
                [dict setObject:dictionary[@"addr1"] forKey:@"adr2"];
                [dict setObject:dictionary[@"pasChn"] forKey:@"chn"];
                [ud setObject:dict forKey:@"PUSH"];
                [ud synchronize];
                
                XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
                if (!menu.airDelegate) {
                    
                }
                else if (![menu.currentViewController isKindOfClass:[UINavigationController class]]) {
                    
                    NSIndexPath *indexpath = [NSIndexPath indexPathForRow:0 inSection:0];
                    
                    [menu openMenuAnimated];
                    
                    dispatch_after(0.2, dispatch_get_main_queue(), ^(void){
                        [menu openViewControllerAtIndexPath:indexpath];
                        [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(postNotificationForNewBooking:) userInfo:dict repeats:NO];
   
                    });
                    
                }
                else {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"DismissNewBookingVC" object:nil];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"NewBooking" object:nil userInfo:dict];
                }
            }
        };
    }
    
}

-(void)postNotificationForNewBooking:(NSTimer *)timer
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"NewBooking" object:nil userInfo:timer.userInfo];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    NSLog(@"user Info %@",userInfo);

    application.applicationIconBadgeNumber = 0;
    [self handleNotificationForUserInfo:userInfo];
}


-(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    
    NSLog(@"Local Notification: %@",notification.userInfo);
    NSDictionary *dict = notification.userInfo;
    if ([dict[@"a"] integerValue] == 11) {
        
        XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
        
        if (!menu.airDelegate) {
            
        }
        else if (![menu.currentViewController isKindOfClass:[UINavigationController class]]) {
            
            NSIndexPath *indexpath = [NSIndexPath indexPathForRow:0 inSection:0];
            [menu openMenuAnimated];
            
            dispatch_after(0.2, dispatch_get_main_queue(), ^(void){
                [menu openViewControllerAtIndexPath:indexpath];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"NewBooking" object:nil userInfo:dict];
                });
            });
        }
        else
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"NewBooking" object:nil userInfo:dict];
        }
    }
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    
    NSString *dt = [[deviceToken description] stringByTrimmingCharactersInSet:
                    [NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    dt = [dt stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSLog(@"My token is: %@", dt);
    
    NSString *model = [[UIDevice currentDevice] model];
    if ([model isEqualToString:@"iPhone Simulator"]) {
        //device is simulator
        [[NSUserDefaults standardUserDefaults]setObject:@"123" forKey:KDAgetPushToken];
    }
    else {
        [[NSUserDefaults standardUserDefaults]setObject:dt forKey:KDAgetPushToken];
    }
    
    //    [[PushNotificationManager pushManager] handlePushRegistration:deviceToken];
    
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    NSLog(@"Failed to get token, error: %@", error);
    NSString *model = [[UIDevice currentDevice] model];
    if ([model isEqualToString:@"iPhone Simulator"])
    {
        //device is simulator
        [[NSUserDefaults standardUserDefaults]setObject:@"123" forKey:KDAgetPushToken];
    }
    
    //    [[PushNotificationManager pushManager] handlePushRegistrationFailure:error];
}


//- (void) onPushAccepted:(PushNotificationManager *)pushManager withNotification:(NSDictionary *)pushNotification {
//    NSLog(@"Push notification received");
//}


#pragma mark -
#pragma mark Core Data stack
// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Model" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Model.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}


#pragma Reachability

- (BOOL)isNetworkAvailable {
    
    return self.networkStatus != NotReachable;
}
// Called by Reachability whenever status changes.
- (void)reachabilityChanged:(NSNotification* )note {
    
    Reachability *curReach = (Reachability *)[note object];
    
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    
    _networkStatus = [curReach currentReachabilityStatus];
    
    if (_networkStatus == NotReachable) {
        NSLog(@"Network not reachable.");
    }
    
}
- (void)monitorReachability {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
    
    self.hostReach = [Reachability reachabilityWithHostName:@"http://www.privemd.com"];
    [self.hostReach startNotifier];
    
    self.internetReach = [Reachability reachabilityForInternetConnection];
    [self.internetReach startNotifier];
    
    self.wifiReach = [Reachability reachabilityForLocalWiFi];
    [self.wifiReach startNotifier];
}
- (void)saveContext
{
    
}

#pragma mark - HandlePushNotification

-(void)playNotificationSound
{
    //play sound
    SystemSoundID	pewPewSound;
    NSString *pewPewPath = [[NSBundle mainBundle]
                            pathForResource:@"taxina" ofType:@"wav"];
    NSURL *pewPewURL = [NSURL fileURLWithPath:pewPewPath];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)pewPewURL, & pewPewSound);
    AudioServicesPlaySystemSound(pewPewSound);
}

/**
 *  handle push if app is opened by clicking push
 *
 *  @param launchOptions pushpayload dictionary
 */
-(void)handlePushNotificationWithLaunchOption:(NSDictionary*)launchOptions {
    
    NSLog(@"handle push %@",launchOptions);
    NSDictionary *remoteNotificationPayload = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if (remoteNotificationPayload) {
        
        if ([self checkIfBookingIsValid:remoteNotificationPayload[@"aps"][@"dt"]]) {
            NSLog(@"checkIfBookingIsValid...................%@",remoteNotificationPayload[@"aps"][@"dt"]);
            NSDictionary *dict = remoteNotificationPayload[@"aps"];
            [[NSUserDefaults standardUserDefaults] setObject:dict forKey:@"PUSH"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(postNotificationForBooking:) userInfo:dict repeats:NO];
        }
    }
}

/**
 *  Called when app is opened from push
 *
 *  @param userInfo message info
 */
-(void)handleNotificationForUserInfo:(NSDictionary*)userInfo{
    
    int type = [userInfo[@"aps"][@"nt"] intValue];
    NSLog(@"userinfoYO: %d", type);
    
    //new booking
    if (type == 7 || type == 51) {
        
        NSMutableDictionary *mdict = [[NSMutableDictionary alloc] init];
        [mdict setObject:userInfo[@"aps"][@"e"] forKey:@"e"];
        [mdict setObject:userInfo[@"aps"][@"dt"] forKey:@"dt"];
        [mdict setObject:userInfo[@"aps"][@"adr2"] forKey:@"adr2"];
        [mdict setObject:userInfo[@"aps"][@"bid"] forKey:@"bid"];
        [mdict setObject:userInfo[@"aps"][@"chn"] forKey:@"chn"];
        [mdict setObject:userInfo[@"aps"][@"ltg"] forKey:@"ltg"];
        
        if (userInfo[@"aps"][@"nt"]!= nil)
            [mdict setObject:userInfo[@"aps"][@"nt"] forKey:@"nt"];
        
        self.isNewBooking = YES;
        
        //booking expired
        XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
        if (!menu.airDelegate) {
            
        }
        else if (![menu.currentViewController isKindOfClass:[UINavigationController class]]) {
            
            NSIndexPath *indexpath = [NSIndexPath indexPathForRow:0 inSection:0];
            [menu openMenuAnimated];
            
            dispatch_after(0.2, dispatch_get_main_queue(), ^(void){
                [menu openViewControllerAtIndexPath:indexpath];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"DismissNewBookingVC" object:nil];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"NewBooking" object:nil userInfo:mdict];
                });
            });
        }
        else
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"DismissNewBookingVC" object:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"NewBooking" object:nil userInfo:mdict];
        }
    }
    
    //booking cancelled
    else if(type == 10)
    {
        
        UpdateBookingStatus *updateBookingStatus = [UpdateBookingStatus sharedInstance];
        [updateBookingStatus stopUpdatingStatus];
        
        UIStoryboard *mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UINavigationController *bookingDirectionVC = [mainstoryboard instantiateViewControllerWithIdentifier:@"ViewController1"];
        //bookingCancelled
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"bookingCancelled" object:nil userInfo:nil];
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:userInfo[@"aps"][@"alert"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//        [alert show];
    }
    // ??
    else if (type == 11)
    {
        NSLog(@"GOT TYPE 11");
        UIWindow *window = [[[UIApplication sharedApplication]delegate] window];
        for (UIView *subView in [window subviews]) {
            if ([subView isKindOfClass:[UILabel class]]) {
                [subView removeFromSuperview];
            }
        }
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:userInfo[@"aps"][@"alert"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        
    //account de-active
    }else if (type == 12)
    {
        [Helper showAlertWithTitle:@"Message" Message:userInfo[@"aps"][@"alert"]];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"accountDeactivated" object:nil];
    }
    else if (type == 51)
    {
        
    }
    
    //drop address changed
    else if (type == 22)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"AddressChanged" object:nil];
    }
    
    //super admin message to all the drivers
    else if (type == 420){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:userInfo[@"aps"][@"alert"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    
    // if no nt(empty) we just show the message
    else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:userInfo[@"aps"][@"alert"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
}

-(void)postNotificationForBooking:(NSTimer*)timer
{
    NSLog(@"userinfo %@",timer.userInfo);
    //[[NSNotificationCenter defaultCenter] postNotificationName:@"Match" object:timer.userInfo];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"NewBooking" object:nil userInfo:timer.userInfo];
}
-(BOOL)checkIfBookingIsValid:(NSString*)apptDate{
    
    NSDateFormatter *df = [self formatter];
    NSDate *notificationDate = [df dateFromString:apptDate];
    NSDate *currentDate = [NSDate date];
    NSCalendar *calender = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSInteger seconds  =  [calender secondsFromDate:notificationDate toDate:currentDate];
    
    if (seconds < 30) {
        return YES;
    }
    return NO;
}
- (NSDateFormatter *)formatter
{
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    });
    return formatter;
}

/**
 *  Subscribe of Pubnub channel from home view controller
 */
-(void)subscribeToPubnubChannel
{
    if (![[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken]) {
        return;
    }
    NSString  *_pubNubChannel = [[NSUserDefaults standardUserDefaults] objectForKey:kNSUDriverListnerChannelKey];
    VNHPubNubWrapper *pubNub = [VNHPubNubWrapper sharedInstance];
    [pubNub subscribeToChannels:@[_pubNubChannel]];
    [pubNub setDelegate:self];
}
-(void)checkDriverStatus{
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kNSURoadyoPubNubChannelkey]) {
        
        VNHPubNubWrapper *pubNub = [VNHPubNubWrapper sharedInstance];
        NSString  *_pubNubChannel = [[NSUserDefaults standardUserDefaults] objectForKey:kNSURoadyoPubNubChannelkey];
        NSString  *listnerChn = [[NSUserDefaults standardUserDefaults] objectForKey:kNSUDriverListnerChannelKey];
        
        NSString *driverEmail = [[NSUserDefaults standardUserDefaults] objectForKey:kNSUPatientEmailAddressKey];
        NSString *deviceUDID = [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey];
        
        NSDictionary *message = @{@"a":[NSNumber numberWithInt:kPubNubCheckDriverStatusAction],
                                  @"e_id": driverEmail,
                                  @"d_id":deviceUDID,
                                  @"chn":listnerChn,
                                  };
        [pubNub publishWithParameter:message toChannel:_pubNubChannel];
    }
}

#pragma mark - 3D touch delegates

- (void)application:(UIApplication *)application performActionForShortcutItem:(UIApplicationShortcutItem *)shortcutItem completionHandler:(void (^)(BOOL))completionHandler
{
    NSLog(@"%@", shortcutItem.type);
    
    if ([shortcutItem.type isEqualToString:@"com.test.static0"])
    {
        XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
        NSIndexPath *indexpath = [NSIndexPath indexPathForRow:1 inSection:0];
        [menu openMenuAnimated];
        dispatch_after(0.2, dispatch_get_main_queue(), ^(void){
            [menu openViewControllerAtIndexPath:indexpath];
        });
    }
    if ([shortcutItem.type isEqualToString:@"com.test.static1"])
    {
        XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
        NSIndexPath *indexpath = [NSIndexPath indexPathForRow:2 inSection:0];
        [menu openMenuAnimated];
        dispatch_after(0.2, dispatch_get_main_queue(), ^(void){
            [menu openViewControllerAtIndexPath:indexpath];
        });
    }
    if ([shortcutItem.type isEqualToString:@"com.test.static2"])
    {
        XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
        NSIndexPath *indexpath = [NSIndexPath indexPathForRow:0 inSection:0];
        [menu openMenuAnimated];
        dispatch_after(0.2, dispatch_get_main_queue(), ^(void){
            [menu openViewControllerAtIndexPath:indexpath];
        });
    }
}

#pragma mark - PubNubWrapperDelegate

/**
 *  Response from Pubnub
 *
 *  @param messageDict message info
 *  @param channelName channel on which the message is sent on
 */

-(void)presenceMessage:(PNPresenceDetailsData *)message andChannel:(NSString *)channel
{
    NSLog(@"Receive Presence.................%@",message);
}


-(void)recievedMessage:(NSDictionary*)messageDict onChannel:(NSString*)channelName;
{
    NSLog(@"Message : %@", messageDict);
    if ([messageDict[@"a"] integerValue] == 11) { // New Booking
        NSMutableDictionary *mdict = [[NSMutableDictionary alloc] init];
        [mdict setObject:messageDict[@"e"] forKey:@"e"];
        [mdict setObject:messageDict[@"dt"] forKey:@"dt"];
        if (messageDict[@"nt"]!= nil)
            [mdict setObject:messageDict[@"nt"] forKey:@"nt"];
        
        UIApplicationState state = [[UIApplication sharedApplication] applicationState];
        if (state == UIApplicationStateBackground || state == UIApplicationStateInactive)
        {
            //Do checking here.
            NSMutableDictionary *notificationDict = [[NSMutableDictionary alloc] init];
            [notificationDict setObject:mdict forKey:@"aps"];
            UILocalNotification* localNotification = [[UILocalNotification alloc] init];
            localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:1];
            localNotification.alertBody = @"Pubnub:Congratulations you got a new booking request";
            localNotification.userInfo = messageDict;
            localNotification.soundName = @"sms-received.wav";
            localNotification.timeZone = [NSTimeZone defaultTimeZone];
            [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
        }
        else {
            
            self.isNewBooking = YES;
            [self playNotificationSound];
            XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
            
            if(!menu.airDelegate)
            {
            }
            else if (![menu.currentViewController isKindOfClass:[UINavigationController class]]) {
                
                NSIndexPath *indexpath = [NSIndexPath indexPathForRow:0 inSection:0];
                [menu openMenuAnimated];
                
                dispatch_after(0.2, dispatch_get_main_queue(), ^(void){
                    [menu openViewControllerAtIndexPath:indexpath];
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"NewBooking" object:nil userInfo:messageDict];
                    });
                });
            }
            else
            {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"NewBooking" object:nil userInfo:messageDict];
            }
        }
    }
    else if ([messageDict[@"a"] integerValue] == 10)
    {
        UpdateBookingStatus *updateBookingStatus = [UpdateBookingStatus sharedInstance];
        [updateBookingStatus stopUpdatingStatus];
        //bookingCancelled
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"bookingCancelled" object:nil userInfo:nil];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"Passenger cancelled booking" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([messageDict[@"a"] integerValue] == 2){
        [[NSUserDefaults standardUserDefaults] setInteger:[messageDict[@"s"] integerValue] forKey:kNSUDriverStatekey];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"DriverState" object:nil userInfo:nil];
        
        NSLog(@"message %@",messageDict);
    }
    else if([messageDict[@"a"]integerValue] == 3){
        
        UpdateBookingStatus *updateBookingStatus = [UpdateBookingStatus sharedInstance];
        [updateBookingStatus stopUpdatingStatus];
    }
  }

@end
