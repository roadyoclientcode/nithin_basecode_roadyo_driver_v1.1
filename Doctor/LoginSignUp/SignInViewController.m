//
//  SignInViewController.m
//  privMD
//
//  Created by Rahul Sharma on 13/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "SignInViewController.h"
#import "UploadFiles.h"
#import "HomeViewController.h"
#import "MenuViewController.h"
#import "Helper.h"
#import "AppConstants.h"
#import "Service.h"
#import "WebServiceConstants.h"
#import "ProgressIndicator.h"
#import "Fonts.h"
#import "CustomNavigationBar.h"
#import "Login.h"
#import "Errorhandler.h"
#import "HLPlaceholder.h"

@interface SignInViewController ()<CustomNavigationBarDelegate,CLLocationManagerDelegate>
@property(nonatomic,assign)double currentLatitude;
@property(nonatomic,assign)double currentLongitude;
@property(nonatomic,strong)CLLocationManager *locationManager;
@end

@implementation SignInViewController
@synthesize emailTextField;
@synthesize passwordTextField;
@synthesize navDoneButton;
@synthesize emailImageView;
@synthesize signinButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)getCurrentLocation{
    
    //check location services is enabled
    if ([CLLocationManager locationServicesEnabled]) {
        
        if (!_locationManager) {
            _locationManager = [[CLLocationManager alloc] init];
            _locationManager.delegate = self;
            _locationManager.distanceFilter = kCLDistanceFilterNone;
            _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
            if ([_locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
                
                [_locationManager requestAlwaysAuthorization];
            }
        }
        [_locationManager startUpdatingLocation];
    }
    else {
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Location Service", @"Location Service") message:NSLocalizedString(@"Unable to find your location,Please enable location services.", @"Unable to find your location,Please enable location services.") delegate:nil cancelButtonTitle:NSLocalizedString(@"Ok", @"Ok") otherButtonTitles:nil, nil];
        [alertView show];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"login_bg.png"]];
    self.navigationItem.title = NSLocalizedString(@"SIGN IN", @"SIGN IN");
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.hidesBackButton = NO;
    emailTextField.placeholderColor = [UIColor lightGrayColor];
    passwordTextField.placeholderColor = [UIColor lightGrayColor];
    self.caridTextField.placeholderColor = [UIColor lightGrayColor];
    [self createNavLeftButton];
    
    [emailTextField becomeFirstResponder];
    _fNameLabel.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"login_textfield.png"]];
    
    _passwordLabel.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"login_textfield.png"]];
    [Helper setButton:signinButton Text:NSLocalizedString(@"SIGN IN", @"SIGN IN") WithFont:Robot_Regular FSize:15 TitleColor:UIColorFromRGB(0x333333) ShadowColor:nil];
    [signinButton setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateHighlighted];
    [signinButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
    [Helper setButton:_forgotPasswordButton Text:NSLocalizedString(@"Forgot password?", @"Forgot password?") WithFont:Robot_Light FSize:12 TitleColor:UIColorFromRGB(0x000000) ShadowColor:nil];
    [_forgotPasswordButton setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateNormal];
    [_forgotPasswordButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateHighlighted];
    [emailTextField setValue:UIColorFromRGB(0x999999)
                  forKeyPath:@"_placeholderLabel.textColor"];
    [passwordTextField setValue:UIColorFromRGB(0x999999)
     
                     forKeyPath:@"_placeholderLabel.textColor"];
    [_caridTextField setValue:UIColorFromRGB(0x999999)
                   forKeyPath:@"_placeholderLabel.textColor"];
    emailTextField.font = [UIFont fontWithName:Robot_CondensedRegular size:15];
    emailTextField.textColor = UIColorFromRGB(0x000000);
    passwordTextField.font = [UIFont fontWithName:Robot_CondensedRegular size:15];
    passwordTextField.textColor = UIColorFromRGB(0x000000);
    _caridTextField.font = [UIFont fontWithName:Robot_CondensedRegular size:15];
    _caridTextField.textColor = UIColorFromRGB(0x000000);
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tapGesture];
    CLLocationCoordinate2D coordinate = [self getLocation];
    _currentLongitude = coordinate.longitude;
    _currentLatitude = coordinate.latitude;
    
    
}
-(void)viewDidAppear:(BOOL)animated{
    [self getCurrentLocation];
}

-(void)viewDidDisappear:(BOOL)animated
{
    // self.navigationController.navigationBarHidden = YES;
}

-(void) createNavLeftButton
{
    UIImage *buttonImage = [UIImage imageNamed:@"signup_btn_back_bg_on.png"];
    UIButton *cancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    
    [cancelButton addTarget:self action:@selector(cancelButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setFrame:CGRectMake(0.0f,0.0f,buttonImage.size.width,buttonImage.size.height)];
    
    [Helper setButton:cancelButton Text:NSLocalizedString(@"BACK", @"BACK") WithFont:Robot_Light FSize:11 TitleColor:[UIColor blueColor] ShadowColor:nil];
    [cancelButton setTitle:NSLocalizedString(@"BACK", @"BACK") forState:UIControlStateNormal];
    [cancelButton setTitle:NSLocalizedString(@"BACK", @"BACK") forState:UIControlStateSelected];
    [cancelButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
    [cancelButton setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateHighlighted];
    cancelButton.titleLabel.font = [UIFont fontWithName:Robot_Light size:11];
    [cancelButton setBackgroundImage:buttonImage forState:UIControlStateHighlighted];
    
    //Adding Button onto View
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:cancelButton];
    // UIBarButtonItem *homeButton = [[UIBarButtonItem alloc] initWithCustomView:segmentView];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -16;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
    
    
    
}
-(void)createnavRightButton
{
    navDoneButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    
    [navDoneButton addTarget:self action:@selector(DoneButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    
    [navDoneButton setFrame:CGRectMake(0.0f,0.0f, 60,30)];
    
    [Helper setButton:navDoneButton Text:NSLocalizedString(@"Done", @"Done") WithFont:@"HelveticaNeue" FSize:17 TitleColor:[UIColor blueColor] ShadowColor:nil];
    
    
    [navDoneButton setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    
    // Create a container bar button
    UIBarButtonItem *containingnextButton = [[UIBarButtonItem alloc] initWithCustomView:navDoneButton];
    
    self.navigationItem.rightBarButtonItem = containingnextButton;
}

-(void)leftBarButtonClicked:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)dismissKeyboard
{
    [emailTextField resignFirstResponder];
    [passwordTextField resignFirstResponder];
    
}

-(void)cancelButtonClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)signInUser
{
    NSString *email = emailTextField.text;
    NSString *carid = _caridTextField.text;
    NSString *password = passwordTextField.text;
    
    if((unsigned long)email.length == 0)
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Enter Email ID", @"Enter Email ID")];
        [emailTextField becomeFirstResponder];
    }
    else if([self emailValidationCheck:email] == 0)
    {
        //email is not valid
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Invalid Email ID", @"Invalid Email ID")];
        emailTextField.text = @"";
        [emailTextField becomeFirstResponder];
        
    }
    
    else if((unsigned long)carid.length == 0)
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Enter the car ID", @"Enter the car ID")];
        [_caridTextField becomeFirstResponder];
    }
    else if((unsigned long)password.length == 0)
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Enter Password", @"Enter Password")];
        [passwordTextField becomeFirstResponder];
    }
    else
    {
        [self.view endEditing:YES];
        checkLoginCredentials = YES;
        [self sendServiceForLogin];
        
    }
    
}
-(void)DoneButtonClicked
{
    [self signInUser];
}

-(void)sendServiceForLogin
{
    PMDReachabilityWrapper * reachability = [PMDReachabilityWrapper sharedInstance];
    if ([reachability isNetworkAvailable])
    {

    ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
    [progressIndicator showPIOnView:self.view withMessage:NSLocalizedString(@"Signing in", @"Signing in")];
    NetworkHandler * handler = [NetworkHandler sharedInstance];
    
    NSString * pushToken;
    if([[NSUserDefaults standardUserDefaults]objectForKey:kSMPgetPushToken])
    {
        pushToken =[[NSUserDefaults standardUserDefaults]objectForKey:kSMPgetPushToken];
        
    }
    else
    {
        pushToken =@"dgfhfghr765998ghghj";
        
    }
        NSDictionary *queryParams;
        queryParams = [NSDictionary dictionaryWithObjectsAndKeys: emailTextField.text, kSMPLoginEmail,
                       passwordTextField.text, kSMPLoginPassword,
                       _caridTextField.text,kSMPLoginCarId,
                       [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],kSMPLoginDevideId,
                       pushToken,kSMPLoginPushToken,
                       @"1",kSMPLoginDeviceType,
                       [NSNumber numberWithDouble:_currentLatitude],kSMPSignUpLattitude,
                       [NSNumber numberWithDouble:_currentLongitude],kSMPSignUpLongitude,
                       [Helper getCurrentDateTime], kSMPCommonUpDateTime,
                       nil];
        NSLog(@"login Parameters >>>>>>>>>>%@",queryParams);
        [handler composeRequestWithMethod:MethodPatientLogin
                                  paramas:queryParams
                             onComplition:^(BOOL succeeded, NSDictionary *response) {
                                 if (succeeded) {
                                     //handle success response
                                     [self loginResponse:response];
                                 }
                                 else{
                                     //error
                                     UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Error", @"Network Error") message:response[NSLocalizedString(@"error", @"error")] delegate:nil cancelButtonTitle:NSLocalizedString(@"Ok", @"Ok") otherButtonTitles:nil, nil];
                                     [alertView show];
                                     [[ProgressIndicator sharedInstance]hideProgressIndicator];
                                 }
                             }];
    }
    else
    {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi showMessage:kNetworkErrormessage On:self.view];
    }
}

-(void)loginResponse :(NSDictionary *)dict
{
    ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
    [progressIndicator hideProgressIndicator];
    NSLog(@"loginResponse>>>>>>>>>>>>>>%@",dict);
    if ([dict[@"errFlag"]intValue] ==0)
    {
         NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
        [ud setObject:dict[@"data"][@"token"] forKey:KDAcheckUserSessionToken];
        [ud setObject:dict[@"data"][@"chn"] forKey:kNSURoadyoPubNubChannelkey];
        [ud setObject:dict[@"data"][@"email"] forKey:kNSUPatientEmailAddressKey];
        [ud setObject:dict[@"data"][@"susbChn"] forKey:kNSURoadyoSubscribeChanelKey];
        [ud setObject:dict[@"data"][@"listner"] forKey:kNSUDriverListnerChannelKey];
        [ud setObject:dict[@"data"][@"vehTypeId"] forKey:kNSUDriverCarTypeIdKey];
        [ud setObject:dict[@"data"][@"cityid"] forKey:KDAgetcityId];
        [ud setObject:dict[@"data"][@"presenseChn"] forKey:@"PresenceChn"];
        [ud setObject:dict[@"data"][@"typeImage"] forKey:kNSURoadyoCarImage];
        [ud setObject:dict[@"data"][@"carType"] forKey:@"LoginCarType"];
        [ud setObject:dict[@"data"][@"pub"] forKey:@"PubKey"];
        [ud setObject:dict[@"data"][@"sub"] forKey:@"SubKey"];
        [ud setObject:dict[@"data"][@"profilePic"] forKey:@"pRofilePic"];
        [ud setObject:dict[@"data"][@"fname"] forKey:@"DriverFirstNAme"];
        [ud setObject:dict[@"data"][@"phone"] forKey:@"phone"];
        [ud setObject:dict[@"data"][@"pub"] forKey:@"pubnubPublishKey"];
        [ud setObject:dict[@"data"][@"sub"] forKey:@"pubnubSubscribeKey"];
        [ud setObject:dict[@"data"][@"driverid"] forKey:@"driverid"];
        
        [ud setObject:@"0" forKey:KNSUIsFromSignUp];
        [ud synchronize];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                    @"Main" bundle:[NSBundle mainBundle]];
        
        UINavigationController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"home"];
        [self.navigationController setNavigationBarHidden:YES animated:YES];
        self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
    }
    else
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:dict[@"errMsg"]];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
}

#pragma mark - TextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField == emailTextField)
    {
        [_caridTextField becomeFirstResponder];
    }
    else if(textField == _caridTextField){
        [passwordTextField becomeFirstResponder];
    }
    else  {
        
        [passwordTextField resignFirstResponder];
        [self DoneButtonClicked];
        
    }
    return YES;
    
}


- (IBAction)forgotPasswordButtonClicked:(id)sender
{
    [self.view endEditing:YES];
    UIAlertView *forgotPasswordAlert = [[UIAlertView alloc]
                                        initWithTitle:NSLocalizedString(@"Forget Password?", @"Forget Password? ")
                                        message:NSLocalizedString(@"Enter your email address below and we will send you password reset instruction.", @"Enter your email address below and we will send you password reset instruction.")
                                        delegate:self
                                        cancelButtonTitle:NSLocalizedString(@"Cancel", @"Cancel")
                                        otherButtonTitles:NSLocalizedString(@"Submit", @"Submit"), nil];
    
    UITextField *emailForgot = [[UITextField alloc]initWithFrame:CGRectMake(30, 100, 225, 30)];
    emailForgot.delegate =self;
    
    emailForgot.placeholder = NSLocalizedString(@"Email", @"Email");
    
    emailImageView.frame = CGRectMake(220,110,18,18);
    
    [forgotPasswordAlert addSubview:emailForgot];
    [forgotPasswordAlert addSubview:emailImageView];
    
    forgotPasswordAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
    
    [forgotPasswordAlert show];
    
    //forgotView.hidden=NO;
    NSLog(@"password recovery to be done here");
    
    
}

- (IBAction)signInButtonClicked:(id)sender {
    [self signInUser];
}
- (void) forgotPaswordAlertviewTextField
{
    UIAlertView *forgotPasswordAlert = [[UIAlertView alloc]
                                        initWithTitle:NSLocalizedString(@"Invalid Email ID", @"Invalid Email ID")
                                        message:NSLocalizedString(@"Re-enter your email ID ", @"Re-enter your email ID ")
                                        delegate:self
                                        cancelButtonTitle:NSLocalizedString(@"Cancel", @"Cancel")
                                        otherButtonTitles:NSLocalizedString(@"Submit", @"Submit"), nil];
    forgotPasswordAlert.tag = 1;
    UITextField *emailForgot = [[UITextField alloc]initWithFrame:CGRectMake(30, 100, 225, 30)];
    
    
    [forgotPasswordAlert addSubview:emailForgot];
    
    forgotPasswordAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
    
    [forgotPasswordAlert show];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 1)
    {
        UITextField *forgotEmailtext = [alertView textFieldAtIndex:0];
        NSLog(@"Email Name: %@", forgotEmailtext.text);
        
        if (((unsigned long)forgotEmailtext.text.length ==0) || [Helper emailValidationCheck:forgotEmailtext.text] == 0)
        {
            [self forgotPaswordAlertviewTextField];
        }
        else
        {
            [self retrievePassword:forgotEmailtext.text];
        }
    }
    
    else
    {
        NSLog(@"cancel");
        passwordTextField.text = @"";
    }
}

- (void)retrievePassword:(NSString *)text
{
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:NSLocalizedString(@"Loading..", @"Loading..")];
    
    WebServiceHandler *handler = [[WebServiceHandler alloc] init];
    
    NSString *parameters = [NSString stringWithFormat:@"ent_email=%@&ent_user_type=%@",text,@"1"];
    
    NSString *removeSpaceFromParameter=[Helper removeWhiteSpaceFromURL:parameters];
    NSLog(@"request doctor around you :%@",parameters);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@forgotPassword",BASE_URL]];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    
    [theRequest setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [theRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody:[[removeSpaceFromParameter stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding]
                             dataUsingEncoding:NSUTF8StringEncoding
                             allowLossyConversion:YES]];
    
    [handler placeWebserviceRequestWithString:theRequest Target:self Selector:@selector(retrievePasswordResponse:)];
    
}

-(void)retrievePasswordResponse :(NSDictionary *)response
{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    
    NSLog(@"response:%@",response);
    if (!response)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error") message:[response objectForKey:NSLocalizedString(@"Message", @"Message")] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
        [alertView show];
        
    }
    else if ([response objectForKey:@"Error"])
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error") Message:[response objectForKey:@"errMsg"]];
        
    }
    else
    {
        NSDictionary *dictResponse=[response objectForKey:@"ItemsList"];
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
        {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:[dictResponse objectForKey:@"errMsg"]];
        }
        else
        {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:[dictResponse objectForKey:@"errMsg"]];
            
        }
    }
}



- (BOOL) emailValidationCheck: (NSString *) emailToValidate
{
    NSString *regexForEmailAddress = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailValidation = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regexForEmailAddress];
    return [emailValidation evaluateWithObject:emailToValidate];
}
- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation {
    
    
    _currentLatitude = newLocation.coordinate.latitude;
    _currentLongitude = newLocation.coordinate.longitude;
    
    [_locationManager stopUpdatingLocation];
    
}
- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error{
    
}
-(CLLocationCoordinate2D)getLocation {
    
    CLLocationManager *locationManager = [[CLLocationManager alloc] init] ;
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    [locationManager startUpdatingLocation];
    [locationManager stopUpdatingLocation];
    
    CLLocation *location = [locationManager location];
    CLLocationCoordinate2D coordinate = [location coordinate];
    
    return coordinate;
    
}

@end
