//
//  SignInViewController.h
//  privMD
//
//  Created by Rahul Sharma on 13/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HLPlaceholder.h"
@interface SignInViewController : UIViewController<UITextFieldDelegate,UITextViewDelegate>
{
    NSDictionary *itemList;
    BOOL checkLoginCredentials;
   
    
}

@property (weak, nonatomic) IBOutlet HLPlaceholder *emailTextField;
@property (weak, nonatomic) IBOutlet HLPlaceholder *passwordTextField;
@property (weak, nonatomic) IBOutlet HLPlaceholder *caridTextField;

@property (strong, nonatomic) IBOutlet UIButton *forgotPasswordButton;
@property (strong, nonatomic) IBOutlet UIView *containEmailnPass;
@property (weak, nonatomic) IBOutlet UILabel *fNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *passwordLabel;
@property (weak, nonatomic) IBOutlet UIButton *signinButton;

@property (strong, nonatomic)  UIButton *navDoneButton;
@property (strong, nonatomic) UIImageView *emailImageView;

- (IBAction)forgotPasswordButtonClicked:(id)sender;
- (IBAction)signInButtonClicked:(id)sender;


@end
