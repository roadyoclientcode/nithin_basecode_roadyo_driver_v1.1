//
//  SignUpViewController.m
//  privMD
//
//  Created by Rahul Sharma on 13/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "SignUpViewController.h"
//#import "MapViewController.h"
#import "TermsnConditionViewController.h"
#import "SignInViewController.h"
#import "UIImageView+WebCache.h"
#import "SDWebImageDownloader.h"
#import "HomeViewController.h"
#import "PMDReachabilityWrapper.h"
#import "DoneCancelNumberPadToolbar.h"
#import "Cartype.h"
#import "MenuViewController.h"
#import "Errorhandler.h"
#import "ResKitWebService.h"
#import "Fonts.h"
#import "Helper.h"
#import "ProgressIndicator.h"
#import "CustomNavigationBar.h"
#import "SignUp.h"
#import "UploadFiles.h"
#import "SignUpDetails.h"



#define REGEX_PASSWORD_ONE_UPPERCASE @"^(?=.*[A-Z]).*$"  //Should contains one or more uppercase letters
#define REGEX_PASSWORD_ONE_LOWERCASE @"^(?=.*[a-z]).*$"  //Should contains one or more lowercase letters
#define REGEX_PASSWORD_ONE_NUMBER @"^(?=.*[0-9]).*$"  //Should contains one or more number
#define REGEX_PASSWORD_ONE_SYMBOL @"^(?=.*[!@#$%&_]).*$"  //Should contains one or more symbol


@interface SignUpViewController ()<DoneCancelNumberPadToolbarDelegate,CustomNavigationBarDelegate,CLLocationManagerDelegate,UploadFileDelegate,UIAlertViewDelegate>
{
    NSArray * arrPracticenor;
    int practicenortype;
    NSMutableArray * arrCartype;
    NSMutableArray * arrSeat;
    NSString *pickerTitle;
    UIImage *resizedImage;
    NSDictionary * SignupResponse;
}
@property (assign,nonatomic) BOOL isImageNeedsToUpload;
@property(nonatomic,strong)CLLocationManager *locationManager;
@property(nonatomic,assign)float currentLatitude;
@property(nonatomic,assign)float currentLongitude;
@property(nonatomic,strong)Cartype *selectedCarType;
@property(nonatomic,assign)NSInteger selectedCapacity;
@property(nonatomic,strong)UITextField *activeTextField;
@property(nonatomic,assign)BOOL isKeyboardIsShown;
@property(nonatomic,strong)SignUpDetails *signUpDetails;
@property(nonatomic,strong)UIImageView *profileImage;
@property(nonatomic,weak) IBOutlet UITableView *tableView;


//-(PasswordStrengthType)checkPasswordStrength:(NSString *)password;
-(int)checkPasswordStrength:(NSString *)password;

@end

@implementation SignUpViewController

@synthesize mainView;
@synthesize mainScrollView;
@synthesize firstNameTextField;
@synthesize lastNameTextField;
@synthesize emailTextField;
@synthesize passwordTextField;
@synthesize conPasswordTextField;
@synthesize phoneNoTextField;
@synthesize carRegistrationNumberTextField;
@synthesize helperCountry;
@synthesize helperCity;
@synthesize saveSignUpDetails;
@synthesize profileButton;
@synthesize profileImageView;
@synthesize tncButton;
@synthesize tncCheckButton;
@synthesize creatingLabel;
@synthesize btnpracticenorType;
@synthesize carTypeLabel;
@synthesize SeatingCapacityLabel;
@synthesize activeTextField;
@synthesize licenceNumberTextField;


#pragma mark -init

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark -view life cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.hidesBackButton = NO;
    emailTextField.placeholder = 
    self.title = NSLocalizedString(@"CREATE ACCOUNT", @"CREATE ACCOUNT");
    [self createNavLeftButton];
    [self createNavRightButton];
    _signUpDetails = [SignUpDetails sharedInstance];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tapGesture];
        // ProfileImageView
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(5, 40, 90, 90)];
    
    _profileImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 90, 90)];
    _profileImage.image = [UIImage imageNamed:@"aa_default_profile_pic.jpg"];
    [view addSubview:_profileImage];
    
    UIButton *buttonSelectPicture = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonSelectPicture.frame = CGRectMake(0, 0, 75, 75);
    [buttonSelectPicture addTarget:self action:@selector(profileButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:buttonSelectPicture];
    
    [self.tableView addSubview:view];
    
    
    
    //table bottom view
    
    UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
    UIButton *buttonCheckBox = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonCheckBox.frame = CGRectMake(5, 5, 34, 34);
    [buttonCheckBox setImage:[UIImage imageNamed:@"signup_btn_checkbox_off"] forState:UIControlStateNormal];
    [buttonCheckBox setImage:[UIImage imageNamed:@"signup_btn_checkbox_on"] forState:UIControlStateSelected];
    [buttonCheckBox addTarget:self action:@selector(checkButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:buttonCheckBox];
    
    
    UIButton *buttonTermsAndCondition = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonTermsAndCondition.frame = CGRectMake(40, 5, 275, 40);
    [Helper setButton:buttonTermsAndCondition Text:NSLocalizedString(@"I ACCEPT TERMS AND CONDITIONS", @"I ACCEPT TERMS AND CONDITIONS") WithFont:Robot_CondensedLight FSize:15 TitleColor:[UIColor blueColor] ShadowColor:nil];
    [buttonTermsAndCondition addTarget:self action:@selector(TermsNconButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    buttonTermsAndCondition.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [bottomView addSubview:buttonTermsAndCondition];
    
    self.tableView.tableFooterView = bottomView;
    
    arrSeat = [[NSMutableArray alloc]initWithObjects:@"4",@"6",@"8", nil];
    isTnCButtonSelected = NO;
    
}

-(void)viewWillAppear:(BOOL)animated
{
    // Register notification when the keyboard will be show
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    // Register notification when the keyboard will be hide
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    _isKeyboardIsShown = NO;
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

-(void) createNavLeftButton
{
    // UIView *navView = [[UIView new]initWithFrame:CGRectMake(0, 0,50, 44)];
    UIImage *buttonImage = [UIImage imageNamed:@"signup_btn_back_bg_on.png"];
    UIButton *navCancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    
    [navCancelButton addTarget:self action:@selector(cancelButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [navCancelButton setFrame:CGRectMake(0.0f,0.0f,buttonImage.size.width,buttonImage.size.height)];
    [Helper setButton:navCancelButton Text:NSLocalizedString(@"BACK", @"BACK") WithFont:Robot_Light FSize:11 TitleColor:[UIColor blueColor] ShadowColor:nil];
    [navCancelButton setTitle:NSLocalizedString(@"BACK", @"BACK") forState:UIControlStateNormal];
    [navCancelButton setTitle:NSLocalizedString(@"BACK", @"BACK") forState:UIControlStateSelected];
    [navCancelButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
    [navCancelButton setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateHighlighted];
    navCancelButton.titleLabel.font = [UIFont fontWithName:Robot_Light size:11];
    [navCancelButton setBackgroundImage:buttonImage forState:UIControlStateHighlighted];
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    // UIBarButtonItem *homeButton = [[UIBarButtonItem alloc] initWithCustomView:segmentView];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -16;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}

-(void)createNavRightButton
{
    UIButton *navNextButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *buttonImage = [UIImage imageNamed:@"signup_btn_back_bg_on.png"];
    [navNextButton setFrame:CGRectMake(0,0,buttonImage.size.width,buttonImage.size.height)];
    
    [navNextButton addTarget:self action:@selector(NextButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    
    
    [Helper setButton:navNextButton Text:NSLocalizedString(@"NEXT", @"NEXT") WithFont:Robot_Light FSize:11 TitleColor:[UIColor blueColor] ShadowColor:nil];
    [navNextButton setTitle:NSLocalizedString(@"NEXT", @"NEXT") forState:UIControlStateNormal];
    [navNextButton setTitle:NSLocalizedString(@"NEXT", @"NEXT") forState:UIControlStateSelected];
    [navNextButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
    [navNextButton setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateHighlighted];
    [navNextButton setBackgroundImage:buttonImage forState:UIControlStateHighlighted];
    
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navNextButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -16;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
    
}


-(void)getCurrentLocation
{
    //check location services is enabled
    if ([CLLocationManager locationServicesEnabled])
    {
        if (!_locationManager)
        {
            _locationManager = [[CLLocationManager alloc] init];
            _locationManager.delegate = self;
            _locationManager.distanceFilter = kCLDistanceFilterNone;
            _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        }
        [_locationManager startUpdatingLocation];
    }
    else {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Location Service", @"Location Service") message:NSLocalizedString(@"Unable to find your location,Please enable location services.", @"Unable to find your location,Please enable location services.") delegate:nil cancelButtonTitle:NSLocalizedString(@"Ok", @"Ok") otherButtonTitles:nil, nil];
        [alertView show];
    }
}


#pragma mark -CLLocation manager deleagte method
- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation __OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_6, __MAC_NA, __IPHONE_2_0, __IPHONE_6_0){
    
    _currentLatitude= newLocation.coordinate.latitude;
    _currentLongitude= newLocation.coordinate.longitude;
    [_locationManager stopUpdatingLocation];
    
}
- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error{
    
}

#pragma mark -leftbar and righbar action

-(void)rightBarButtonClicked:(UIButton *)sender
{
    [self NextButtonClicked];
}

-(void)leftBarButtonClicked:(UIButton *)sender
{
    [self cancelButtonClicked];
}

-(void)dismissKeyboard
{
    [self.view endEditing:YES];
    [firstNameTextField resignFirstResponder];
    [lastNameTextField resignFirstResponder];
    [emailTextField resignFirstResponder];
    [passwordTextField resignFirstResponder];
    [conPasswordTextField resignFirstResponder];
    [phoneNoTextField resignFirstResponder];
    [carRegistrationNumberTextField resignFirstResponder];
    [licenceNumberTextField resignFirstResponder];
    [newSheet dismissWithClickedButtonIndex:1 animated:YES];
    [self moveViewDown];
}

-(void)signUp
{
    NSString *signupFirstName = _signUpDetails.firstName;
    NSString *lastName = _signUpDetails.lastName;
    NSString *signupEmail = _signUpDetails.email;
    NSString *signupPassword = _signUpDetails.password;
    NSString *signupPhoneno = _signUpDetails.mobile;
    if ((unsigned long)signupFirstName.length == 0)
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Enter First Name", @"Enter First Name")];
    }
    else if ((unsigned long)signupEmail.length == 0)
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Enter Email ID", @"Enter Email ID")];
        
    }
    else if ((unsigned long)signupPhoneno.length == 0)
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Enter contact number", @"Enter contact number")];
        
    }
    else if ((unsigned long)signupPassword.length == 0)
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Enter Password", @"Enter Password")];
        
    }
    else if ([Helper emailValidationCheck:signupEmail] == 0)
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Invalid Email Id", @"Invalid Email Id")];
        
        
    }
    else if (!isTnCButtonSelected)
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Please select our Terms and Conditions", @"Please select our Terms and Conditions")];
    }
    else
    {
        
        NSString * pushToken;
        if([[NSUserDefaults standardUserDefaults]objectForKey:kSMPgetPushToken])
        {
            pushToken =[[NSUserDefaults standardUserDefaults]objectForKey:kSMPgetPushToken];
            
        }
        else
        {
            pushToken =@"dgfhfghr765998ghghj";
            
        }
        
        ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
        [progressIndicator showPIOnView:self.view withMessage:NSLocalizedString(@"Signing up", @"Signing up")];

        NetworkHandler * handler = [NetworkHandler sharedInstance];
        
        if ([lastName length] == 0) {
            lastName = @"";
        }
        NSDictionary *queryParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                     
                                     signupFirstName,kSMPSignUpFirstName,
                                     lastName,kSMPSignUpLastName,
                                     signupEmail,kSMPSignUpEmail,
                                     signupPassword, kSMPSignUpPassword,
                                     signupPhoneno,kSMPSignUpMobile,
                                     //     postCode,kSMPSignUpZipCode,
                                     [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],kSMPSignUpDeviceId,
                                     pushToken,kSMPgetPushToken,
                                     @"1",kSMPSignUpDeviceType,
                                     [Helper getCurrentDateTime],kSMPSignUpDateTime, nil];
        
        NSLog(@"param%@",queryParams);
        [handler composeRequestWithMethod:MethodPatientSignUp
                                  paramas:queryParams
                             onComplition:^(BOOL succeeded, NSDictionary *response)
        {
                                          if (succeeded) { //handle success response
                                              [self signupResponse:response];
                                          }
                                          else{//error
                                              UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Error", @"Network Error") message:response[NSLocalizedString(@"error", @"error")] delegate:nil cancelButtonTitle:NSLocalizedString(@"Ok", @"Ok") otherButtonTitles:nil, nil];
                                              [alertView show];
                                              [[ProgressIndicator sharedInstance]hideProgressIndicator];
                                          }
                                      }];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - MainView Up/Down
-(void)moveViewUpToPoint:(int)point
{
    CGRect rect = mainScrollView.frame;
    rect.origin.y = point;
    [UIView animateWithDuration:0.4
                     animations:^(void){
                         mainScrollView.frame = rect;
                     }
                     completion:^(BOOL finished){
                     }];
    
    
}

-(void)moveViewDown
{
    CGRect rect = mainScrollView.frame;
    rect.origin.y = 0;
    //    [UIView beginAnimations:nil context:nil];
    //    [UIView setAnimationDuration:.2];
    //    [mainScrollSignUp setFrame:rect];
    //    [UIView commitAnimations];
    
    [UIView animateWithDuration:0.4
                     animations:^(void){
                         mainScrollView.frame = rect;
                     }
                     completion:^(BOOL finished){
                     }];
}


-(void)signupResponse :(NSDictionary *)dictionary
{
    SignupResponse =dictionary;
    ProgressIndicator * progress = [ProgressIndicator sharedInstance];
    //[progress hideProgressIndicator];
    
//    Errorhandler * handler = [dictionary objectAtIndex:0];
    
    if ([dictionary[@"errFlag"] intValue] ==0) {
        
//        SignUp  * signup = handler.objects;
        
        NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
        [ud setObject:dictionary[@"data"][@"token"] forKey:KDAcheckUserSessionToken];
        [ud setObject:dictionary[@"data"][@"chn"] forKey:kNSURoadyoPubNubChannelkey];
        [ud setObject:dictionary[@"data"][@"email"] forKey:kNSUPatientEmailAddressKey];
        [ud setObject:@"1" forKey:KNSUIsFromSignUp];
        
        [_signUpDetails clear];
        
        if (_pickedImage != nil) {
            
            [self uploadImage];
        }
        else {
            [progress hideProgressIndicator];
            [self gotoHomeScreen];
        }
    }
    else{
        
        [progress hideProgressIndicator];
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:dictionary[@"errMsg"]];
    }
}
-(void)gotoHomeScreen{
    
    //    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
    //                                @"Main" bundle:[NSBundle mainBundle]];
    //
    //    MenuViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"home"];
    //    self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Message", @"Message") message:NSLocalizedString(@"Thankyou for your request.In next 24 hours one of our staff will contact you for further process.", @"Thankyou for your request.In next 24 hours one of our staff will contact you for further process.") delegate:self cancelButtonTitle:NSLocalizedString(@"Ok", @"Ok") otherButtonTitles:nil, nil];
    [alert show];
}

-(void)uploadImage{
    
    UploadFiles * upload = [[UploadFiles alloc]init];
    upload.delegate = self;
    [upload uploadImageFile:resizedImage];
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag==200) {
        
        [self cancelButtonClicked];
        // [self.navigationController popViewControllerAnimated:YES];
    }
    else {
        [self cancelButtonClicked];
    }
}

#pragma mark -ButtonAction


- (void)showDatePickerWithTitle:(UITextField*)textFeild
{
    pickerTitle = NSLocalizedString(@"Choose company", @"Choose company");
    newSheet=[[UIActionSheet alloc]initWithTitle:pickerTitle
                                        delegate:nil
                               cancelButtonTitle:nil
                          destructiveButtonTitle:nil
                               otherButtonTitles:nil];
    newSheet.backgroundColor = [UIColor whiteColor];
    
    
    //[self moveViewupwordto:300];
    pickerview = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 40, 0, 0)];
    pickerview.delegate = self;
    pickerview.dataSource = self;
    pickerview.showsSelectionIndicator = YES;
    pickerview.opaque = NO;
    pickerview.tag = 100;
    [self.view addSubview:pickerview];
    [newSheet addSubview:pickerview];
    UIButton *closeButton= [UIButton buttonWithType:UIButtonTypeCustom];
    [closeButton setTitle:NSLocalizedString(@"Done", @"Done") forState:UIControlStateNormal];
    [closeButton setTitleColor:[UIColor greenColor] forState:UIControlStateNormal];
    
    closeButton.frame = CGRectMake(260, 7.0f, 50.0f, 30.0f);
    
    [closeButton addTarget:self action:@selector(dismissActionSheet) forControlEvents:UIControlEventTouchUpInside];
    
    [newSheet addSubview:closeButton];
    
    UIButton *cancelButton= [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelButton setTitle:NSLocalizedString(@"Cancel", @"Cancel") forState:UIControlStateNormal];
    [cancelButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    cancelButton.frame = CGRectMake(10, 7.0f, 70.0f, 30.0f);
    
    
    cancelButton.tintColor = [UIColor greenColor];
    [cancelButton addTarget:self action:@selector(dismissActionSheet1) forControlEvents:UIControlEventTouchUpInside];
    [newSheet addSubview:cancelButton];
    
    
    [newSheet showInView:[[UIApplication sharedApplication] keyWindow]];
    [newSheet setBounds:CGRectMake(0, 0, 320, 480)];
    
    
    
    
    
    
}

#pragma mark -
#pragma mark UIPickerView Delegate

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    if (pickerView.tag == 100) {
        return [arrCartype count];
    }
    
    return 0;
    
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    NSString * strCar;
    if (pickerView.tag == 100) {
        //        if (row == arrCartype.count) {
        //
        //            strCar = @"Other";
        //        }
        //        else {
        
        Cartype * type = [arrCartype objectAtIndex:row];
        strCar = type.companyname;
        //   }
        
    }
    
    
    return strCar;
    
    
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component;
{
    
    //
    //    if (pickerView.tag == 100)
    //    {
    //          Cartype *type = [arrCartype objectAtIndex:row];
    //          //[Helper setToLabel:lblCarType Text:type.type_name WithFont:Robot_Light FSize:11 Color:UIColorFromRGB(0x000000)];
    //
    //    }
    
}

- (void)dismissActionSheet//save
{
    [newSheet dismissWithClickedButtonIndex:1 animated:YES];
    NSInteger selectedIndex = [pickerview selectedRowInComponent:0];
    if (pickerview.tag == 100) { //cartype
        
        //        if (selectedIndex == arrCartype.count) {
        //            _selectedCarType = 0;
        //            self.activeTextField.text = @"Other";
        //            _signUpDetails.compneyName = @"Other";
        //            _signUpDetails.compneyId = @"0";
        //        }
        //        else{
        
        _selectedCarType = arrCartype[selectedIndex];
        self.activeTextField.text = _selectedCarType.companyname;
        _signUpDetails.compneyName = _selectedCarType.companyname;
        _signUpDetails.compneyId = _selectedCarType.company_id;
        // }
        
        
        
        [self findNextResponder:self.activeTextField];
        
    }
    else if (pickerview.tag == 200) { //Capacity
        
        _seaterTextFeild.text = arrSeat[selectedIndex];
        _selectedCapacity = [arrSeat[selectedIndex] integerValue];
        [carRegistrationNumberTextField becomeFirstResponder];
        
    }
    
    
    
    //[lastNameTextField resignFirstResponder];
    
    [self moveViewDown];
}

- (void)dismissActionSheet1
{
    
    [newSheet dismissWithClickedButtonIndex:1 animated:YES];
    if (pickerview.tag == 100) {
        [self findNextResponder:self.activeTextField];
    }
    else {
        [carRegistrationNumberTextField becomeFirstResponder];
    }
    //[lastNameTextField resignFirstResponder];
}


#pragma mark - Done delegate method
-(void)doneCancelNumberPadToolbarDelegate:(DoneCancelNumberPadToolbar *)controller didClickDone:(UITextField *)textField
{
    if (textField.tag == kTagMobileNumber) {
        
        [self findNextResponder:textField];
        
    }
    
    
}

-(void)doneCancelNumberPadToolbarDelegate:(DoneCancelNumberPadToolbar *)controller didClickCancel:(UITextField *)textField
{
    if (textField.tag == kTagMobileNumber) {
        
        [self findNextResponder:textField];
        
    }
    
    
}
#pragma mark - TextFields
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    
    self.activeTextField = textField;
    
    //    if (textField.tag == kTagCompneyID) {
    //
    //        [self showDatePickerWithTitle:textField];
    //
    //        return NO;
    //    }
    
//    [self findNextResponder:textField];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField == conPasswordTextField)
    {
        int strength = [self checkPasswordStrength:passwordTextField.text];
        if(strength == 0)
        {
            
            [passwordTextField becomeFirstResponder];
        }
    }
    
    if (textField.tag != kTagEmail &&[_signUpDetails.email length] > 0 && ![Helper emailValidationCheck:_signUpDetails.email]) {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Invalid Email Id", @"Invalid Email Id")];
        UITextField *textFld = (UITextField *)[self.view viewWithTag:kTagEmail];
        [textFld becomeFirstResponder];
    }//    [self findNextResponder:textField];
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField.tag == kTagFirstName) {
        _signUpDetails.firstName = textField.text;
    }
    else if (textField.tag == kTagLastName) {
        _signUpDetails.lastName = textField.text;
    }
    else if (textField.tag == kTagEmail) {
        _signUpDetails.email = textField.text;
    }
    //    else if (textField.tag == kTagTaxNumber) {
    //        _signUpDetails.taxNumber = textField.text;
    //    }
    else if (textField.tag == kTagMobileNumber) {
        _signUpDetails.mobile = textField.text;
    }
    //    else if (textField.tag == kTagPasscode) {
    //        _signUpDetails.postCode = textField.text;
    //    }
    else if (textField.tag == kTagPassword) {
        _signUpDetails.password = textField.text;
    }
    //    else if(textField.tag == kTagConfirmPassword){
    //
    //        _signUpDetails.confirmPassword = textField.text;
    //    }
    //    else if (textField.tag == kTagCompneyID) {
    //
    //        _signUpDetails.compneyId = textField.text;
    //    }
    //    else if (textField.tag == kTagCompneyName) {
    //        _signUpDetails.compneyName = textField.text;
    //    }
}


- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField.tag == kTagConfirmPassword) {
        [self.view endEditing:YES];
        return YES;
    }
    [self findNextResponder:textField];
    return YES;
    
}
-(void)findNextResponder:(UITextField*)textField{
    
    CGPoint pnt = [self.tableView convertPoint:textField.bounds.origin fromView:textField];
    NSIndexPath* path = [self.tableView indexPathForRowAtPoint:pnt];
    
    NSIndexPath *nextIndexPath = [self nextIndexPath:path];
    UITableViewCell *nextCell = [self.tableView cellForRowAtIndexPath:nextIndexPath];
    
    if (nextCell == nil) {
        [textField resignFirstResponder];
    }
    else {
        
        for(id view in nextCell.contentView.subviews){
            if ([view isKindOfClass:[UITextField class]]) {
                UITextField *nextTextField = (UITextField*)view;
                [nextTextField becomeFirstResponder];
            }
        }
    }
    
//    if ([_signUpDetails.email length] > 0 && ![Helper emailValidationCheck:_signUpDetails.email]) {
//        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Invalid Email Id", @"Invalid Email Id")];
//        UITextField *textFld = (UITextField *)[self.view viewWithTag:kTagEmail];
//        [textFld becomeFirstResponder];
//    }
    

}
- (NSIndexPath *) nextIndexPath:(NSIndexPath *) indexPath {
    
    int numOfSections = [self numberOfSectionsInTableView:self.tableView];
    int nextSection = ((indexPath.section + 1) % numOfSections);
    
    if ((indexPath.row +1) == [self tableView:self.tableView numberOfRowsInSection:indexPath.section]) {
        return [NSIndexPath indexPathForRow:0 inSection:nextSection];
    } else {
        return [NSIndexPath indexPathForRow:(indexPath.row + 1) inSection:indexPath.section];
    }
}
- (void)NextButtonClicked
{
    [self dismissKeyboard];
    [self signUp];
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{

}

- (void)cancelButtonClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)TermsNconButtonClicked:(id)sender
{
    [self performSegueWithIdentifier:@"gotoTerms" sender:self];
}

- (IBAction)checkButtonClicked:(id)sender
{
    UIButton *mBut = (UIButton *)sender;
    
    if(mBut.isSelected)
    {
        isTnCButtonSelected = NO;
        [mBut setSelected:NO];
    }
    else
    {
        isTnCButtonSelected = YES;
        [mBut setSelected:YES];
        
        
    }
}

- (IBAction)profileButtonClicked:(id)sender
{
    [self.view endEditing:YES];
    UIActionSheet *actionSheet;
    if (!_pickedImage) {
        actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Select Picture", @"Select Picture") delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", @"Cancel") destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Take Photo", @"Take Photo"),NSLocalizedString(@"Choose From Library", @"Choose From Library"), nil];
    }else{
        actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Select Picture", @"Select Picture") delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", @"Cancel") destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Take Photo", @"Take Photo"),NSLocalizedString(@"Choose From Library", @"Choose From Library"),NSLocalizedString(@"Remove Photo", @"Remove Photo"), nil];
    }
    
    actionSheet.tag = 1;
    [actionSheet showInView:self.view];
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag == 1)
    {
        switch (buttonIndex)
        {
            case 0:
            {
                [self cameraButtonClicked:nil];
                break;
            }
            case 1:
            {
                [self libraryButtonClicked:nil];
                break;
            }
            case 2:
            {
                _profileImage.image = [UIImage imageNamed:@"aa_default_profile_pic.jpg"];
                _pickedImage = nil;
            }
            default:
                if (!_pickedImage) {
                    _profileImage.image = [UIImage imageNamed:@"aa_default_profile_pic.jpg"];
                }
                break;
        }
    }
}


-(void)cameraButtonClicked:(id)sender
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc]init];
    imagePicker.delegate =self;
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.allowsEditing = YES;
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Message", @"Message") message: NSLocalizedString(@"Camera is not available", @"Camera is not available") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil, nil];
        [alert show];
        
    }
}
-(void)libraryButtonClicked:(id)sender
{
    UIImagePickerController * picker = [[UIImagePickerController alloc] init];
    picker.delegate =self;
    picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    picker.allowsEditing = YES;
    // picker.contentSizeForViewInPopover = CGSizeMake(400, 800);
    //    [self presentViewController:picker animated:YES completion:nil];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        // popover = [[UIPopoverController alloc] initWithContentViewController:picker];
        
        
        
        //[popover presentPopoverFromBarButtonItem:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    } else {
        
        [self presentViewController:picker animated:YES completion:nil];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    NSLog(@"Image Info : %@",info);
    _pickedImage = [info objectForKey:UIImagePickerControllerEditedImage];
    
    CGSize newSize = CGSizeMake(130.00f, 130.0f);
    UIGraphicsBeginImageContext(newSize);
    [_pickedImage drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    resizedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    NSLog(@"pickedImage : %@",_pickedImage);
    
    //  [_signUpPhotoButton setImage:pickedImage forState:UIControlStateNormal];
    
    _profileImage.image = _pickedImage;
    
    
}

#pragma UploadFileDelegate

-(void)uploadFile:(UploadFiles *)uploadfile didUploadSuccessfullyWithUrl:(NSArray *)imageUrls
{
    ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
    [progressIndicator hideProgressIndicator];
    
    [self gotoHomeScreen];
    
    
    
    
}
-(void)uploadFile:(UploadFiles *)uploadfile didFailedWithError:(NSError *)error{
    NSLog(@"upload file  error %@",[error localizedDescription]);
    [self gotoHomeScreen];
}


//-(NSURL *) getImagePathWithURL
//{
//    NSString *strPath= [NSString stringWithFormat:@"NexPanama.png"];
//    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0];
//    NSString *path = [documentsDirectory stringByAppendingPathComponent:strPath];
//    NSURL *targetURL = [NSURL fileURLWithPath:path];
//    //    NSData *returnData=[NSData dataWithContentsOfURL:targetURL];
//    //    UIImage *imagemain=[UIImage returnData];
//    return targetURL;
//}

#pragma mark - Password Checking

-(int)checkPasswordStrength:(NSString *)password
{
    unsigned long int  len = password.length;
    //will contains password strength
    int strength = 0;
    
    if (len == 0) {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Please enter password first", @"Please enter password first")];
        
        return 0;//PasswordStrengthTypeWeak;
    } else if (len <= 5) {
        strength++;
    } else if (len <= 10) {
        strength += 2;
    } else{
        strength += 3;
    }
    int kp = strength;
    strength += [self validateString:password withPattern:REGEX_PASSWORD_ONE_UPPERCASE caseSensitive:YES];
    if (kp >= strength)
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Please enter atleast one Uppercase alphabet", @"Please enter atleast one Uppercase alphabet")];
        return 0;
    }
    else
    {
        kp++;
    }
    strength += [self validateString:password withPattern:REGEX_PASSWORD_ONE_LOWERCASE caseSensitive:YES];
    if (kp >= strength)
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Please enter atleast one Lowercase alphabet", @"Please enter atleast one Lowercase alphabet")];
        return 0;
    }
    else
    {
        kp++;
    }
    strength += [self validateString:password withPattern:REGEX_PASSWORD_ONE_NUMBER caseSensitive:YES];
    if (kp >= strength) {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Please enter atleast  one Number", @"Please enter atleast  one Number")];
        // [passwordTextField becomeFirstResponder];
        return 0;
    }
    return 1;
    //  strength += [self validateString:password withPattern:REGEX_PASSWORD_ONE_SYMBOL caseSensitive:YES];
    // if (kp >= strength) {
    //[Helper showAlertWithTitle:@"Message" Message:@"Please enter atleast one special symbol"];
    //[passwordTextField becomeFirstResponder];
    // return 0;
    // }
    
    
    //    if(strength <= 3){
    //        return PasswordStrengthTypeWeak;
    //    }else if(3 < strength && strength < 6){
    //        return PasswordStrengthTypeModerate;
    //    }else{
    //        return PasswordStrengthTypeStrong;
    //    }
}

// Validate the input string with the given pattern and
// return the result as a boolean
- (int)validateString:(NSString *)string withPattern:(NSString *)pattern caseSensitive:(BOOL)caseSensitive
{
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:((caseSensitive) ? 0 : NSRegularExpressionCaseInsensitive) error:&error];
    
    NSAssert(regex, @"Unable to create regular expression");
    
    NSRange textRange = NSMakeRange(0, string.length);
    //NSLog(@"test range %ld",textRange);
    NSRange matchRange = [regex rangeOfFirstMatchInString:string options:NSMatchingReportProgress range:textRange];
    
    BOOL didValidate = 0;
    
    // Did we find a matching range
    if (matchRange.location != NSNotFound)
        didValidate = 1;
    
    return didValidate;
}

#pragma mark - UIKeyBoardNotification Methods
-(void) keyboardWillHide:(NSNotification *)note
{
    // NSTimeInterval duration = [[[sender userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:0.4 animations:^{
        UIEdgeInsets edgeInsets = UIEdgeInsetsMake(64, 0, 0, 0);
        [[self tableView] setContentInset:edgeInsets];
        [[self tableView] setScrollIndicatorInsets:edgeInsets];
    }];
    
}
-(void) keyboardWillShow:(NSNotification *)note
{
    CGSize kbSize = CGSizeMake(320, 216);//[[[sender userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    // NSTimeInterval duration = [[[sender userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    CGFloat height = UIDeviceOrientationIsPortrait([[UIDevice currentDevice] orientation]) ? kbSize.height : kbSize.width;
    
    [UIView animateWithDuration:0.4 animations:^{
        UIEdgeInsets edgeInsets = [[self tableView] contentInset];
        edgeInsets.bottom = height;
        [[self tableView] setContentInset:edgeInsets];
        edgeInsets = [[self tableView] scrollIndicatorInsets];
        edgeInsets.bottom = height;
        [[self tableView] setScrollIndicatorInsets:edgeInsets];
    }];    _isKeyboardIsShown = YES;
}

#pragma mark -
#pragma mark UITableViewDelegate Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (section == 0) {
        return 2;
    }
    else if(section == 1){
        return 3;
    }
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = nil;//[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        
        
        UIImageView *line = [[UIImageView alloc] initWithFrame:CGRectMake(0, 49, 320, 1)];
        line.image = [UIImage imageNamed:@"signup_bg_namedetails_divider"];
        line.tag = 200;
        [cell.contentView addSubview:line];
        
    }
    
    UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(5, 5, 310, 40)];
    textField.borderStyle = UITextBorderStyleNone;
    textField.font = [UIFont fontWithName:Robot_CondensedRegular size:15];
    //textField.placeholder = @"enter text";
    textField.autocorrectionType = UITextAutocorrectionTypeNo;
    textField.keyboardType = UIKeyboardAppearanceDark;
    textField.returnKeyType = UIReturnKeyNext;
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    textField.delegate = self;
    [textField setTintColor:[UIColor blueColor]];
    [textField setValue:UIColorFromRGB(0x000000)
             forKeyPath:@"_placeholderLabel.textColor"];
    [cell.contentView addSubview:textField];
    
    
    
    
    
    
    
    
    if (indexPath.section == 0) {
        
        if (indexPath.row == 0) {
            
            UIImageView *lineImageView = (UIImageView*)[cell.contentView viewWithTag:200];
            lineImageView.frame = CGRectMake(100, 49, 220, 1);
            textField.frame = CGRectMake(100, 5, 210, 40);
            textField.text = _signUpDetails.firstName;
            textField.tag = kTagFirstName;
            textField.autocapitalizationType = UITextAutocapitalizationTypeWords;
            textField.placeholder = NSLocalizedString(@"FIRST NAME*", @"FIRST NAME*");
        }
        if (indexPath.row == 1) {
            
            textField.tag = kTagLastName;
            textField.text = _signUpDetails.lastName;
            textField.autocapitalizationType = UITextAutocapitalizationTypeWords;
            textField.placeholder = NSLocalizedString(@"LAST NAME", @"LAST NAME");
            textField.frame = CGRectMake(100, 5, 210, 40);
        }
        
    }
    else if (indexPath.section == 1){
        
        if (indexPath.row == 0) {
            
            textField.placeholder = NSLocalizedString(@"EMAIL*", @"EMAIL*");
            textField.text = _signUpDetails.email;
            textField.tag = kTagEmail;
            textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
            [textField setKeyboardType:UIKeyboardTypeEmailAddress];
            
        }
        if (indexPath.row == 1) {
            
            textField.placeholder = NSLocalizedString(@"CONTACT NUMBER*", @"CONTACT NUMBER*");
            textField.tag = kTagMobileNumber;
            textField.text = _signUpDetails.mobile;
            
            DoneCancelNumberPadToolbar *toolbar = [[DoneCancelNumberPadToolbar alloc] initWithTextField:textField];
            toolbar.delegate = self;
            carRegistrationNumberTextField.inputAccessoryView = toolbar;
            
        }
        //        if (indexPath.row == 3) {
        //
        //            textField.placeholder = @"COMPANY NAME";
        //            textField.tag = kTagCompneyID;
        //            textField.text = _signUpDetails.compneyName;
        //        }
        //        if (indexPath.row == 3) {
        //
        //            textField.placeholder = @"TAX NUMBER";
        //            textField.tag = kTagTaxNumber;
        //            textField.text = _signUpDetails.taxNumber;
        //        }
        if (indexPath.row == 2) {
            
            textField.placeholder = NSLocalizedString(@"PASSWORD*", @"PASSWORD*");
            textField.tag = kTagPassword;
            textField.text = _signUpDetails.password;
            textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
            textField.secureTextEntry = YES;
        }
        //        if (indexPath.row == 3) {
        //            
        //            textField.placeholder = @"CONFIRM PASSWORD";
        //            textField.tag = kTagConfirmPassword;
        //            textField.text = _signUpDetails.confirmPassword;
        //             textField.secureTextEntry = YES;
        //                textField.returnKeyType = UIReturnKeyDone;
        //        }
        
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
}



#pragma mark - UITableViewDataSource Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell* tableViewCell = [tableView cellForRowAtIndexPath:indexPath];
    [tableViewCell setSelected:NO animated:YES];
    
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}



@end
