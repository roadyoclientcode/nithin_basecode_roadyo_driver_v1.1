//
//  PubNubWrapper.m
//  Homappy
//
//  Created by Rahul Sharma on 29/02/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "VNHPubNubWrapper.h"
#import "PubNub.h"
#import "PNConfiguration.h"
#import "PubNub+Subscribe.h"
#import "XDKAirMenuController.h"
#import "UpdateBookingStatus.h"

@implementation VNHPubNubWrapper

static VNHPubNubWrapper *share;

/**********************************PubNub User Defined Methods*******************************************/
/**
 *  Shared Instance
 *
 *  @return returns Object of its own Class
 */
+ (id)sharedInstance
{
    if (!share) {
        share = [[self alloc] init];
    }
    return share;
}
- (instancetype)init
{
    if (self == [super init])
    {
        NSString *publishKey;
        NSString *subscribeKey;
        
        NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
        if ([[ud objectForKey:@"pubnubPublishKey"] length] && [[ud objectForKey:@"pubnubSubscribeKey"] length])
        {
            publishKey = [ud objectForKey:@"pubnubPublishKey"];
            subscribeKey = [ud objectForKey:@"pubnubSubscribeKey"];
        }
        else
        {
            publishKey = kPMDPubNubPublisherKey;
            subscribeKey = kPMDPubNubSubcriptionKey;
        }

        PNConfiguration *configuration = [PNConfiguration configurationWithPublishKey:publishKey
                                                                         subscribeKey:subscribeKey];
        configuration.uuid = [[NSUserDefaults standardUserDefaults] objectForKey:kNSUPatientEmailAddressKey];
        configuration.presenceHeartbeatValue = 10;
        configuration.presenceHeartbeatInterval = 5;
        self.client = [PubNub clientWithConfiguration:configuration];
    }
    return self;
}

/**
 *  Initiation of PubNub Method
 *  Call this method from AppDelagate, with Calling sharedInstance
 */
- (void)initiatePubNub
{
    self.my_channel = [[NSUserDefaults standardUserDefaults] objectForKey:kNSUDriverListnerChannelKey];
    [self.client subscribeToChannels: @[self.my_channel] withPresence:YES];
}

/*--------------------------------*/
#pragma marks - Subcription Methods
/*--------------------------------*/
/**
 *  Subscribe to MyChannels
 *  To the channel that I get on login or Signup
 */
- (void)subscribeToMyChannel
{
    self.my_channel = [[NSUserDefaults standardUserDefaults] objectForKey:kNSUDriverListnerChannelKey];
    [self.client addListener:self];
    [self.client subscribeToChannels: @[self.my_channel] withPresence:YES];
}
/**
 *  Subscribe with Channels
 *
 *  @param Channels array of Channels
 */
- (void)subscribeToChannels:(NSArray *)channels
{
    [self.client addListener:self];
    [self.client subscribeToChannels:channels withPresence:YES];
    self.subscribed_channels = [channels mutableCopy];
}

/*--------------------------------*/
#pragma marks - Publishing Methods
/*--------------------------------*/

/**
 *  Publish with Parameter to Channel
 *
 *  @param parameter Parameter Dict
 *  @param channel   toChannel
 */
- (void)publishWithParameter:(NSDictionary *)parameter
                   toChannel:(NSString *)channel {
    
    [self.client publish:parameter
               toChannel:channel
          storeInHistory:YES
          withCompletion:^(PNPublishStatus *status) {
              
              // Check whether request successfully completed or not.
              if (!status.isError) {
                  // Message successfully published to specified channel.
//                  NSLog(@"\n\nRequest successfully completed. Message successfully published to specified channel.\n\n");
//                  
//                  NSLog(@"\n\nPublish with Parameter : %@ \nTo Channel : %@\n\n",parameter,channel);
              }
              // Request processing failed.
              else {
                  // Handle message publish error. Check 'category' property to find out possible issue
                  // because of which request did fail.
                  //
                  // Request can be resent using: [status retry];
//                  NSLog(@"\n\nPublish error : %@\n\n",[status stringifiedCategory]);
              }
          }];
}
/**
 *  Publish with complition block
 *
 *  @param parameter       Parameter Dict
 *  @param channel         channel Publish to
 *  @param completionBlock complition block
 */
- (void)publishWithParameter:(NSDictionary *)parameter
                   toChannel:(NSString *)channel
              withCompletion:(void(^)(bool success))completionBlock{
    
    [self.client publish:parameter
               toChannel:channel
          storeInHistory:YES
          withCompletion:^(PNPublishStatus *status) {
              
              // Check whether request successfully completed or not.
              if (!status.isError) {
                  // Message successfully published to specified channel.
                  NSLog(@"\n\nRequest successfully completed. Message successfully published to specified channel.\n\n");
                  completionBlock(YES);
              }
              // Request processing failed.
              else
              {
                  
                  // Handle message publish error. Check 'category' property to find out possible issue
                  // because of which request did fail.
                  //
                  // Request can be resent using: [status retry];
                  NSLog(@"\n\nPublish error : %@\n\n",[status stringifiedCategory]);
                  completionBlock(NO);
              }
          }];
}
/**
 *  Publush with Parameter To Channel and Start timer
 *
 *  @param parameter  Parameter Dict
 *  @param channel    ChannelTo
 *  @param startTimer Start Timer YES if You want, else NO
 */
- (void)publishWithParameter:(NSDictionary *)parameter
                   toChannel:(NSString *)channel
                  startTimer:(BOOL)startTimer {
    
    [self.client publish:parameter
               toChannel:channel
          storeInHistory:YES
          withCompletion:^(PNPublishStatus *status) {
              
              // Check whether request successfully completed or not.
              if (!status.isError) {
                  // Message successfully published to specified channel.
                  NSLog(@"\n\nRequest successfully completed. Message successfully published to specified channel.\n\n");
                  
                  NSLog(@"\n\nPublish with Parameter : %@ \nTo Channel : %@\n\n",parameter,channel);
              }
              // Request processing failed.
              else {
                  
                  // Handle message publish error. Check 'category' property to find out possible issue
                  // because of which request did fail.
                  //
                  // Request can be resent using: [status retry];
                  NSLog(@"\n\nPublish error : %@\n\n",[status stringifiedCategory]);
              }
          }];
    
    self.params = [parameter mutableCopy];
    self.toChannel = channel;
    
    if (startTimer && !self.timerObj) {
        
        self.timerObj = [NSTimer scheduledTimerWithTimeInterval:5
                                                         target:self
                                                       selector:@selector(startTimer)
                                                       userInfo:nil
                                                        repeats:YES];
        NSLog(@"Timer started");
    }
}
/**
 *  Start timer
 */
- (void)startTimer {
    [self publishWithParameter:self.params
                     toChannel:self.toChannel
                    startTimer:YES];
}
/**
 *  Stop timer
 */
- (void)stopTimer {
    if (self.timerObj) {
        [self.timerObj invalidate];
        self.timerObj = nil;
    }
    NSLog(@"Tmer stopped");
}

/**********************************PubNub SDK Methods*******************************************/

/**
 @brief  Notify listener about new message which arrived from one of remote data object's live feed
 on which client subscribed at this moment.
 
 @param client  Reference on \b PubNub client which triggered this callback method call.
 @param message Reference on \b PNResult instance which store message information in \c data
 property.
 
 @since 4.0
 */
- (void)client:(PubNub *)client didReceiveMessage:(PNMessageResult *)message {
    
    // Handle new message stored in message.data.message
    if (message.data.actualChannel) {
        
        // Message has been received on channel group stored in
        // message.data.subscribedChannel
    }
    else {
        
        // Message has been received on channel stored in
        // message.data.subscribedChannel
    }
    NSLog(@"\n\nReceived message: %@ \non channel %@ \nat %@\n\n", message.data.message,
          message.data.subscribedChannel, message.data.timetoken);
    [self recievedMessageFromPubNub:message.data.message];
    if (self.delegate && [self.delegate respondsToSelector:@selector(receivedMessage:andChannel:)]) {
        
        [self.delegate receivedMessage:message.data.message
                            andChannel:message.data.subscribedChannel];
    }
}


- (void)client:(PubNub *)client didReceivePresenceEvent:(PNPresenceEventResult *)event {
    
    // Handle presence event event.data.presenceEvent (one of: join, leave, timeout,
    // state-change).
    if (event.data.actualChannel) {
        
        // Presence event has been received on channel group stored in
        // event.data.subscribedChannel
    }
    else {
        
        // Presence event has been received on channel stored in
        // event.data.subscribedChannel
    }if (self.delegate &&[self.delegate respondsToSelector:@selector(presenceMessage:andChannel:)]){
        [self.delegate presenceMessage:event.data.presence
                            andChannel:event.data.presenceEvent];
    }
    NSLog(@"Did receive presence event: %@", event.data.presenceEvent);
}
- (void)subscribeToMyPresenceChannel
{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"PresenceChn"]) {
        self.my_channel = [[NSUserDefaults standardUserDefaults] objectForKey:@"PresenceChn"];
        [self.client addListener:self];
        [self.client subscribeToChannels: @[self.my_channel] withPresence:YES];
    }
}
-(void)unSubscribeToMyPresenceChannel
{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"PresenceChn"]) {
        self.my_channel = [[NSUserDefaults standardUserDefaults] objectForKey:@"PresenceChn"];
        [self.client unsubscribeFromChannels:@[self.my_channel] withPresence:NO];
    }
    
}
- (void)client:(PubNub *)client didReceiveStatus:(PNSubscribeStatus *)status {
    
    if (status.category == PNUnexpectedDisconnectCategory) {
        // This event happens when radio / connectivity is lost
    }
    else if (status.category == PNConnectedCategory) {
        
        // Connect event. You can do stuff like publish, and know you'll get it.
        // Or just use the connected event to confirm you are subscribed for
        // UI / internal notifications, etc
        
    }
    else if (status.category == PNReconnectedCategory) {
        
        // Happens as part of our regular operation. This event happens when
        // radio / connectivity is lost, then regained.
    }
    else if (status.category == PNDecryptionErrorCategory) {
        
        // Handle messsage decryption error. Probably client configured to
        // encrypt messages and on live data feed it received plain text.
    }
    
}
-(void)recievedMessageFromPubNub:(NSDictionary*)messageDict
{
    NSLog(@"Message : %@", messageDict);
    
    if ([messageDict[@"a"] integerValue] == 11) { // New Booking
        NSMutableDictionary *mdict = [[NSMutableDictionary alloc] init];
        [mdict setObject:messageDict[@"e"] forKey:@"e"];
        [mdict setObject:messageDict[@"dt"] forKey:@"dt"];
//        if (messageDict[@"nt"]!= nil)
            [mdict setObject:messageDict[@"nt"] forKey:@"nt"];
        
        UIApplicationState state = [[UIApplication sharedApplication] applicationState];
        if (state == UIApplicationStateBackground || state == UIApplicationStateInactive)
        {
            //Do checking here.
            NSMutableDictionary *notificationDict = [[NSMutableDictionary alloc] init];
            [notificationDict setObject:mdict forKey:@"aps"];
            UILocalNotification* localNotification = [[UILocalNotification alloc] init];
            localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:1];
            localNotification.alertBody = @"Pubnub:Congratulations you got a new booking request";
            localNotification.userInfo = messageDict;
              localNotification.soundName = @"taxina.wav";
            localNotification.timeZone = [NSTimeZone defaultTimeZone];
            [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
        }
        else {
            
            XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
            if (![menu.currentViewController isKindOfClass:[UINavigationController class]]) {
                
                NSIndexPath *indexpath = [NSIndexPath indexPathForRow:0 inSection:0];
                [menu openMenuAnimated];
                
                dispatch_after(0.2, dispatch_get_main_queue(), ^(void){
                    [menu openViewControllerAtIndexPath:indexpath];
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"NewBooking" object:nil userInfo:messageDict];
                    });
                });
            }
            else
            {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"NewBooking" object:nil userInfo:messageDict];
            }
        }
    }
    else if ([messageDict[@"a"] integerValue] == 10){
        UpdateBookingStatus *updateBookingStatus = [UpdateBookingStatus sharedInstance];
        [updateBookingStatus stopUpdatingStatus];
        //bookingCancelled
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"bookingCancelled" object:nil userInfo:nil];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"Passenger cancelled booking" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([messageDict[@"a"] integerValue] == 2){
        
        
        [[NSUserDefaults standardUserDefaults] setInteger:[messageDict[@"s"] integerValue] forKey:kNSUDriverStatekey];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"DriverState" object:nil userInfo:nil];
        
        NSLog(@"message %@",messageDict);
    }
    else if([messageDict[@"a"]integerValue] == 3)
    {
        UpdateBookingStatus *updateBookingStatus = [UpdateBookingStatus sharedInstance];
        [updateBookingStatus stopUpdatingStatus];
    }
}

-(void)getOccupancyCountOnDriverChannel:(NSDictionary *)message onChannel:(NSString *)channel
{
    [self.client hereNowForChannel:channel withVerbosity:PNHereNowOccupancy completion:^(PNPresenceChannelHereNowResult * _Nullable result, PNErrorStatus * _Nullable status) {
        if (!status.isError)
        {
            if (result != nil)
            {
                NSInteger count = [[NSString stringWithFormat:@"%@", result.data.occupancy] integerValue];
                if (count > 0)
                {
                    NSLog(@"Passenger is Online");
                    [self publishWithParameter:message toChannel:channel];
                }
                else
                {
                    NSLog(@"Passenger Went Offline");
                }
            }
        }
        else
        {
            NSLog(@"Pubnub Occupancy Error = %@", [status errorData]);
        }
    }];
}

/**
 @brief      Notify listener about subscription state changes.
 @discussion This callback can fire when client tried to subscribe on channels for which it doesn't
 have access rights or when network went down and client unexpectedly disconnected.
 
 @param client Reference on \b PubNub client which triggered this callback method call.
 @param status  Reference on \b PNStatus instance which store subscriber state information.
 
 @since 4.0
 */
//- (void)client:(PubNub *)client didReceiveStatus:(PNSubscribeStatus *)status {
//    
//    if (status.category == PNUnexpectedDisconnectCategory) {
//        // This event happens when radio / connectivity is lost
//    }
//    else if (status.category == PNConnectedCategory) {
//        
//        // Connect event. You can do stuff like publish, and know you'll get it.
//        // Or just use the connected event to confirm you are subscribed for
//        // UI / internal notifications, etc
//        NSLog(@"\n\nPubnub Connected. \nSubscribed Channels : %@\n\n",[status subscribedChannels]);
//        
//    }
//    else if (status.category == PNReconnectedCategory) {
//        
//        // Happens as part of our regular operation. This event happens when
//        // radio / connectivity is lost, then regained.
//    }
//    else if (status.category == PNDecryptionErrorCategory) {
//        
//        // Handle messsage decryption error. Probably client configured to
//        // encrypt messages and on live data feed it received plain text.
//    }
//}

/**************************************** Unsubscribe from Channels ***************************************/

//Unsubscription process results arrive to listener
//which should adopt to PNObjectEventListener protocol and registered using:


/**
 *  Unsubscribe from My Channels
 */
- (void)unsubscribeFromMyChannel
{
    
    [self.client unsubscribeFromChannels:@[self.my_channel]
                            withPresence:NO];
}
/**
 *  Unsubscribe from All Channels
 */
- (void)unsubscribeFromAllChannel {
    
    [self.client unsubscribeFromChannels:self.subscribed_channels
                            withPresence:NO];
}
/**
 *  Unsubscribe from Custom Channels
 *
 *  @param channels Channels Array
 */
- (void)unsubscribeFromChannels:(NSArray *)channels {
    
    [self.client unsubscribeFromChannels:channels
                            withPresence:NO];
}
@end
