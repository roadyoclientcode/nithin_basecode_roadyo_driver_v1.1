//
//  OnBookingViewController.m
//  Roadyo
//
//  Created by Rahul Sharma on 01/05/14.
//  Copyright (c) 2014 3Embed. All rights reserved.
//

#import "OnBookingViewController.h"
#import "CustomNavigationBar.h"
#import "UIImageView+WebCache.h"
#import "SDWebImageDownloader.h"
#import "BookingHistoryViewController.h"
#import "DirectionService.h"
#import "BookingDetail.h"
#import "LocationTracker.h"
#import "VNHPubNubWrapper.h"
#import "UpdateBookingStatus.h"
#import "MathController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "MTGoogleMapCustomURLInteraction.h"
#import "NewBookingHistoryViewController.h"
#import "XDKAirMenuController.h"
#import "CustomSliderView.h"
#import "LocationServicesViewController.h"
#import "MannualInvoiceView.h"
#import <CoreLocation/CoreLocation.h>


@interface OnBookingViewController ()<CustomNavigationBarDelegate,GMSMapViewDelegate,CLLocationManagerDelegate,UIActionSheetDelegate,MannualInvoiceViewDelegate,CustomSliderViewDelegate>
{
    GMSMapView *mapView_;
    GMSGeocoder *geocoder_;
    NSMutableArray *waypoints_;
    NSMutableArray *waypointStrings_;
    NSString *proLatLong;
    NSString *customerLatLong;
    NSString *ETATime;
    NSString *distanceGoogle;
    NSTimer *timerJobStarted;
    int timeSec, timeMin, timeHr;
    UIImage *markerIcon;
    NSString * dropLatAdded;
    NSString * dropLongAdded;
    UIAlertView *phoneCall;
    BOOL isbookingInvoiceOpen;
     CLLocationManager *locationManagerr;
}

@property(assign,nonatomic) int statusButtonTag;
@property(nonatomic,assign) BOOL isUpdatedLocation;
@property(nonatomic,assign) float currentLatitude;
@property(nonatomic,assign) float currentLongitude;
@property(nonatomic,assign) BOOL isDriverArrived;
@property(nonatomic,assign) BOOL isPathPlotted;

@property(nonatomic,assign) CLLocationCoordinate2D destinationCoordinates;
@property(nonatomic,assign) CLLocationCoordinate2D pickupCoordinates;
@property(nonatomic,assign) CLLocationCoordinate2D previouCoord;
@property(nonatomic,strong) GMSMarker *currentLocationMarker;
@property(nonatomic,strong) CLLocationManager *locationManager;
@property(nonatomic,strong) GMSMarker *destinationMarker;
@property(nonatomic,strong) NSMutableDictionary *allMarkers;
@property(nonatomic,strong) NSString *destinationLocation;
@property(nonatomic,strong) NSString *destinationArea;
@property(nonatomic,strong) NSString *startLocation;
@property(strong,nonatomic) IBOutlet UIView *bottomView;
@property(strong,nonatomic) IBOutlet UIView *customMapView;
@property(strong,nonatomic) IBOutlet UIView *topView;
@property(strong,nonatomic) NSString *statusButtonTitle;
@property(strong,nonatomic) NSString *approxPrice;
@property(strong,nonatomic) NSTimer *statusUpdateTimer;
@property(nonatomic,assign) PubNubStreamAction driverState;
@property (strong, nonatomic) UIImage *carImage;
@property LocationTracker * locationTracker;
@property (nonatomic) NSTimer* locationUpdateTimer;

@property (weak, nonatomic) IBOutlet UIView *sliderView;
@property (strong, nonatomic) CustomSliderView *customSliderView;
@property (weak, nonatomic) IBOutlet UIView *sliderOuterView;

@end

@implementation OnBookingViewController
@synthesize customMapView;
@synthesize passDetail;
static NSInteger distanceBtween;


#pragma mark-view Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    waypoints_ = [[NSMutableArray alloc]init];
    waypointStrings_ = [[NSMutableArray alloc]initWithCapacity:2];
    //self.navigationController.navigationBarHidden = NO;
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:37.778376
                                                            longitude:-122.409853
                                                                 zoom:17];
    //UIView *map = [[UIView alloc] initWithFrame:CGRectMake(0, 200, 320, 300)];
    mapView_ = [GMSMapView mapWithFrame:CGRectMake(0, 0, 320, 568) camera:camera];
    mapView_.delegate = self;
    mapView_.settings.myLocationButton = NO;
    mapView_.padding = UIEdgeInsetsMake(90,40, 0, 0);
    [self.view addSubview: mapView_];
    [self.view bringSubviewToFront:self.AddessDetailsView];
    [self addShadowToView:self.AddessDetailsView];
    [self.view bringSubviewToFront:self.wazeBtn];
    [self.view bringSubviewToFront:self.googleMapsBtn];
    [self.view bringSubviewToFront:self.sliderOuterView];
    [[NSUserDefaults standardUserDefaults]setObject:flStrForStr(passDetail[@"bid"])
                                             forKey:@"bid"];
    if (_LoadFrom == 1) {
        _bookingStatus = 0;
    }
    XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
    [menu disablePanGesture:YES];
    if (IS_IPHONE_5) {
    }
    else{
        customMapView.frame =CGRectMake(0, 161, 320, 300);
        viewStatus.frame = CGRectMake(0, 141+300, 320, 300);
    }
    
    self.distanceLbl.hidden = YES;
    NSString *strImageUrl = [NSString stringWithFormat:@"%@%@",baseUrlForOriginalImage,passDetail[@"pPic"]];
    
    [imgPass setImageWithURL:[NSURL URLWithString:strImageUrl]
            placeholderImage:[UIImage imageNamed:@"doctor_image_thumbnail.png"]
                   completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
                   }];
    if (passDetail.count == 0) {
        [self getBookingInfo];
        
    }else
    {
        [self updateUI];
        //         [self createTopView];
    }
    [self updateUI];
    
    self.googleMapsBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.wazeBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    _currentLocationMarker  = [[GMSMarker alloc] init];
    
    _customSliderView = [[[NSBundle mainBundle] loadNibNamed:@"NewSliderView" owner:self options:nil] lastObject];
    _customSliderView.delegate = self;
    _customSliderView.sliderTitle.text = @"Slide To Action";
    CGRect frame = _customSliderView.frame;
    frame.size.width = _sliderView.frame.size.width;
    frame.size.height = _sliderView.frame.size.height;
    _customSliderView.frame = frame;
    [_sliderView addSubview:_customSliderView];
    
    
    [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%d",_bookingStatus] forKey:@"currentBookingStatus"];
    
    //check for booking status
    if (_bookingStatus == kNotificationTypeBookingOnTheWay) {
        
        _statusButtonTitle = NSLocalizedString(@"I HAVE ARRIVED", @"I HAVE ARRIVED");
        _customSliderView.sliderTitle.text = NSLocalizedString(@"I HAVE ARRIVED", @"I HAVE ARRIVED");
        self.title = NSLocalizedString(@"ON THE WAY", @"ON THE WAY");
        
        _statusButtonTag = 500;
        [self changePickUpAddressOnUI];
    }
    else if (_bookingStatus == kNotificationTypeBookingArrived) {
        
        self.navigationItem.leftBarButtonItem = nil;
        self.navigationItem.hidesBackButton = YES;
        _statusButtonTitle = NSLocalizedString(@"BEGIN TRIP", @"BEGIN TRIP");
        _customSliderView.sliderTitle.text = NSLocalizedString(@"BEGIN TRIP", @"BEGIN TRIP");
        self.title = NSLocalizedString(@"I HAVE ARRIVED", @"I HAVE ARRIVED");
        _statusButtonTag = 600;
        
        [self changeDropAddressOnUI];
        _isDriverArrived = YES;
        
    }
    else if (_bookingStatus == kNotificationTypeBookingBeginTrip) {
        
        _statusButtonTitle = NSLocalizedString(@"PASSENGER DROPPED", @"PASSENGER DROPPED");
        _customSliderView.sliderTitle.text = NSLocalizedString(@"PASSENGER DROPPED", @"PASSENGER DROPPED");
        self.title = NSLocalizedString(@"JOURNEY STARTED", @"JOURNEY STARTED");
        _statusButtonTag = 700;
        [self changeDropAddressOnUI];
        _isDriverArrived = YES;
        self.distanceLbl.hidden = NO;
    }
    else {
        _statusButtonTitle = NSLocalizedString(@"ON THE WAY", @"ON THE WAY");
        _customSliderView.sliderTitle.text = NSLocalizedString(@"ON THE WAY", @"ON THE WAY");
        self.title = @"";
        _statusButtonTag = 400;
        [self changePickUpAddressOnUI];
        
    }
    
    btnArrival.tag = _statusButtonTag;
    //    [self createBottomView];
    
    
    
    
    
    
    
    UIButton *cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelButton.frame = CGRectMake(0, 0, 50, 44);
    [cancelButton setTitle:NSLocalizedString(@"CANCEL", @"CANCEL") forState:UIControlStateNormal];
    [cancelButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
    [cancelButton setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateHighlighted];
    cancelButton.titleLabel.font = [UIFont fontWithName:Robot_Light size:12];
    [cancelButton setBackgroundImage:[UIImage imageNamed:@"signup_btn_back_bg_on"] forState:UIControlStateHighlighted];
    [cancelButton addTarget:self action:@selector(cancelBookingClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    if (_bookingStatus == kNotificationTypeBookingOnTheWay || _bookingStatus == kNotificationTypeBookingArrived) {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:cancelButton];
    }else if (_bookingStatus == 0) {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:cancelButton];
        // self.navigationItem.backBarButtonItem.title = @"BACK";
        self.navigationItem.hidesBackButton = NO;
    }
    else
    {
        self.navigationItem.hidesBackButton = YES;
    }
    
    
    
    
    
    
    UIButton *mapButton = [UIButton buttonWithType:UIButtonTypeCustom];
    mapButton.frame = CGRectMake(0, 0, 50, 44);
    [mapButton setTitle:NSLocalizedString(@"MAPS", @"MAPS") forState:UIControlStateNormal];
    [mapButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
    [mapButton setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateHighlighted];
    mapButton.titleLabel.font = [UIFont fontWithName:Robot_Light size:12];
    [mapButton setBackgroundImage:[UIImage imageNamed:@"signup_btn_back_bg_on"] forState:UIControlStateHighlighted];
    [mapButton addTarget:self action:@selector(mapsButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    //plot path on map
    LocationTracker *tracker = [LocationTracker sharedInstance];
    [self setStartLocationCoordinates:tracker.lastLocaiton.coordinate.latitude Longitude:tracker.lastLocaiton.coordinate.longitude];
    [self startUpdatingLocationToServer];
    locationManagerr = [[CLLocationManager alloc] init];
    locationManagerr.distanceFilter = kCLDistanceFilterNone;
    locationManagerr.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManagerr startUpdatingLocation];
    [locationManagerr requestAlwaysAuthorization]; //Note this one
    
    float Latitude;
    float Longitue;
    if ([passDetail[@"pickLat"] integerValue] == 0|| [passDetail[@"pickLong"] integerValue] == 0) {
        Latitude = locationManagerr.location.coordinate.latitude;
        Longitue = locationManagerr.location.coordinate.longitude;
    }
    else{
        Latitude = [passDetail[@"pickLat"] floatValue];
        Longitue = [passDetail[@"pickLong"] floatValue];
    }
    
    [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%f",Latitude] forKey:@"picklat"];
    [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%f",Longitue] forKey:@"picklog"];
}

-(CLLocationCoordinate2D)getLocation {
    
    CLLocationManager *locationManager = [[CLLocationManager alloc] init] ;
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    [locationManager startUpdatingLocation];
    [locationManager stopUpdatingLocation];
    CLLocation *location = [locationManager location];
    CLLocationCoordinate2D coordinate = [location coordinate];
    return coordinate;
    
}
-(void) updateUI
{
    self.DriverTxtLbl.text = [NSString stringWithFormat:@"%@ %@",passDetail[@"fName"],passDetail[@"lName"]].uppercaseString;
    if ([passDetail[@"payType"] integerValue] == 1) {
        self.cashOrCard.text = @"CARD";
        self.CashImage.image = [UIImage imageNamed:@"Grupo.png"];
    }else
    {
        self.cashOrCard.text = @"CASH";
        self.CashImage.image = [UIImage imageNamed:@"Layer.png"];
    }
    self.pickUpAndDropAddressLbl.text = [NSString stringWithFormat:@"%@ %@",passDetail[@"addr1"],passDetail[@"addr2"]];
    NSString *bid = NSLocalizedString(@"BID:", @"BID:");
    self.BIDLbl.text = [NSString stringWithFormat:@"%@ %@",bid,passDetail[@"bid"]];
    self.timeLbl.text = [self changeDateFormateToTime:passDetail[@"pickupDt"]]; //pickupDt
    
//    self.distanceLbl.text =[NSString stringWithFormat:@"%0.002f %@",[flStrForStr(passDetail[@"dis"]) floatValue],[Helper getCurrencyUnit]];
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    LocationTracker *tracker = [LocationTracker sharedInstance];
    tracker.distance = 0;
    if ([ud objectForKey:kNSUDriverDistanceTravalled])
    {
//        self.distanceLbl.text = [NSString stringWithFormat:@"%.2f",[[ud objectForKey:kNSUDriverDistanceTravalled] floatValue] ];
//        tracker.distance = [[ud objectForKey:kNSUDriverDistanceTravalled] floatValue];
    }
    tracker.distanceCallback = ^(float totalDistance, CLLocation *location){
//        self.distanceLbl.text = [MathController stringifyDistance:totalDistance];
        float latitude = location.coordinate.latitude;
        float longitude = location.coordinate.longitude;
        proLatLong = [NSString stringWithFormat:@"%f,%f", latitude,longitude];
        customerLatLong = [NSString stringWithFormat:@"%@,%@", passDetail[@"pickLat"], passDetail[@"pickLong"]];
        [ud setObject:[NSNumber numberWithFloat:totalDistance] forKey:kNSUDriverDistanceTravalled];
        [ud synchronize];
    };
}

- (NSString *)changeDateFormateToTime:(NSString *)originalDate{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];//2012-11-22 10:19:04
    
    
    NSDate *date = [dateFormatter dateFromString:originalDate];
    
    [dateFormatter setDateFormat:@"hh:mm a"];//05:30 AM
    NSString *formattedTimeString = [dateFormatter stringFromDate:date];
    
    return formattedTimeString;
}

- (void) addShadowToView:(UIView *)view
{
    // border radius
    [view.layer setCornerRadius:10.0f];
    
    // border
    [view.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [view.layer setBorderWidth:1.0f];
    
    // drop shadow
    [view.layer setShadowColor:[UIColor blackColor].CGColor];
    [view.layer setShadowOpacity:0.5];
    [view.layer setShadowRadius:2.0];
    [view.layer setShadowOffset:CGSizeMake(1.0, 2.0)];
}
-(void)startUpdatingLocationToServer{
    
    self.locationTracker = [LocationTracker sharedInstance];
    [self.locationTracker startLocationTracking];
    //Send the best location to server every 60 seconds
    //You may adjust the time interval depends on the need of your app.
    NSTimeInterval time = 4.0;
    self.locationUpdateTimer =
    [NSTimer scheduledTimerWithTimeInterval:time
                                     target:self
                                   selector:@selector(updateLocation)
                                   userInfo:nil
                                    repeats:YES];
}

-(void)updateLocation
{
    [self.locationTracker updateLocationToServer];
}




-(void)bookingCancelledByPassenger
{
    UpdateBookingStatus *updateBookingStatus = [UpdateBookingStatus sharedInstance];
    [updateBookingStatus stopUpdatingStatus];
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isBooked"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if ([ud objectForKey:@"onTheWayPressed"]) {
        [ud removeObjectForKey:@"onTheWayPressed"];
    }
    if ([ud objectForKey:@"iHaveArrivedPressed"]) {
        [ud removeObjectForKey:@"iHaveArrivedPressed"];
    }
    if ([ud objectForKey:@"beginTripPressed"]) {
        [ud removeObjectForKey:@"beginTripPressed"];
    }
    if ([ud objectForKey:@"passengerDroppedPressed"]) {
        [ud removeObjectForKey:@"passengerDroppedPressed"];
    }
    
    [[NSUserDefaults standardUserDefaults]setObject:@"0" forKey:@"bid"];
    
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)getBookingInfo{
    
    BookingDetail *bookingDetail = [BookingDetail sharedInstance];
    bookingDetail.callback = ^(NSDictionary *bookingDetail,BOOL sucess,Errorhandler *handler){
        if (sucess) {
            
            passDetail = [[NSMutableDictionary alloc] initWithDictionary:bookingDetail];
            [self updateUI];
            [self updateBookingDetailview];
        }
        else {
            [self getBookingInfo];
        }
    };
    
    [bookingDetail sendBookingDetailRequest];
    
}
-(void)viewWillAppear:(BOOL)animated{
    
    if ([self.navigationController isNavigationBarHidden]) {
        NSLog(@"navigation is hidden");
    }
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [mapView_ addObserver:self
               forKeyPath:@"myLocation"
                  options:NSKeyValueObservingOptionNew
                  context:NULL];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(bookingCancelledByPassenger) name:@"bookingCancelled" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkBookingStatus) name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkBookingStatus) name:@"AddressChanged" object:nil];
    
    // Ask for My Location data after the map has already been added to the UI.
    dispatch_async(dispatch_get_main_queue(), ^{
        mapView_.myLocationEnabled = YES;
    });
}
- (void)viewWillDisappear:(BOOL)animated{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"bookingCancelled" object:nil];
    
    [_allMarkers removeAllObjects];
    
    _isUpdatedLocation = NO;
    @try {
        [mapView_ removeObserver:self
                      forKeyPath:@"myLocation"
                         context:NULL];
    }
    @catch (NSException *exception) {
        
    }
    
}
- (void)viewDidDisappear:(BOOL)animated
{
    UpdateBookingStatus *updateBookingStatus = [UpdateBookingStatus sharedInstance];
    [updateBookingStatus stopUpdatingStatus];
}

- (void) viewDidAppear:(BOOL)animated
{
    [self startUpdatingDistance];
    [self checkLocationServices];
    if (IS_SIMULATOR)
    {
        _currentLatitude = 13.028866;
        _currentLongitude = 77.589760;
        
        CLLocationCoordinate2D locationCord = CLLocationCoordinate2DMake(_currentLatitude,_currentLongitude);
        
        mapView_.camera = [GMSCameraPosition cameraWithTarget:locationCord
                                                         zoom:14];
        GMSMarker *marker = [[GMSMarker alloc] init];
        marker.position = CLLocationCoordinate2DMake(_currentLatitude, _currentLongitude);
        marker.title = NSLocalizedString(@"It's Me", @"It's Me");
        marker.snippet = NSLocalizedString(@"Current Location", @"Current Location");
        marker.map = mapView_;
        
    }
    else {
        
        NSLog(@"latitude %f",[mapView_.myLocation coordinate].latitude);
        //[self.view bringSubviewToFront:viewPassDetail];
        //[self.view bringSubviewToFront:_bottomView];
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0,0,10,20)];
        UIImageView *carIcon = [[UIImageView alloc] init];
        
        NSString *strImageUrl = [NSString stringWithFormat:@"%@%@",baseUrlForOriginalImage,[[NSUserDefaults standardUserDefaults]objectForKey:kNSURoadyoCarImage]];
        NSLog(@"strImageUrl >>>>>>>>>>>>>>>>%@ ",strImageUrl);
        CGRect frame = carIcon.frame;
        frame.size.height = 20;
        frame.size.width = 10;
        frame.origin.x = 0;
        frame.origin.y = 0;
        carIcon.frame = frame;
        [carIcon sd_setImageWithURL:[NSURL URLWithString:strImageUrl]
                   placeholderImage:nil
                          completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
                              
                              if(image)
                              {
                                  
                                  image = [self imageWithImage:image scaledToSize:[self makeSize:image.size fitInSize:CGSizeMake(50, 50)]];
                                  _currentLocationMarker.icon = image;
                                  [view addSubview:carIcon];
                                  _carImage = image;
                                  return;
                              }
                          }];
        
        NSLog(@"CAR IMAGE1: %@", _carImage);
        _currentLocationMarker.icon = _carImage;
    }
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if ([ud objectForKey:@"passengerDroppedPressed"]) {
        [self performSegueWithIdentifier:@"GoToBookingHistory" sender:self];
    }
    [self getCurrentLocation];
}

#pragma mark- Custom Methods

- (void) addCustomNavigationBar{
    
    CustomNavigationBar *customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, 320, 64)];
    customNavigationBarView.tag = 78;
    customNavigationBarView.delegate = self;
    [customNavigationBarView setTitle:NSLocalizedString(@"Pick Up", @"Pick Up")];
    [customNavigationBarView setLeftBarButtonTitle:NSLocalizedString(@"Back", @"Back")];
    [self.view addSubview:customNavigationBarView];
    
}

- (CGSize)makeSize:(CGSize)originalSize fitInSize:(CGSize)boxSize
{
    float widthScale = 0;
    float heightScale = 0;
    widthScale = boxSize.width/originalSize.width;
    heightScale = boxSize.height/originalSize.height;
    float scale = MIN(widthScale, heightScale);
    CGSize newSize = CGSizeMake(originalSize.width *scale, originalSize.height *scale);
    return newSize;
}
-(UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

/**
 *  create a bottom button
 */
-(void)createBottomView {
    
    _bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, [UIScreen mainScreen].bounds.size.height-40, 320, 40)];
    [_bottomView setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:_bottomView];
    
    UIButton *buttonDriverState = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonDriverState.frame = CGRectMake(0, 0, 320, 40);
    buttonDriverState.tag = _statusButtonTag;
    
    [buttonDriverState setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateHighlighted];
    [buttonDriverState setTitleColor:UIColorFromRGB(0x000000) forState:UIControlStateNormal];
    [buttonDriverState setBackgroundImage:[UIImage imageNamed:@"arrive_status_bg"] forState:UIControlStateNormal];
    [buttonDriverState setBackgroundImage:[UIImage imageNamed:@"arrive_status_bg_selected.png"] forState:UIControlStateHighlighted];
    
    [buttonDriverState addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    [buttonDriverState setTitle:_statusButtonTitle forState:UIControlStateNormal];
    [_bottomView addSubview:buttonDriverState];
}

/**
 *  create a view with all the booking details
 */

-(void)createTopView
{
    _topView = [[UIView alloc] initWithFrame:CGRectMake(0, 65, 320, 145)];
    [_topView setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:_topView];
    [self addShadowToView:_topView];
    
    //    UIImageView *profileImageView = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, 100, 100)];
    //    profileImageView.image = [UIImage imageNamed:@"Icon-60"];
    //    profileImageView.tag = 100;
    //    [_topView addSubview:profileImageView];
    
    NSString *strImageUrl = [NSString stringWithFormat:@"%@%@",baseUrlForXXHDPIImage,passDetail[@"pPic"]];
    
    //    [profileImageView setImageWithURL:[NSURL URLWithString:strImageUrl]
    //                placeholderImage:[UIImage imageNamed:@"doctor_image_thumbnail.png"]
    //                       completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
    //                       }];
    
    //Name
    UILabel *labelPassengerName = [[UILabel alloc] initWithFrame:CGRectMake(5, 5,40,20)];
    [Helper setToLabel:labelPassengerName Text:NSLocalizedString(@"Name :", @"Name :") WithFont:Robot_Bold FSize:10 Color:UIColorFromRGB(0x333333)];
    // [_topView addSubview:labelPassengerName];
    
    
    UILabel *labelPassengerNameValue = [[UILabel alloc] initWithFrame:CGRectMake(5, 5+8, 200, 20)];
    labelPassengerNameValue.tag = 101;
    labelPassengerNameValue.backgroundColor = [UIColor blueColor];
    [Helper setToLabel:labelPassengerNameValue Text:[NSString stringWithFormat:@"%@ %@",passDetail[@"fName"],passDetail[@"lName"]].uppercaseString WithFont:Robot_Regular FSize:15 Color:UIColorFromRGB(0x333333)];
    CGRect labelsize =  [labelPassengerNameValue.text boundingRectWithSize:labelPassengerNameValue.frame.size options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont fontWithName:Robot_Regular size:15]} context:nil];
    CGRect frame = labelPassengerNameValue.frame;
    frame.size.width = labelsize.size.width;
    labelPassengerNameValue.frame = frame;
    [_topView addSubview:labelPassengerNameValue];
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(labelPassengerNameValue.frame.origin.x + labelPassengerNameValue.frame.size.width + 5, 5, 30, 30)];
    button.tag = 102;
    [button setBackgroundImage:[UIImage imageNamed:@"proscreen_call_off.png"] forState:UIControlStateNormal];
    [button setBackgroundImage:[UIImage imageNamed:@"proscreen_call_on.png"] forState:UIControlStateHighlighted];
    [button addTarget:self action:@selector(callButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [_topView addSubview:button];
    
    UIButton *directionButton = [[UIButton alloc] initWithFrame:CGRectMake(270,55, 35, 35)];
    [directionButton setBackgroundImage:[UIImage imageNamed:@"pro_screen_google_direction_off@2x.png"] forState:UIControlStateNormal];
    [directionButton setBackgroundImage:[UIImage imageNamed:@"pro_screen_google_direction_off@2x.png"] forState:UIControlStateHighlighted];
    [directionButton addTarget:self action:@selector(mapsButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [_topView addSubview:directionButton];
    //waze button
    
    UIButton *but= [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [but addTarget:self action:@selector(wazeButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [but setFrame:CGRectMake(270,6, 40, 40)];
    [but setBackgroundImage:[UIImage imageNamed:@"pro_screen_waze_direction_off@2x.png"] forState:UIControlStateNormal];
    [but setBackgroundImage:[UIImage imageNamed:@"pro_screen_waze_direction_off@2x.png"] forState:UIControlStateHighlighted];
    
    //[but setTitle:NSLocalizedString(@"WAZE", @"WAZE") forState:UIControlStateNormal];
    [but setExclusiveTouch:YES];
    
    // if you like to add backgroundImage else no need
    //    [but setbackgroundImage:[UIImage imageNamed:@"XXX.png"] forState:UIControlStateNormal];
    
    [_topView addSubview:but];
    
    
    //    UIButton *wazeMaps = [[UIButton alloc] initWithFrame:CGRectMake(button.frame.origin.x+100 + button.frame.size.width + 5, 5, 60, 34)];
    //    wazeMaps.tag = 129;
    //    [wazeMaps setTitle:NSLocalizedString(@"WAZE", @"WAZE") forState:UIControlStateNormal];
    //    [wazeMaps setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
    //    [wazeMaps setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateHighlighted];
    //    wazeMaps.titleLabel.font = [UIFont fontWithName:Robot_Light size:12];
    //
    ////    [wazeMaps setBackgroundImage:[UIImage imageNamed:@"pro_screen_direction_off.png"] forState:UIControlStateNormal];
    ////    [wazeMaps setBackgroundImage:[UIImage imageNamed:@"pro_screen_direction_on.png"] forState:UIControlStateHighlighted];
    //    [wazeMaps addTarget:self action:@selector(wazeButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    //    [_topView addSubview:wazeMaps];
    //Address
    UILabel *labelAddress = [[UILabel alloc] initWithFrame:CGRectMake(5,CGRectGetMaxY(labelPassengerName.frame)+10+5,250, 13)];
    [Helper setToLabel:labelAddress  Text:NSLocalizedString(@"PICK UP :", @"PICK UP :")WithFont:Robot_Bold FSize:14 Color:UIColorFromRGB(0x333333)];
    labelAddress.tag = 210;
    //    UILabel *payTypeLabel = [[UILabel alloc] initWithFrame:CGRectMake(55, CGRectGetMaxY(labelPassengerName.frame)+10+5,90, 13)];
    //    payTypeLabel.tag = 404;
    if ([passDetail[@"payType"] integerValue] == 1) {
        labelAddress.text = @"PICK UP : (CARD)";
    }else {
        labelAddress.text = @"PICK UP : (CASH)";
    }
    
    [_topView addSubview:labelAddress];
    //    [_topView addSubview:payTypeLabel];
    
    
    UILabel *labelAddressValue = [[UILabel alloc] initWithFrame:CGRectMake(5, CGRectGetMaxY(labelAddress.frame)+6, 250, 82)];
    labelAddressValue.numberOfLines = 0;
    labelAddressValue.tag = 211;
    [Helper setToLabel:labelAddressValue Text:[NSString stringWithFormat:@"%@ %@",passDetail[@"addr1"],passDetail[@"addr2"]] WithFont:Robot_Regular FSize:15 Color:UIColorFromRGB(0x333333)];
    [labelAddressValue sizeToFit];
    //    CGRect labelAddressSize = [labelAddressValue.text boundingRectWithSize:labelAddressValue.frame.size options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont fontWithName:Robot_Regular size:14]}  context:nil];
    //    frame = labelAddressValue.frame;
    //    frame.origin.y = CGRectGetMaxY(labelAddress.frame);
    //    frame.size.height = labelAddressSize.size.height + 15;
    // //   frame.size.width = labelAddressSize.size.width;
    //    labelAddressValue.frame = frame;
    
    [_topView addSubview:labelAddressValue];
    
    
    //distance strip view
    UIView *distanceTimeView = [[UIView alloc] initWithFrame:CGRectMake(0,  _topView.frame.size.height - 40 + 10, 320, 30)];
    distanceTimeView.tag = 212;
    distanceTimeView.backgroundColor = [UIColor grayColor];
    [_topView addSubview:distanceTimeView];
    
    frame = mapView_.frame;
    frame.origin.y = distanceTimeView.frame.origin.y + distanceTimeView.frame.size.height;
    frame.size.height = [UIScreen mainScreen].bounds.size.height - frame.origin.y;
    mapView_.frame = frame;
    
    [self.view bringSubviewToFront:_sliderOuterView];
    
    
    //distance
    UILabel *labelDistance = [[UILabel alloc] initWithFrame:CGRectMake(5,0, 100, 30)];
    labelDistance.tag = 300;
    NSString *bid = NSLocalizedString(@"BID:", @"BID:");
    [Helper setToLabel:labelDistance Text:[NSString stringWithFormat:@"%@ %@",bid,passDetail[@"bid"]] WithFont:Robot_Light FSize:15 Color:UIColorFromRGB(0xffffff)];
    
    [distanceTimeView addSubview:labelDistance];
//    self.distanceLbl.text =[NSString stringWithFormat:@"%0.002f %@",[flStrForStr(passDetail[@"dis"]) floatValue],[Helper getCurrencyUnit]];
    UILabel *labelTimeToReachTimer = [[UILabel alloc] initWithFrame:CGRectMake(150, 0, 150, 30)];
    labelTimeToReachTimer.tag = 420;
    labelTimeToReachTimer.font = [UIFont fontWithName:Robot_Regular size:15];
    labelTimeToReachTimer.textColor = [UIColor whiteColor];
    [labelTimeToReachTimer setTextAlignment:NSTextAlignmentRight];
    [distanceTimeView addSubview:labelTimeToReachTimer];
    
    
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    LocationTracker *tracker = [LocationTracker sharedInstance];
    tracker.distance = 0;
    if ([ud objectForKey:kNSUDriverDistanceTravalled]) {
//        self.distanceLbl.text = [NSString stringWithFormat:@"%.2f",[[ud objectForKey:kNSUDriverDistanceTravalled] floatValue] ];
        tracker.distance = [[ud objectForKey:kNSUDriverDistanceTravalled] floatValue];
    }
    tracker.distanceCallback = ^(float totalDistance, CLLocation *location){
        
//        self.distanceLbl.text = [MathController stringifyDistance:totalDistance];
        float latitude = location.coordinate.latitude;
        float longitude = location.coordinate.longitude;
        proLatLong = [NSString stringWithFormat:@"%f,%f", latitude,longitude];
        customerLatLong = [NSString stringWithFormat:@"%@,%@", passDetail[@"pickLat"], passDetail[@"pickLong"]];
        [ud setObject:[NSNumber numberWithFloat:totalDistance] forKey:kNSUDriverDistanceTravalled];
        [ud synchronize];
    };
}

-(void)updateBookingDetailview
{
    UILabel *labelPassengerName = (UILabel*)[_topView viewWithTag:101];
    CGRect frame = labelPassengerName.frame;
    frame.origin.x = 5;
    frame.origin.y = 13;
    frame.size.width = 200;
    frame.size.height = 20;
    labelPassengerName.frame = frame;
    // labelPassengerName.text = [NSString stringWithFormat:@"%@ %@",passDetail[@"fName"],passDetail[@"lName"]].uppercaseString;
    [Helper setToLabel:labelPassengerName Text:[NSString stringWithFormat:@"%@ %@",passDetail[@"fName"],passDetail[@"lName"]].uppercaseString WithFont:Robot_Regular FSize:15 Color:UIColorFromRGB(0x333333)];
    CGRect labelsize =  [labelPassengerName.text boundingRectWithSize:labelPassengerName.frame.size options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont fontWithName:Robot_Regular size:15]} context:nil];
    frame = labelPassengerName.frame;
    frame.size.width = labelsize.size.width;
    labelPassengerName.frame = frame;
    [_topView addSubview:labelPassengerName];
    
    UIButton *buttonMobile = (UIButton*)[_topView viewWithTag:102];
    frame = buttonMobile.frame;
    frame.origin.x = labelPassengerName.frame.origin.x + labelPassengerName.frame.size.width + 5;
    frame.origin.y = 5;
    frame.size.width = 30;
    frame.size.height = 30;
    buttonMobile.frame = frame;
    // [buttonMobile setTitle:passDetail[@"mobile"] forState:UIControlStateNormal];
    
    
    UILabel *labelAddress = (UILabel*)[_topView viewWithTag:211];
    labelAddress.text = [NSString stringWithFormat:@"%@ %@",passDetail[@"addr1"],passDetail[@"addr2"]];
    frame = labelAddress.frame;
    frame.origin.x = 5;
    frame.origin.y = 67;
    frame.size.width = 252;
    frame.size.height = 82;
    labelAddress.frame = frame;
    
    
    [labelAddress sizeToFit];
    //    frame = labelAddress.frame;
    //    frame.origin.x = 5;
    //    frame.origin.y = 61;
    //    frame.size.width = 250;
    //    frame.size.height = 82;
    //    labelAddress.frame = frame;
    //    CGRect labelAddressSize = [labelAddress.text boundingRectWithSize:labelAddress.frame.size options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont fontWithName:Robot_Regular size:14]}  context:nil];
    //    frame = labelAddress.frame;
    //    frame.origin.y = 61;
    //    frame.size.height = labelAddressSize.size.height + 15;
    //    //   frame.size.width = labelAddressSize.size.width;
    //    labelAddress.frame = frame;
    
    
    UIView *bookingIdView = [_topView viewWithTag:212];
    UILabel *labelbookingId = (UILabel*)[bookingIdView viewWithTag:300];
    NSString *bid = NSLocalizedString(@"BID:", @"BID:");
    labelbookingId.text = [NSString stringWithFormat:@"%@ %@",bid,passDetail[@"bid"]];
    
//    self.distanceLbl.text =[NSString stringWithFormat:@"%0.002f %@",[flStrForStr(passDetail[@"dis"]) floatValue],[Helper getCurrencyUnit]];
    
}




/**
 *  changes address when drivers reached at pickup location and shows drop location
 */
-(void)changeDropAddressOnUI{
    
    UILabel *labelAddress = (UILabel*)[_topView viewWithTag:210];
    UILabel *labelAddressValue = (UILabel*)[_topView viewWithTag:211];
    //    self.title = @"I HAVE ARRIVED";
    labelAddress.text = NSLocalizedString(@"DROP :", @"DROP :");
    if ([passDetail[@"payType"] integerValue] == 1) {
        labelAddress.text = @"DROP : (CARD)";
    }else {
        labelAddress.text = @"DROP : (CASH)";
    }
    //    UILabel *payTypeLabel = (UILabel *)[self.view viewWithTag:404];
    //    if ([passDetail[@"payType"] integerValue] == 1) {
    //        payTypeLabel.text = @"(CARD)";
    //    }else {
    //        payTypeLabel.text = @"(CASH)";
    //    }
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    int differenceBetweenStartAndCurrentTimeINT = 0;
    if ([self.title isEqualToString:@"I HAVE ARRIVED"]) {
        NSTimeInterval differenceBetweenStartAndCurrentTime = [[NSDate date] timeIntervalSinceDate:[ud objectForKey:@"iHaveArrivedPressed"]];
        differenceBetweenStartAndCurrentTimeINT = differenceBetweenStartAndCurrentTime;
    }else {
        NSTimeInterval differenceBetweenStartAndCurrentTime = [[NSDate date] timeIntervalSinceDate:[ud objectForKey:@"beginTripPressed"]];
        differenceBetweenStartAndCurrentTimeINT = differenceBetweenStartAndCurrentTime;
    }
    if (differenceBetweenStartAndCurrentTimeINT > 60) {
        timeSec = differenceBetweenStartAndCurrentTimeINT % 60;
        timeMin = differenceBetweenStartAndCurrentTimeINT/60;
        if (timeMin > 60) {
            timeHr = timeMin/60;
            timeMin = timeMin%60;
        }else{
            timeHr = 0;
        }
    }else{
        timeSec = differenceBetweenStartAndCurrentTimeINT;
        timeMin = 0;
        timeHr = 0;
    }
    if (![timerJobStarted isValid])
    {
        [self startTimer];
    }
    
    self.pickUpAndDropAddressLbl.text = [NSString stringWithFormat:@"%@ %@",passDetail[@"dropAddr1"],passDetail[@"dropAddr2"]];
}

/**
 *  Pickup Address View
 */

-(void)changePickUpAddressOnUI{
    
    UILabel *labelAddress = (UILabel*)[_topView viewWithTag:210];
    UILabel *labelAddressValue = (UILabel*)[_topView viewWithTag:211];
    if (_statusButtonTag == 400)
    {
        self.title = @"";
    }else {
        self.title = NSLocalizedString(@"ON THE WAY", @"ON THE WAY");
    }
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"onTheWayPressed"]) {
        NSDate *date = [NSDate date];
        [[NSUserDefaults standardUserDefaults] setObject:date forKey:@"onTheWayPressed"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSTimeInterval differenceBetweenStartAndCurrentTime = [[NSDate date] timeIntervalSinceDate:[ud objectForKey:@"onTheWayPressed"]];
    int differenceBetweenStartAndCurrentTimeINT = differenceBetweenStartAndCurrentTime;
    if (differenceBetweenStartAndCurrentTimeINT > 60) {
        timeSec = differenceBetweenStartAndCurrentTimeINT % 60;
        timeMin = differenceBetweenStartAndCurrentTimeINT/60;
        if (timeMin > 60) {
            timeHr = timeMin/60;
            timeMin = timeMin%60;
        }else{
            timeHr = 0;
        }
    }else{
        timeSec = differenceBetweenStartAndCurrentTimeINT;
        timeMin = 0;
        timeHr = 0;
    }
    if (![timerJobStarted isValid] && [_statusButtonTitle isEqualToString:@"I HAVE ARRIVED"]) {
        [self startTimer];
    }
    
    labelAddress.text = NSLocalizedString(@"PICK UP :", @"PICK UP :");
    if ([passDetail[@"payType"] integerValue] == 1) {
        labelAddress.text = @"PICK UP : (CARD)";
    }else {
        labelAddress.text = @"PICK UP : (CASH)";
    }
    labelAddressValue.text = [NSString stringWithFormat:@"%@ %@", passDetail[@"addr1"],passDetail[@"addr2"]];
    [labelAddressValue sizeToFit];
    
}

/**
 *  Slider Action when dragged till end
 */
#pragma mark - ButtonActions

- (IBAction)phoneBtnClicked:(id)sender
{
    //    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://+%@",passDetail[@"mobile"]]]];
    NSString *makeCall = NSLocalizedString(@"Do you want to make a call on", @"Do you want to make a call on");
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Call", @"Call") message:[NSString stringWithFormat:@"%@ %@",makeCall,passDetail[@"mobile"]] delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", @"Cancel") otherButtonTitles:NSLocalizedString(@"Call", @"Call"), nil];
    alert.tag = 100;
    [alert show];
}

- (IBAction)googleMapsBtnClick:(id)sender
{
    LocationTracker *locaitontraker = [LocationTracker sharedInstance];
    float latitude = locaitontraker.lastLocaiton.coordinate.latitude;
    float longitude = locaitontraker.lastLocaiton.coordinate.longitude;
    CLLocationCoordinate2D position = CLLocationCoordinate2DMake(latitude, longitude);
    [self getAddress:position];
}
- (IBAction)wazeBtnClick:(id)sender
{
    if ([[UIApplication sharedApplication]
         canOpenURL:[NSURL URLWithString:@"waze://"]])
    {
        if(_bookingStatus == kNotificationTypeBookingOnTheWay)
        {
            if(passDetail[@"pickLat"] && passDetail[@"pickLong"])
            {
                NSString *urlStr =[NSString stringWithFormat:@"waze://?ll=%f,%f&navigate=yes",
                                   [passDetail[@"pickLat"] doubleValue], [passDetail[@"pickLong"] doubleValue]];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStr]];
            }
            else
            {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Message" message:@"Pickup location not updated." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alertView show];
            }
        }
        else
        {
            if (dropLatAdded == nil && dropLongAdded == nil)
            {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Message" message:@"Drop location not updated." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alertView show];
            }
            else
            {
                // Waze is installed. Launch Waze and start navigation
                NSString *urlStr =[NSString stringWithFormat:@"waze://?ll=%f,%f&navigate=yes",
                                   [dropLatAdded doubleValue], [dropLongAdded doubleValue]];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStr]];
            }
        }
    }
    else
    {
        // Waze is not installed. Launch AppStore to install Waze app
        [[UIApplication sharedApplication] openURL:[NSURL
                                                    URLWithString:@"http://itunes.apple.com/us/app/id323229106"]];
    }
}


-(void)sliderAction
{
    [self buttonAction:btnArrival];
}

-(IBAction)buttonAction:(id)sender
{
    PMDReachabilityWrapper * reachability = [PMDReachabilityWrapper sharedInstance];
    if ([reachability isNetworkAvailable])
    {
        ProgressIndicator * pi = [ProgressIndicator sharedInstance];
        [pi showPIOnView:self.view withMessage:NSLocalizedString(@"Updating Status..", @"Updating Status..")];
        
        [self sendRequestForUpdateStatus:sender];
    }
    else
    {
        ProgressIndicator * pi = [ProgressIndicator sharedInstance];
        [pi showMessage:kNetworkErrormessage On:self.view];
        [_customSliderView sliderImageOrigin];
    }
    
}
-(void)contactButtonClicked:(id)sender
{
    UIButton *button = (UIButton *)sender;
    NSString *buttonTitle = button.currentTitle;
    
    NSString *number = [NSString stringWithFormat:@"%@",buttonTitle];
    NSURL* callUrl=[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",number]];
    
    //check  Call Function available only in iphone
    if([[UIApplication sharedApplication] canOpenURL:callUrl])
    {
        [[UIApplication sharedApplication] openURL:callUrl];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"ALERT", @"ALERT") message:NSLocalizedString(@"This function is only available on the iPhone", @"This function is only available on the iPhone")  delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
        [alert show];
    }
}

-(void)cancelBookingClicked:(id)sender{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Why are you canceling?", @"Why are you canceling?") delegate:self cancelButtonTitle:NSLocalizedString(@"No don't cancel booking", @"No don't cancel booking") destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Do not charge client", @"Do not charge client"),NSLocalizedString(@"Client no-show", @"Client no-show"),NSLocalizedString(@"Client requested cancel", @"Client requested cancel"),NSLocalizedString(@"Wrong address shown", @"Wrong address shown"),NSLocalizedString(@"other", @"other") ,nil];
    //[actionSheet showInView:self.view];
    UIWindow* window = [[[UIApplication sharedApplication] delegate] window];
    if ([window.subviews containsObject:self.view]) {
        [actionSheet showInView:self.view];
    } else
    {
        [actionSheet showInView:window];
    }
}

-(void)mapsButtonClicked:(id)sender {
    
    LocationTracker *locaitontraker = [LocationTracker sharedInstance];
    
    float latitude = locaitontraker.lastLocaiton.coordinate.latitude;
    float longitude = locaitontraker.lastLocaiton.coordinate.longitude;
    
    CLLocationCoordinate2D position = CLLocationCoordinate2DMake(latitude, longitude);
    [self getAddress:position];
}
-(void)wazeButtonClick:(id)sender
{
    if ([[UIApplication sharedApplication]
         canOpenURL:[NSURL URLWithString:@"waze://"]]) {
        
        if (dropLatAdded == nil && dropLongAdded == nil) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Message" message:@"Drop location not updated." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
        else{
            // Waze is installed. Launch Waze and start navigation
            NSString *urlStr =
            [NSString stringWithFormat:@"waze://?ll=%f,%f&navigate=yes",
             [dropLatAdded doubleValue], [dropLongAdded doubleValue]];
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStr]];
        }
    } else {
        
        // Waze is not installed. Launch AppStore to install Waze app
        [[UIApplication sharedApplication] openURL:[NSURL
                                                    URLWithString:@"http://itunes.apple.com/us/app/id323229106"]];
    }
}
#pragma mark UIAddress Formatter

- (void)getAddress:(CLLocationCoordinate2D)coordinate {
    
    if (!geocoder_) {
        geocoder_ = [[GMSGeocoder alloc] init];
    }
    GMSReverseGeocodeCallback handler = ^(GMSReverseGeocodeResponse *response,
                                          NSError *error) {
        if (response && response.firstResult) {
            
            GMSAddress *address = response.firstResult;
            NSString *start = [address.lines componentsJoinedByString:@","];
            
            
            [self performSelectorOnMainThread:@selector(openDirection:) withObject:start waitUntilDone:YES];
            
            
            
        }else {
            // NSLog(@"Could not reverse geocode point (%f,%f): %@",
            //coordinate.latitude, coordinate.longitude, error);
        }
    };
    
    [geocoder_ reverseGeocodeCoordinate:coordinate
                      completionHandler:handler];
    
}


/**
 *  Open Google Maps
 *
 *  @param startLocation start location
 */

- (void)openDirection:(NSString *)startLocation{
    
    NSString *destination;
    
    if ([self.title isEqualToString:@"I HAVE ARRIVED"] || [self.title isEqualToString:@"JOURNEY STARTED"]) {
        destination = passDetail[@"dropAddr1"];
    }else
        destination = passDetail[@"addr1"];
    
    [MTGoogleMapCustomURLInteraction showDirections:@{DirectionsStartAddress: startLocation,
                                                      DirectionsEndAddress: destination,
                                                      DirectionsDirectionMode: @"Driving",
                                                      ShowMapKeyZoom: @"7",
                                                      ShowMapKeyViews: @"Satellite",
                                                      ShowMapKeyMapMode: @"standard"}
                                      allowCallback:YES];
}



#pragma mark - ActionSheet Delegate
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (actionSheet.cancelButtonIndex != buttonIndex) {
        switch (buttonIndex) {
            case 0:  // do not charge client
            {
                [self sendRequestForCancelBookingWithReason:kCBRDoNotChargeClient];
                break;
            }
            case 1: // client no - show
            {
                [self sendRequestForCancelBookingWithReason:kCBRPassengerDoNotShow];
                break;
            }
            case 2: // client request to cancel
            {
                [self sendRequestForCancelBookingWithReason:kCBRDPassengerRequestedCancel];
                break;
            }
            case 3: // wrong address shown
            {
                [self sendRequestForCancelBookingWithReason:kCBRWrongAddressShown];
                break;
            }
            case 4: // other
            {
                [self sendRequestForCancelBookingWithReason:kCBOhterReasons];
                break;
            }
            default:
                break;
        }
    }
}
#pragma mark - Map direction

-(void)getCurrentLocation {
    
    //check location services is enabled
    if ([CLLocationManager locationServicesEnabled]) {
        
        if (!_locationManager) {
            _locationManager = [[CLLocationManager alloc] init];
            _locationManager.delegate = self;
            if ([_locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
                
                [_locationManager requestAlwaysAuthorization];
            }
            if ([_locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
                [_locationManager requestWhenInUseAuthorization];
            }
            _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
            _locationManager.distanceFilter = 50;
            _locationManager.headingFilter = 1;
            if  ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)])
            {
                [self.locationManager requestAlwaysAuthorization];
            }
            if([self.locationManager respondsToSelector:@selector(allowsBackgroundLocationUpdates)]){
                [self.locationManager setAllowsBackgroundLocationUpdates:YES];
            }
        }
        [_locationManager startUpdatingLocation];
        [_locationManager startUpdatingHeading];
        
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Location Service" message:@"Unable to find your location,Please enable location services." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
    }
    
}


/**
 *  adds direction on Map
 *
 *  @param json representaion of pathpoints
 */
- (void)addDirections:(NSDictionary *)json {
    
    if ([json[@"routes"] count]>0) {
        NSDictionary *routes = [json objectForKey:@"routes"][0];
        
        NSDictionary *route = [routes objectForKey:@"overview_polyline"];
        NSString *overview_route = [route objectForKey:@"points"];
    }
    
    
}

/**
 *  Sets the start location of Path
 *
 *  @param latitude  start point latitude
 *  @param longitude start point longitude
 */
-(void)setStartLocationCoordinates:(float)latitude Longitude:(float)longitude{
    //change map camera postion to current location
    
    _isUpdatedLocation = YES;
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:latitude
                                                            longitude:longitude
                                                                 zoom:17];
    [mapView_ setCamera:camera];
    //add marker at current location
    CLLocationCoordinate2D position = CLLocationCoordinate2DMake(latitude, longitude);
    if (!_currentLocationMarker) {
        _currentLocationMarker = [[GMSMarker alloc] init];
    }
    _currentLocationMarker.map = mapView_;
    //    _currentLocationMarker.flat = YES;
    _currentLocationMarker.position = position;
    _currentLocationMarker.icon = _carImage;//[UIImage imageNamed:@"car_type_two.png"];
    [waypoints_ addObject:_currentLocationMarker];
    //save current location to plot direciton on map
    _startLocation = [NSString stringWithFormat:@"%.6f,%.6f",latitude, longitude];
    [waypointStrings_ insertObject:_startLocation atIndex:0];
    if (_isDriverArrived)
    {
        [self changeDropAddressOnUI];
        if (waypoints_.count > 1) {
            [waypoints_ removeLastObject];
            [waypointStrings_ removeLastObject];
        }
        _isPathPlotted = NO;
        [self updateDestinationLocationWithLatitude:[passDetail[@"dropLat"] doubleValue]Longitude:[passDetail[@"dropLong"] doubleValue]];
    }
    else {
        [self updateDestinationLocationWithLatitude:[passDetail[@"pickLat"] doubleValue]Longitude:[passDetail[@"pickLong"] doubleValue]];
    }
}

/**
 * sets destination point of path
 *
 *  @param latitude  destination point latitude
 *  @param longitude destination point longitude
 */
-(void)updateDestinationLocationWithLatitude:(float)latitude Longitude:(float)longitude{
    if (!_isPathPlotted) {
        NSLog(@"pathplotted");
        //        _isPathPlotted = YES;
        
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(
                                                                     latitude,
                                                                     longitude);
        
        _previouCoord = position;
        
        _destinationMarker = nil;
        //        _destinationMarker = [GMSMarker markerWithPosition:position];
        if (!_destinationMarker) {
            _destinationMarker = [[GMSMarker alloc] init];
        }
        _destinationMarker.position = position;
        _destinationMarker.map = mapView_;
        \
        [waypoints_ addObject:_destinationMarker];
        NSString *positionString = [[NSString alloc] initWithFormat:@"%f,%f",
                                    latitude,longitude];
        
        [waypointStrings_ addObject:positionString];
        
        if([waypoints_ count]>1){
            NSString *sensor = @"false";
            NSArray *parameters = [NSArray arrayWithObjects:sensor, waypointStrings_,
                                   nil];
            NSArray *keys = [NSArray arrayWithObjects:@"sensor", @"waypoints", nil];
            NSDictionary *query = [NSDictionary dictionaryWithObjects:parameters
                                                              forKeys:keys];
            DirectionService *mds=[[DirectionService alloc] init];
            SEL selector = @selector(addDirections:);
            [mds setDirectionsQuery:query
                       withSelector:selector
                       withDelegate:self];
        }
    }
    else
    {
        
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(latitude,longitude);
        CLLocationDirection heading = GMSGeometryHeading(_previouCoord, position);
        _previouCoord = position;
        
        [CATransaction begin];
        [CATransaction setAnimationDuration:2.0];
        _destinationMarker.position = position;
        [CATransaction commit];
        if (_destinationMarker.flat) {
            //            _destinationMarker.rotation = heading;
        }
    }
}


#pragma mark - Location Manager Delegate

-(void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading {
    
    if(newHeading.headingAccuracy>0)
    {
        float heading = newHeading.magneticHeading; //in degrees
        [mapView_ animateToBearing:heading];
    }
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    //  CLLocation *newLocation = [locations lastObject];
    
    _isUpdatedLocation = NO;
    if (!_isUpdatedLocation) {
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    if (!_isUpdatedLocation) {
        _isUpdatedLocation = YES;
        
        [self setStartLocationCoordinates:newLocation.coordinate.latitude Longitude:newLocation.coordinate.longitude];
    }
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
}


/**
 *  To get the coordinated of string address
 *
 *  @param address location address
 *
 *  @return address coordinates
 */
- (CLLocationCoordinate2D)geoCodeUsingAddress:(NSString *)address
{
    double latitude = 0, longitude = 0;
    NSString *esc_addr =  [address stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *req = [NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode/json?sensor=false&address=%@", esc_addr];
    NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:req] encoding:NSUTF8StringEncoding error:NULL];
    if (result) {
        NSScanner *scanner = [NSScanner scannerWithString:result];
        if ([scanner scanUpToString:@"\"lat\" :" intoString:nil] && [scanner scanString:@"\"lat\" :" intoString:nil]) {
            [scanner scanDouble:&latitude];
            if ([scanner scanUpToString:@"\"lng\" :" intoString:nil] && [scanner scanString:@"\"lng\" :" intoString:nil]) {
                [scanner scanDouble:&longitude];
            }
        }
    }
    CLLocationCoordinate2D center;
    center.latitude = latitude;
    center.longitude = longitude;
    return center;
}


-(void)getAddressFromLatLon:(CLLocation*) location {
    
    if (!geocoder_) {
        geocoder_ = [[GMSGeocoder alloc] init];
    }
    GMSReverseGeocodeCallback handler = ^(GMSReverseGeocodeResponse *response,
                                          NSError *error) {
        if (response && response.firstResult) {
            
            GMSAddress *address = response.firstResult;
            //            NSString *start = [address.lines componentsJoinedByString:@","];
            _destinationLocation = [[NSString alloc] init];
            _destinationArea = [[NSString alloc] init];
            
            if (address.thoroughfare != nil)
            {
                _destinationLocation = [_destinationLocation stringByAppendingString:[NSString stringWithFormat:@"%@, ",address.thoroughfare]];
                
            }
            if (address.subLocality != nil) {
                _destinationLocation = [_destinationLocation stringByAppendingString:[NSString stringWithFormat:@"%@, ",address.subLocality]];
                
            }
            if (address.locality != nil) {
                
                _destinationArea = address.subLocality;
                _destinationLocation = [_destinationLocation stringByAppendingString:[NSString stringWithFormat:@"%@, ",address.locality]];
                
            }
            if (address.administrativeArea != nil) {
                _destinationLocation = [_destinationLocation stringByAppendingString:[NSString stringWithFormat:@"%@, ",address.administrativeArea]];
                
            }
            if (address.postalCode != nil) {
                _destinationLocation = [_destinationLocation stringByAppendingString:[NSString stringWithFormat:@"%@, ",address.postalCode]];
                
            }
            NSInteger locationCount = _destinationLocation.length;
            if ([[_destinationLocation substringFromIndex:locationCount - 2] rangeOfString:@","].location != NSNotFound) {
                _destinationLocation = [_destinationLocation substringToIndex:locationCount - 2];
            }
            _destinationCoordinates = location.coordinate;
            [self sendRequestToUpdateDestinationDetails];//manual booking
        }
        else
        {
            [[ProgressIndicator sharedInstance] hideProgressIndicator];
            [Helper showAlertWithTitle:@"Error" Message:@"Failed to get destination location. Please try again."];
            
            [_customSliderView sliderImageOrigin];
        }
    };
    
    [geocoder_ reverseGeocodeCoordinate:location.coordinate completionHandler:handler];
}


#pragma mark- GMSMapviewDelegate


- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate
{
    
}
- (void) mapView:(GMSMapView *) mapView willMove:(BOOL)gesture{
    
    
}

- (void) mapView:(GMSMapView *) mapView didChangeCameraPosition:(GMSCameraPosition *) position{
    
}

- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position{
    
    
    
}
- (void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker{
    
    [self performSegueWithIdentifier:@"doctorDetails" sender:marker];
}

- (UIView *)mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker {
    
    return nil;
    
}
- (void)mapView:(GMSMapView *)mapView didBeginDraggingMarker:(GMSMarker *)marker{
}
- (void)mapView:(GMSMapView *)mapView didEndDraggingMarker:(GMSMarker *)marker{
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
    
    if (!_isUpdatedLocation) {
        // If the first location update has not yet been recieved, then jump to that
        // location.
        _isUpdatedLocation = YES;
        CLLocation *location = [change objectForKey:NSKeyValueChangeNewKey];
        //        mapView_.camera = [GMSCameraPosition cameraWithTarget:location.coordinate
        //                                                         zoom:17];
        GMSCameraUpdate *update = [GMSCameraUpdate setTarget:location.coordinate zoom:18];
        [mapView_ animateWithCameraUpdate:update];
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude);
        //[GMSMarker markerWithPosition:position];
        CLLocationDirection heading = GMSGeometryHeading(_previouCoord, position);
        _currentLocationMarker.position = position;
        //        _currentLocationMarker.flat = YES;
        [CATransaction begin];
        [CATransaction setAnimationDuration:1.0];
        _currentLocationMarker.groundAnchor = CGPointMake(0.5f, 0.5f);
        if (_currentLocationMarker.flat) {
            //            _currentLocationMarker.rotation = heading;
        }
        [CATransaction commit];
        _currentLocationMarker.map = mapView_;
        
        _previouCoord = position;
    }
    else {
        
        
        
        CLLocation *location = [change objectForKey:NSKeyValueChangeNewKey];
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude);
        //_currentLocationMarker.position = position;
        
        GMSCameraUpdate *update = [GMSCameraUpdate setTarget:location.coordinate zoom:18];
        [mapView_ animateWithCameraUpdate:update];
        
        
        //        CLLocationDirection heading = GMSGeometryHeading(_previouCoord, position);
        
        
        [CATransaction begin];
        [CATransaction setAnimationDuration:1.0];
        _currentLocationMarker.position = position;
        _currentLocationMarker.icon = _carImage;//[UIImage imageNamed:@"car_type_two.png"];
        _currentLocationMarker.groundAnchor = CGPointMake(0.5f, 0.5f);
        //        _currentLocationMarker.flat = YES;
        [CATransaction commit];
        //        if (_currentLocationMarker.flat) {
        //            _currentLocationMarker.rotation = heading;
        //        }
        _previouCoord = position;
    }
    
    
}

#pragma mark -leftbar and righbar action

-(void)rightBarButtonClicked:(UIButton *)sender{
    
    //[self gotolistViewController];
}

-(void)leftBarButtonClicked:(UIButton *)sender{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if ([ud objectForKey:@"onTheWayPressed"]) {
        [ud removeObjectForKey:@"onTheWayPressed"];
    }
    if ([ud objectForKey:@"iHaveArrivedPressed"]) {
        [ud removeObjectForKey:@"iHaveArrivedPressed"];
    }
    if ([ud objectForKey:@"beginTripPressed"]) {
        [ud removeObjectForKey:@"beginTripPressed"];
    }
    if ([ud objectForKey:@"passengerDroppedPressed"]) {
        [ud removeObjectForKey:@"passengerDroppedPressed"];
    }
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - WebRequests And Response
/**
 *  Updating the booking status on the server
 *
 *  @param sender Custon slider
 */

-(void)sendRequestForUpdateStatus:(UIButton*)sender
{
    PMDReachabilityWrapper * reachability = [PMDReachabilityWrapper sharedInstance];
    if ([reachability isNetworkAvailable])
    {
        UpdateBookingStatus *updateBookingStatus = [UpdateBookingStatus sharedInstance];
        [updateBookingStatus stopUpdatingStatus];
        
        if (![sender isKindOfClass:[UIButton class]])
            return;
        
        ProgressIndicator * pi = [ProgressIndicator sharedInstance];
        NetworkHandler * handler = [NetworkHandler sharedInstance];
        NSString *email = passDetail[@"email"];
        NSString *appointmentDate = passDetail[@"apptDt"];
        NSDictionary *queryParams;
        
        if (sender.tag == 400)
        {
            queryParams = [NSDictionary dictionaryWithObjectsAndKeys:
                           [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],KDAcheckUserSessionToken,
                           [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],kSMPCommonDevideId,
                           email,kSMPRespondPassengerEmail,
                           appointmentDate, kSMPRespondBookingDateTime,
                           constkNotificationTypeBookingOnTheWay,kSMPRespondResponse,
                           @"testing",kSMPRespondDocNotes,
                           [Helper getCurrentDateTime],kSMPCommonUpDateTime, nil];
        }
        else if(sender.tag == 500)
        {
            queryParams = [NSDictionary dictionaryWithObjectsAndKeys:
                           [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],KDAcheckUserSessionToken,
                           [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],kSMPCommonDevideId,
                           email,kSMPRespondPassengerEmail,
                           appointmentDate, kSMPRespondBookingDateTime,
                           constkNotificationTypeBookingArrived,kSMPRespondResponse,
                           @"testing",kSMPRespondDocNotes,
                           [Helper getCurrentDateTime],kSMPCommonUpDateTime, nil];
        }
        else if(sender.tag == 600)
        {
            queryParams = [NSDictionary dictionaryWithObjectsAndKeys:
                           [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],KDAcheckUserSessionToken,
                           [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],kSMPCommonDevideId,
                           email,kSMPRespondPassengerEmail,
                           appointmentDate, kSMPRespondBookingDateTime,
                           constkNotificationTypeBookingBeginTrip,kSMPRespondResponse,
                           @"testing",kSMPRespondDocNotes,
                           [Helper getCurrentDateTime],kSMPCommonUpDateTime, nil];
        }
        else{
            
            [pi showPIOnView:self.view withMessage:NSLocalizedString(@"Please wait..", @"Please wait..")];
            
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isBooked"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            LocationTracker *locaitontraker = [LocationTracker sharedInstance];
            [self getAddressFromLatLon:locaitontraker.lastLocaiton];
            
            return;
        }
        if (sender.tag != 700)
        {
            [handler composeRequestWithMethod:MethodupdateApptStatus
                                      paramas:queryParams
                                 onComplition:^(BOOL succeeded, NSDictionary *response) {
                                     
                                     ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
                                     [progressIndicator hideProgressIndicator];
                                     [_customSliderView sliderImageOrigin];
                                     if (succeeded) { //handle success response
                                         
                                         
                                         if ([response[@"errFlag"] intValue] ==0)
                                         {
                                             if (sender.tag == 400) {
                                                 
                                                 if (_bookingStatus == 0) {
                                                     self.navigationItem.hidesBackButton = YES;
                                                     self.navigationItem.leftBarButtonItem = nil;
                                                     //self.navigationItem.rightBarButtonItem = nil;
                                                 }
                                                 [sender setTitle:NSLocalizedString(@"I HAVE ARRIVED", @"I HAVE ARRIVED") forState:UIControlStateNormal];
                                                 _statusButtonTitle = NSLocalizedString(@"I HAVE ARRIVED", @"I HAVE ARRIVED");
                                                 _customSliderView.sliderTitle.text = NSLocalizedString(@"I HAVE ARRIVED", @"I HAVE ARRIVED");
                                                 self.title = NSLocalizedString(@"ON THE WAY", @"ON THE WAY");
                                                 sender.tag = 500;
                                                 [self updateToPassengerForDriverState:kPubNubStartDoctorLocationStreamAction withIteration:-1];
                                                 [self changePickUpAddressOnUI];
                                             }
                                             else if(sender.tag == 500) //driver reached responese
                                             {
                                                 [[NSUserDefaults standardUserDefaults]setObject:@"7" forKey:@"currentBookingStatus"];
                                                 [sender setTitle:NSLocalizedString(@"BEGIN TRIP", @"BEGIN TRIP") forState:UIControlStateNormal];
                                                 _customSliderView.sliderTitle.text = NSLocalizedString(@"BEGIN TRIP", @"BEGIN TRIP");
                                                 [self stopTimer];
                                                 timeHr = timeMin = timeSec = 0;
                                                 NSDate *date = [NSDate date];
                                                 [[NSUserDefaults standardUserDefaults] setObject:date forKey:@"iHaveArrivedPressed"];
                                                 [[NSUserDefaults standardUserDefaults] synchronize];
                                                 self.title = NSLocalizedString(@"I HAVE ARRIVED", @"I HAVE ARRIVED");
                                                 
                                                 
                                                 [pi hideProgressIndicator];
                                                 sender.tag = 600;
                                                 
                                                 _isPathPlotted = NO;
                                                 _isUpdatedLocation = NO;
                                                 _isDriverArrived = YES;
                                                 [mapView_ clear];
                                                 
                                                 [self getCurrentLocation];
                                                 LocationTracker *tracker = [LocationTracker sharedInstance];
                                                 [self setStartLocationCoordinates:tracker.lastLocaiton.coordinate.latitude Longitude:tracker.lastLocaiton.coordinate.longitude];
                                                 [self updateToPassengerForDriverState:kPubNubDriverReachedPickupLocation withIteration:-1];
                                                 [self changeDropAddressOnUI];
                                             }
                                             else if (sender.tag == 600)
                                             {
                                                 [[NSUserDefaults standardUserDefaults]setObject:@"8" forKey:@"currentBookingStatus"];
                                                 self.navigationItem.leftBarButtonItem = nil;
                                                 self.navigationItem.hidesBackButton = YES;
                                                 self.navigationItem.rightBarButtonItem = nil;
                                                 self.distanceLbl.hidden = NO;
                                                 self.title = NSLocalizedString(@"JOURNEY STARTED", @"JOURNEY STARTED");
                                                 
                                                 LocationTracker *tracker = [LocationTracker sharedInstance];
                                                 tracker.distance = 0;
                                                 [sender setTitle:NSLocalizedString(@"PASSENGER DROPPED", @"PASSENGER DROPPED") forState:UIControlStateNormal];
                                                 _customSliderView.sliderTitle.text = NSLocalizedString(@"PASSENGER DROPPED", @"PASSENGER DROPPED");
                                                 [self stopTimer];
                                                 timeSec = timeMin = timeHr = 0;
                                                 NSDate *date = [NSDate date];
                                                 [[NSUserDefaults standardUserDefaults] setObject:date forKey:@"beginTripPressed"];
                                                 
                                                 [pi hideProgressIndicator];
                                                 [self changeDropAddressOnUI];
                                                 sender.tag = 700;
                                                 //_driverState = kPubNubDriverReachedPickupLocation;
                                                 [self updateToPassengerForDriverState:kPubNubDriverBeginTrp withIteration:-1];
                                             }
                                             else if (sender.tag == 700){
                                                 
                                                 [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isBooked"];
                                                 [[NSUserDefaults standardUserDefaults] synchronize];
                                                 [pi hideProgressIndicator];
                                                 [self performSegueWithIdentifier:@"GoToBookingHistory" sender:self];
                                             }
                                         }
                                         else{
                                             
                                             
                                             if ([response[@"errFlag"] intValue] == 41) {
                                                 [progressIndicator hideProgressIndicator];
                                                 NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
                                                 [ud setBool:NO forKey:@"isBooked"];
                                                 if ([ud objectForKey:@"onTheWayPressed"]) {
                                                     [ud removeObjectForKey:@"onTheWayPressed"];
                                                 }
                                                 if ([ud objectForKey:@"iHaveArrivedPressed"]) {
                                                     [ud removeObjectForKey:@"iHaveArrivedPressed"];
                                                 }
                                                 if ([ud objectForKey:@"beginTripPressed"]) {
                                                     [ud removeObjectForKey:@"beginTripPressed"];
                                                 }
                                                 if ([ud objectForKey:@"passengerDroppedPressed"]) {
                                                     [ud removeObjectForKey:@"passengerDroppedPressed"];
                                                 }
                                                 [ud synchronize];
                                                 [self.navigationController popToRootViewControllerAnimated:YES];
                                             }
                                             [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response[@"errMsg"]];
                                             
                                         }
                                     }
                                     else{
                                         [Helper showAlertWithTitle:@"Error" Message:@"Network Error"];
                                         [[ProgressIndicator sharedInstance]hideProgressIndicator];
                                         [_customSliderView sliderImageOrigin];
                                     }
                                 }];
        }
        else
        {
            ProgressIndicator * pi = [ProgressIndicator sharedInstance];
            [pi showMessage:kNetworkErrormessage On:self.view];
            [_customSliderView sliderImageOrigin];
        }
    }
}

-(void)sendRequestForCancelBookingWithReason:(CancelBookingReasons)reason{
    
    
    UpdateBookingStatus *updateBookingStatus = [UpdateBookingStatus sharedInstance];
    [updateBookingStatus stopUpdatingStatus];
    
    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
    if ( [reachability isNetworkAvailable]) {
        
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi showPIOnView:self.view withMessage:NSLocalizedString(@"cancelling..", @"cancelling..")];
        
        NSString *email = passDetail[@"email"];
        NSString *appointmentDate = passDetail[@"apptDt"];
        
        NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
        
        NSString *deviceID = [[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey];
        
        NSString *currentDate = [Helper getCurrentDateTime];
        
        NSDictionary *params = @{@"ent_sess_token":sessionToken,
                                 @"ent_dev_id":deviceID,
                                 @"ent_pas_email":email,
                                 @"ent_appnt_dt":appointmentDate,
                                 @"ent_date_time":currentDate,
                                 @"ent_cancel_type":[NSNumber numberWithInt:reason],
                                 };
        NSLog(@"sendRequestForCancelBookingWithReason>>>>>>>>%@",params);
        NetworkHandler *networHandler = [NetworkHandler sharedInstance];
        [networHandler composeRequestWithMethod:MethodCancelBooking
                                        paramas:params
                                   onComplition:^(BOOL success, NSDictionary *response){
                                       NSLog(@"sendRequestForCancelBookingWith response>>>>>>>>>>>>>%@",response);
                                       if (success) { //handle success response
                                           //[self cancelBookingResponse:response];
                                           [pi hideProgressIndicator];
                                           if ([response[@"errFlag"] intValue] == 0) { //success
                                               
                                               [self updateForForCancelBooking:reason];
                                               [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"bid"];
                                               [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isBooked"];
                                               [[NSUserDefaults standardUserDefaults] synchronize];
                                               NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
                                               if ([ud objectForKey:@"onTheWayPressed"]) {
                                                   [ud removeObjectForKey:@"onTheWayPressed"];
                                               }
                                               if ([ud objectForKey:@"iHaveArrivedPressed"]) {
                                                   [ud removeObjectForKey:@"iHaveArrivedPressed"];
                                               }
                                               if ([ud objectForKey:@"beginTripPressed"]) {
                                                   [ud removeObjectForKey:@"beginTripPressed"];
                                               }
                                               if ([ud objectForKey:@"passengerDroppedPressed"]) {
                                                   [ud removeObjectForKey:@"passengerDroppedPressed"];
                                               }
                                               [self.navigationController popToRootViewControllerAnimated:YES];
                                               
                                               
                                               
                                           }
                                           else {
                                               [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isBooked"];
                                               [[NSUserDefaults standardUserDefaults] synchronize];
                                               NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
                                               if ([ud objectForKey:@"onTheWayPressed"]) {
                                                   [ud removeObjectForKey:@"onTheWayPressed"];
                                               }
                                               if ([ud objectForKey:@"iHaveArrivedPressed"]) {
                                                   [ud removeObjectForKey:@"iHaveArrivedPressed"];
                                               }
                                               if ([ud objectForKey:@"beginTripPressed"]) {
                                                   [ud removeObjectForKey:@"beginTripPressed"];
                                               }
                                               if ([ud objectForKey:@"passengerDroppedPressed"]) {
                                                   [ud removeObjectForKey:@"passengerDroppedPressed"];
                                               }
                                               [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response[@"errMsg"]];
                                               [self.navigationController popToRootViewControllerAnimated:YES];
                                           }
                                       }
                                       else {
                                           
                                           [pi hideProgressIndicator];
                                       }
                                   }];
    }
    else {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi showMessage:kNetworkErrormessage On:self.view];
    }
}


-(void)cancelBookingResponse:(NSDictionary*)response{
    
    if (response == nil)
    {
        return;
    }
    else
    {
        if ([response[@"errFlag"] intValue] == 0) { //success
            [self updateToPassengerForDriverState:kPubNubDriverCancelBooking withIteration:5];
            
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isBooked"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
            if ([ud objectForKey:@"onTheWayPressed"]) {
                [ud removeObjectForKey:@"onTheWayPressed"];
            }
            if ([ud objectForKey:@"iHaveArrivedPressed"]) {
                [ud removeObjectForKey:@"iHaveArrivedPressed"];
            }
            if ([ud objectForKey:@"beginTripPressed"]) {
                [ud removeObjectForKey:@"beginTripPressed"];
            }
            if ([ud objectForKey:@"passengerDroppedPressed"]) {
                [ud removeObjectForKey:@"passengerDroppedPressed"];
            }
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
        else
        {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response[@"errMsg"]];
        }
    }
}


-(IBAction)updateAppointmentDetail:(id)sender
{
    
    PMDReachabilityWrapper * reachability = [PMDReachabilityWrapper sharedInstance];
    if ([reachability isNetworkAvailable])
    {
        ProgressIndicator * pi = [ProgressIndicator sharedInstance];
        [pi showPIOnView:self.view withMessage:NSLocalizedString(@"Updating details..", @"Updating details..")];
        
        NetworkHandler * handler = [NetworkHandler sharedInstance];
        NSString *email = passDetail[@"email"];
        NSString *appointmentDate = passDetail[@"apptDt"];
        
        LocationTracker *tracker = [LocationTracker sharedInstance];
        float _distance = tracker.distance;
        NSDictionary *queryParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                     [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],KDAcheckUserSessionToken,
                                     [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],kSMPCommonDevideId,
                                     [[NSUserDefaults standardUserDefaults] objectForKey:KDAgetcityId],KDAgetcityId,
                                     email,kSMPRespondPassengerEmail,
                                     appointmentDate, kSMPRespondBookingDateTime,
                                     constkNotificationTypeBookingComplete,kSMPRespondResponse,
                                     [Helper getCurrentDateTime],kSMPCommonUpDateTime,
                                     @"notes",@"ent_doc_remarks",
                                     _destinationLocation,@"ent_drop_addr_line1",
                                     _destinationArea,@"ent_drop_addr_line2",
                                     [NSNumber numberWithDouble:_destinationCoordinates.latitude],@"ent_drop_lat",
                                     [NSNumber numberWithDouble:_destinationCoordinates.longitude],@"ent_drop_long",
                                     [NSNumber numberWithFloat:_distance],@"ent_distance",
                                     nil];
        
        [handler composeRequestWithMethod:MethodupdateApptStatus
                                  paramas:queryParams
                             onComplition:^(BOOL succeeded, NSDictionary *response) {
                                 
                                 if (succeeded) { //handle success response
                                     //[self updateStstusResponse:(NSArray*)response];
                                     
                                     Errorhandler * handler = [(NSArray*)response objectAtIndex:0];
                                     
                                     [pi hideProgressIndicator];
                                     
                                     if ([[handler errFlag] intValue] ==0)
                                     {
                                         [self performSegueWithIdentifier:@"GoToBookingHistory" sender:self];
                                     }
                                     else {
                                         [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:handler.errMsg];
                                     }
                                 }
                             }];
    }
    else{
        ProgressIndicator * pi = [ProgressIndicator sharedInstance];
        [pi showMessage:kNetworkErrormessage On:self.view];
    }
}
/**
 *  Update destination details will current latitude, longitude and address
 */

-(void)sendRequestToUpdateDestinationDetails
{
    PMDReachabilityWrapper * reachability = [PMDReachabilityWrapper sharedInstance];
    if ([reachability isNetworkAvailable])
    {
        NSMutableArray * completeLatLong=[[[NSUserDefaults standardUserDefaults] objectForKey:@"latlongs"] mutableCopy];
        NSString *joinedComponents = @"";// = [completeLatLong componentsJoinedByString:@","];
        for (NSDictionary * dict in completeLatLong)
        {
            joinedComponents = [joinedComponents stringByAppendingFormat:@"%@ - %@, ",dict[@"longitude"],dict[@"latitude"]];
        }
        NSInteger bid = [[[NSUserDefaults standardUserDefaults] objectForKey:@"bid"] integerValue];
        NSString *sessionToken =    [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken];
        NSString *deviceid = [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey];
        NSString * cityId = [[NSUserDefaults standardUserDefaults]objectForKey:KDAgetcityId];
        NSString *email = passDetail[@"email"];
      //  float distance = [[[NSUserDefaults standardUserDefaults] objectForKey:kNSUDriverDistanceTravalled] floatValue];
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        [params setObject:sessionToken forKey:KDAcheckUserSessionToken];
        [params setObject:cityId forKey:KDAgetcityId];
        
        [params setObject:deviceid forKey:kSMPCommonDevideId];
        [params setObject:[Helper getCurrentDateTime] forKey:kSMPCommonUpDateTime];
        [params setObject:[NSNumber numberWithInteger:bid] forKey:@"ent_appnt_id"];
        [params setObject:_destinationLocation forKey:@"ent_drop_addr_line1"];
        [params setObject:@"" forKey:@"ent_drop_addr_line2"];
        [params setObject:flStrForStr(email) forKey:@"ent_pas_email"];
        [params setObject:[NSNumber numberWithDouble:_destinationCoordinates.latitude] forKey:@"ent_drop_lat"];
        [params setObject:[NSNumber numberWithDouble:_destinationCoordinates.longitude] forKey:@"ent_drop_long"];
        [params setObject:[NSString stringWithFormat:@"%0.02f", distanceBtween/1609.344] forKey:@"ent_distance"];
        [params setObject:joinedComponents forKey:KDATotalLatLongCalculated];
        NSLog(@"MethodUpdateAppointmentDetail request params>>>>>>>>>>>>>>%@",params);
        NetworkHandler *handler = [NetworkHandler sharedInstance];
        [handler composeRequestWithMethod:MethodUpdateAppointmentDetail
                                  paramas:params
                             onComplition:^(BOOL success , NSDictionary *response)
        {
                                 if (success)
                                 {
                                     NSLog(@"Destination Details: %@", response);
                                     ProgressIndicator *pi = [ProgressIndicator sharedInstance];
                                     [pi hideProgressIndicator];
                                     NSDictionary *dict  =  response;
                                     if ([[dict objectForKey:@"errFlag"] integerValue] == 0)
                                     {
                                         [self updateToPassengerForDriverState:kPubNubDriverReachedDestinationLoation];
                                         UpdateBookingStatus * update = [UpdateBookingStatus sharedInstance];
                                         [update stopUpdatingStatus];
                                         NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
                                         [ud setObject:[NSDate date] forKey:@"passengerDroppedPressed"];
                                         [ud setObject:dict[@"surge"] forKey:@"surge"];
                                         [ud synchronize];
                                         
                                         NSLog(@"response..................%@",response);
                                         MannualInvoiceView* mannualInvoice = [[MannualInvoiceView alloc]init];
                                         UIWindow *window = [[UIApplication sharedApplication] keyWindow];
                                         mannualInvoice.pickupAddressString = flStrForStr(response[@"addr1"]);
                                         mannualInvoice.dropOffAddressString = flStrForStr(_destinationLocation);
                                         mannualInvoice.amountString =[NSString stringWithFormat:@"%0.02f",[response[@"apprAmount"] doubleValue]];
                                         mannualInvoice.distanceString = [NSString stringWithFormat:@"%0.02f %@",[response[@"dis"] floatValue], [Helper getDistanceUnit]] ;
                                         mannualInvoice.airportFeeString = [NSString stringWithFormat:@"%0.02f",[response[@"AirportFree"] doubleValue]];
                                         mannualInvoice.baseFeeString = [NSString stringWithFormat:@"%0.02f",[response[@"baseFee"] doubleValue]];//DistanceFee
                                         mannualInvoice.bidString = flStrForStr(passDetail[@"bid"]);
                                         mannualInvoice.distanceFeeString = [NSString stringWithFormat:@"%0.02f",[response[@"DistanceFee"] doubleValue]];
                                         mannualInvoice.TipLblString = [NSString stringWithFormat:@"%0.02f",[response[@"tip"] doubleValue]];
                                         mannualInvoice.timeFeeString = [NSString stringWithFormat:@"%0.02f",[response[@"TimeFee"] doubleValue]];
                                         mannualInvoice.discountString = [NSString stringWithFormat:@"%0.02f",[response[@"discount"] doubleValue]];
                                         NSTimeInterval differenceBetweenStartAndCurrentTime = [[ud objectForKey:@"passengerDroppedPressed"] timeIntervalSinceDate:[ud objectForKey:@"beginTripPressed"]];
                                         int differenceBetweenStartAndCurrentTimeINT = differenceBetweenStartAndCurrentTime;
                                         if (differenceBetweenStartAndCurrentTimeINT > 60)
                                         {
                                             timeSec = differenceBetweenStartAndCurrentTimeINT % 60;
                                             timeMin = differenceBetweenStartAndCurrentTimeINT/60;
                                             if (timeMin > 60)
                                             {
                                                 timeHr = timeMin/60;
                                                 timeMin = timeMin%60;
                                             }
                                             else
                                             {
                                                 timeHr = 0;
                                             }
                                         }
                                         else
                                         {
                                             timeSec = differenceBetweenStartAndCurrentTimeINT;
                                             timeMin = 0;
                                             timeHr = 0;
                                         }
                                         
                                         mannualInvoice.durationString = [self timeFormatted:[[ NSString stringWithFormat:@"%@",response[@"dur"]] intValue]];
                                         [mannualInvoice onWindow:window pickAddress:flStrForStr(response[@"addr1"]) dropAddress:flStrForStr(response[@"dropAddr1"]) commingFromMannual:YES];
                                         mannualInvoice.delegate = self;
                                         [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"picklat"];
                                         [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"picklog"];
                                         [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"distance"];
                                         [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"latlongs"];
                                         [[NSUserDefaults standardUserDefaults] synchronize];
                                     }
                                     else
                                     {
                                         [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:dict[@"errMsg"]];
                                     }
                                 }
                                 else
                                 {
                                     [[ProgressIndicator sharedInstance] hideProgressIndicator];
                                 }
                             }];
    }
    else
    {
        ProgressIndicator * pi = [ProgressIndicator sharedInstance];
        [pi showMessage:kNetworkErrormessage On:self.view];
        [pi hideProgressIndicator];
        [_customSliderView sliderImageOrigin];
    }
}

-(void)doneButtonSelectWithTag
{
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isBooked"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"bid"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kNSUDriverDistanceTravalled];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"onTheWayPressed"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"iHaveArrivedPressed"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"beginTripPressed"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"passengerDroppedPressed"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"saveLatLongs"];//latlongs
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"latlongs"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"surge"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kNSUPassengerChannelKeyFromAPPTStatus];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"picklat"];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"picklog"];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"distance"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self.navigationController popToRootViewControllerAnimated:YES];
}


-(void)updateToPassengerForDriverState:(PubNubStreamAction)state
{
    UpdateBookingStatus *updateBookingStatus = [UpdateBookingStatus sharedInstance];
    updateBookingStatus.driverState = state;
    updateBookingStatus.iterations = 5;
    [updateBookingStatus updateToPassengerForDriverState];
    [updateBookingStatus startUpdatingStatus];
}

- (NSString *)timeFormatted:(int)totalSeconds
{
    int seconds = totalSeconds % 60;
    int minutes = (totalSeconds / 60) % 60;
    int hours = totalSeconds / 3600;
    return [NSString stringWithFormat:@"%02dH:%02dM:%02dS",hours, minutes, seconds];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    [_locationTracker stopLocationTracking];
    [self.locationUpdateTimer invalidate];
    self.locationUpdateTimer = nil;
    if ([[segue identifier] isEqualToString:@"GoToBookingHistory"])
    {
        NewBookingHistoryViewController *booking = (NewBookingHistoryViewController*)[segue destinationViewController];
        booking.passDetails = passDetail;
    }
}

-(void)updateForForCancelBooking:(CancelBookingReasons)reason
{
    UpdateBookingStatus *updateBookingStatus = [UpdateBookingStatus sharedInstance];
    updateBookingStatus.cancelBookingReason = reason;
    [self updateToPassengerForDriverState:kPubNubDriverCancelBooking withIteration:-1];
}

/**
 *  Updating the booking status on Pubnub as well
 *
 *  @param state     Current booking status
 *  @param iteration Number of times to update the booking status on pubnub
 */

-(void)updateToPassengerForDriverState:(PubNubStreamAction)state withIteration:(int)iteration{
    
    UpdateBookingStatus *updateBookingStatus = [UpdateBookingStatus sharedInstance];
    updateBookingStatus.driverState = state;
    updateBookingStatus.iterations = iteration;
    [updateBookingStatus updateToPassengerForDriverState];
    [updateBookingStatus startUpdatingStatus];
}

-(void)getETA:(NSString*)origin Destination: (NSString*)destination
{
    NSString *ETAString = [NSString stringWithFormat:@"maps/api/distancematrix/json?origins=%@&destinations=%@",origin,destination];
    
    NSURL *url = [NSURL URLWithString:@"http://maps.googleapis.com/"];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    [httpClient postPath:ETAString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self getETAResponse:operation.responseString.JSONValue];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
    }];
}

-(void)getETAResponse: (NSDictionary*) response
{
    NSDictionary *dict = response;
}

-(void)startTimer {
    timerJobStarted = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerTick:) userInfo:nil repeats:YES];
    //    [[NSRunLoop mainRunLoop] addTimer:timerJobStarted forMode:NSRunLoopCommonModes];
}
- (void)timerTick:(NSTimer *)timer
{
    timeSec++;
    if (timeSec == 60)
    {
        timeSec = 0;
        timeMin++;
    }
    if (timeMin == 60) {
        timeMin = 0;
        timeHr++;
    }
    
    NSString* timeNow = [NSString stringWithFormat:@"%02d:%02d:%02d",timeHr, timeMin, timeSec];
    //Display on your label
    //[timeLabel setStringValue:timeNow];
    //    UIView *view = (UIView *)[_topView viewWithTag:212];
    //    UILabel *labelTimer = (UILabel *)[view viewWithTag:420];
    self.timeLbl.text = timeNow;
}
//Call this to stop the timer event(could use as a 'Pause' or 'Reset')
-(void)stopTimer
{
    [timerJobStarted invalidate];
    NSString *finalTimeNow = [NSString stringWithFormat:@"%02d:%02d:%02d",timeHr, timeMin, timeSec];
    //    UIView *view = (UIView *)[_topView viewWithTag:212];
    //    UILabel *labelTimer = (UILabel *)[view viewWithTag:420];
    self.timeLbl.text = finalTimeNow;
}


-(IBAction)callButtonPressed:(id)sender {
    
    NSString *makeCall = NSLocalizedString(@"Do you want to make a call on", @"Do you want to make a call on");
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Call", @"Call") message:[NSString stringWithFormat:@"%@ %@",makeCall,passDetail[@"mobile"]] delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", @"Cancel") otherButtonTitles:NSLocalizedString(@"Call", @"Call"), nil];
    [alert show];
}
-(void)alertView:(UIAlertView *)alertVie didDismissWithButtonIndex:(NSInteger) buttonIndex
{
    if (buttonIndex == 0)
    {
    }
    else if (buttonIndex == 1)
    {
        NSString *phoneNumber = passDetail[@"mobile"];
        NSString *phoneURLString = [NSString stringWithFormat:@"tel:%@", phoneNumber];
        NSURL *phoneURL = [NSURL URLWithString:phoneURLString];
        [[UIApplication sharedApplication] openURL:phoneURL];
    }
}


/**
 *  Updating drop address from push
 */

-(void)checkBookingStatus
{
    PMDReachabilityWrapper * reachabilty = [PMDReachabilityWrapper sharedInstance];
    if ([reachabilty isNetworkAvailable])
    {
    NSLog(@"checkBookingStatus............................");
    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    
    NSString *deviceID = [[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey];
    NSString *appointmntDate; //= appDate
    appointmntDate = @"";
    
    NSString *currentDate = [Helper getCurrentDateTime];
    
    NSDictionary *params = @{@"ent_sess_token":sessionToken,
                             @"ent_dev_id":deviceID,
                             @"ent_user_type":@"1",
                             @"ent_appnt_dt":appointmntDate,
                             @"ent_date_time":currentDate,
                             };
    NSLog(@"PARAMS BOOKING: %@", params);
    NetworkHandler *handler = [NetworkHandler sharedInstance];
    [[ProgressIndicator sharedInstance] showPIOnView:self.view withMessage:@"Loading.."];
    [handler composeRequestWithMethod:@"getApptStatus" paramas:params onComplition:^(BOOL succeeded, NSDictionary *response) {
        [[ProgressIndicator sharedInstance] hideProgressIndicator];
        if (succeeded) {
            NSLog(@"Booking Response: %@", response);
            if ([response[@"data"] count] == 0) {
                return;
            }
            else
            {
                dropLatAdded = response[@"data"][0][@"dropLat"];
                dropLongAdded = response[@"data"][0][@"dropLong"];
            }
            if ([response[@"status"] integerValue] == 4) {
                
                NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
                [ud setBool:NO forKey:@"isBooked"];
                if ([ud objectForKey:@"onTheWayPressed"]) {
                    [ud removeObjectForKey:@"onTheWayPressed"];
                }
                if ([ud objectForKey:@"iHaveArrivedPressed"]) {
                    [ud removeObjectForKey:@"iHaveArrivedPressed"];
                }
                if ([ud objectForKey:@"beginTripPressed"]) {
                    [ud removeObjectForKey:@"beginTripPressed"];
                }
                if ([ud objectForKey:@"passengerDroppedPressed"]) {
                    [ud removeObjectForKey:@"passengerDroppedPressed"];
                }
                [ud synchronize];
                
                [self.navigationController popToRootViewControllerAnimated:YES];
                [Helper showAlertWithTitle:@"Message" Message:@"Passenger cancelled the booking"];
            }
            else
            {
                if ([response[@"errFlag"] integerValue] == 0) {
                    
                    [passDetail removeAllObjects];
                    passDetail = response[@"data"][0];
                    if (_isDriverArrived) {
                        [mapView_ clear];
                        
                        [self getCurrentLocation];
                        LocationTracker *tracker = [LocationTracker sharedInstance];
                        [self setStartLocationCoordinates:tracker.lastLocaiton.coordinate.latitude Longitude:tracker.lastLocaiton.coordinate.longitude];
                        UILabel *labelAddressValue = (UILabel*)[_topView viewWithTag:211];
                        UILabel *labelAddress = (UILabel *)[_topView viewWithTag:210];
                        CGRect frame = labelAddressValue.frame;
                        frame.size.height = 82;
                        frame.size.width = 250;
                        frame.origin.y = CGRectGetMaxY(labelAddress.frame)+6;
                        labelAddressValue.frame = frame;
                        NSLog(@"labelAddressValue: %@", labelAddressValue);
                        if (_statusButtonTag != 400 || _statusButtonTag != 500)
                        {
                            self.pickUpAndDropAddressLbl.text = [NSString stringWithFormat:@"%@ %@",passDetail[@"dropAddr1"],passDetail[@"dropAddr2"] ];
                        }
                    }
                }
            }
        }
    }];
    }else{
        ProgressIndicator * pi = [ProgressIndicator sharedInstance];
        [pi showMessage:kNetworkErrormessage On:self.view];
    }
}

/**
 *  Convert a view from image
 *
 *  @param view
 *
 *  @return UIImage
 */
- (UIImage *)imageFromView:(UIView *) view
{
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(view.frame.size, NO, [[UIScreen mainScreen] scale]);
    } else {
        UIGraphicsBeginImageContext(view.frame.size);
    }
    [view.layer renderInContext: UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

//declaring uialertview delegate for phone call
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 100)
    {
        if (buttonIndex == 1)
        {
            [self callNumber];
        }
    }
}
-(void)checkLocationServices
{
    if(![CLLocationManager locationServicesEnabled] || [CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorizedAlways)
    {
        LocationServicesViewController *locationVC = [self.storyboard instantiateViewControllerWithIdentifier:@"locationVC"];
        UINavigationController *navigationVC  = [[UINavigationController alloc] initWithRootViewController:locationVC];
        [self.navigationController presentViewController:navigationVC animated:YES completion:nil];
    }
}
-(void)callNumber
{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"tel://"]])
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://+%@",[passDetail[@"mobile"] substringFromIndex:1]]]];
    }
    else
    {
        [Helper showAlertWithTitle:@"Error" Message:@"Your device doesn't support Call!"];
    }
}

-(void)startUpdatingDistance
{
    NSTimeInterval time = 5.0;
    _locationUpdateTimer =
    [NSTimer scheduledTimerWithTimeInterval:time
                                     target:self
                                   selector:@selector(distanceCalculation)
                                   userInfo:nil
                                    repeats:YES];
}

-(void)distanceCalculation
{
    NSUserDefaults *ud =[NSUserDefaults standardUserDefaults];
    distanceBtween=0.00;
    locationManagerr = [[CLLocationManager alloc] init];
    locationManagerr.distanceFilter = kCLDistanceFilterNone;
    locationManagerr.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManagerr startUpdatingLocation];
    [locationManagerr requestAlwaysAuthorization]; //Note this one
    float dropLat =locationManagerr.location.coordinate.latitude; //locaitontraker.lastLocaiton.coordinate.latitude;
    float dropLog = locationManagerr.location.coordinate.longitude; //locaitontraker.lastLocaiton.coordinate.longitude;
    float pickLat;
    float pickLog;
    if ( [ud objectForKey:@"picklat"] && [ud objectForKey:@"picklog"] ) {
        pickLat = [[ud objectForKey:@"picklat"] doubleValue];
        pickLog = [[ud objectForKey:@"picklog"] doubleValue];
        NSLog(@"Lat............ : %f  Long............. : %f",pickLat , pickLog);
        [ud setObject:[NSString stringWithFormat:@"%f",dropLat] forKey:@"picklat"];
        [ud setObject:[NSString stringWithFormat:@"%f",dropLog] forKey:@"picklog"];
        [ud synchronize];
        CLLocation *itemLoc = [[CLLocation alloc] initWithLatitude:pickLat longitude:pickLog];
        CLLocation *current = [[CLLocation alloc] initWithLatitude:dropLat longitude:dropLog];
        NSInteger itemDist = [current distanceFromLocation:itemLoc];
        distanceBtween = itemDist;
        if ([ud objectForKey:@"distance"])
        {
            if ([[ud objectForKey:@"distance"]integerValue] >= 20)
            {
                distanceBtween = distanceBtween + [[ud objectForKey:@"distance"]integerValue];
            }
            [ud setObject:[NSString stringWithFormat:@"%.2ld",(long)distanceBtween] forKey:@"distance"];
            self.distanceLbl.text=[NSString stringWithFormat:@"%.2f miles",[[ud objectForKey:@"distance"]floatValue]/1609.344];
            [ud synchronize];
        }
        else
        {
            [ud setObject:[NSString stringWithFormat:@"%0.2f miles",(long)distanceBtween/1609.344] forKey:@"distance"];
            [ud synchronize];
        }
    }
}

@end
