//
//  BookingNotificationView.m
//  Shiply
//
//  Created by Rathore on 20/10/14.
//  Copyright (c) 2014 3Embed. All rights reserved.
//

#import "BookingNotificationView.h"
#import "UIImage+Blur.h"
#import "UIImage+Screenshot.h"
#import "NetworkHandler.h"


static BOOL IsPresenting;

@interface BookingNotificationView ()
@property (strong, nonatomic) IBOutlet UIView *view;
@property(nonatomic,strong)IBOutlet UILabel *labelPickupAddress;
@property(nonatomic,strong)IBOutlet UILabel *labelTimer;
@property(nonatomic,strong)IBOutlet UILabel *labelBookingTime;
@property(nonatomic,strong)IBOutlet UILabel *labelDistance;
@property(nonatomic,strong)IBOutlet UIButton *buttonAccept;
@property(nonatomic,strong)IBOutlet UIButton *buttonReject;

@property (strong, nonatomic) UIImageView *backgroundView;
@property (strong, nonatomic) UIView *backgroundBlackView;

@property(nonatomic,strong)NSString *address;
@property(nonatomic,strong)NSString *time;
@property(nonatomic,strong)NSString *distance;
@property(nonatomic,strong)NSDictionary *bookingNotification;
@property(nonatomic,strong) NSTimer *timer;
@property(nonatomic,assign)int secondsLeft;
@property (nonatomic, assign) CGFloat screenBlurLevel;
@property (nonatomic,assign) BOOL isLaterBooking;


@end



@implementation BookingNotificationView

- (id)init
{
    self = [super init];
    
    if (self) {
        
        self.backgroundColor = [UIColor redColor];
        
        [[[NSBundle mainBundle] loadNibNamed:@"BookingNotificationView" owner:self options:nil]objectAtIndex:0];
        //[self addSubview:[self customView]];
        
        
        self.view.frame = CGRectMake(-160, -130/2, 320, 130);
        [self addSubview:self.view];
      
        //address label
//        self.labelPickupAddress = [[UILabel alloc] initWithFrame:CGRectMake(14, 15, 258, 45)];
//        [Helper setToLabel:self.labelPickupAddress Text:@"" WithFont:Robot_Regular FSize:16 Color:[UIColor blackColor]];
//        self.labelPickupAddress.numberOfLines = 2;
//        [self addSubview:self.labelPickupAddress];
//        
//        
//        UILabel *labelBookingFor = [[UILabel alloc] initWithFrame:CGRectMake(14, 15, 258, 45)];
//        [Helper setToLabel:labelBookingFor Text:@"Booking for" WithFont:Robot_Regular FSize:14 Color:[UIColor blackColor]];
//        [self addSubview:labelBookingFor];
//        
//        self.labelBookingTime = [[UILabel alloc] initWithFrame:CGRectMake(14, 15, 258, 45)];
//        [Helper setToLabel:self.labelBookingTime Text:@"" WithFont:Robot_Regular FSize:16 Color:[UIColor blackColor]];
//        self.labelBookingTime.textAlignment = NSTextAlignmentCenter;
//        [self addSubview:self.labelBookingTime];
//
//        self.labelTimer = [[UILabel alloc] initWithFrame:CGRectMake(14, 15, 258, 45)];
//        [Helper setToLabel:self.labelTimer Text:@"30" WithFont:Robot_Regular FSize:16 Color:[UIColor blackColor]];
//        self.labelTimer.textAlignment = NSTextAlignmentCenter;
//        [self addSubview:self.labelTimer];
//        
//        UILabel *secondsLeft = [[UILabel alloc] initWithFrame:CGRectMake(14, 15, 258, 45)];
//        [Helper setToLabel:secondsLeft Text:@"seconds left to accept" WithFont:Robot_Regular FSize:14 Color:[UIColor blackColor]];
//        [self addSubview:secondsLeft];
        
        UIButton *buttonAccept = [UIButton buttonWithType:UIButtonTypeCustom];
        [buttonAccept addTarget:self action:@selector(buttonAcceptRejectClicked:) forControlEvents:UIControlEventTouchUpInside];
        [buttonAccept setBackgroundImage:[UIImage imageNamed:@"home_acceptreject_on"] forState:UIControlStateNormal];
        buttonAccept.titleLabel.font = [UIFont fontWithName:Robot_Regular size:15];
        [buttonAccept setTitle:@"Accept" forState:UIControlStateNormal];
        [buttonAccept setTitleColor:[UIColor greenColor] forState:UIControlStateNormal];
        [buttonAccept setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
        buttonAccept.frame = CGRectMake(0, 130, 158, 45);
        buttonAccept.tag = 100;
        [self addSubview:buttonAccept];
        
        
        UIButton *buttonReject = [UIButton buttonWithType:UIButtonTypeCustom];
        [buttonReject addTarget:self action:@selector(buttonAcceptRejectClicked:) forControlEvents:UIControlEventTouchUpInside];
        [buttonReject setBackgroundImage:[UIImage imageNamed:@"home_acceptreject_on"] forState:UIControlStateNormal];
        [buttonReject setTitle:@"Reject" forState:UIControlStateNormal];
        buttonReject.titleLabel.font = [UIFont fontWithName:Robot_Regular size:15];
        [buttonReject setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        [buttonReject setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
        buttonReject.frame = CGRectMake(160, 130, 160, 45);
        buttonReject.tag = 200;
        [self addSubview:buttonReject];
        

        
        CGRect frame = [[UIScreen mainScreen] bounds];
        self.backgroundView = [[UIImageView alloc] initWithFrame:frame];
        self.backgroundView.backgroundColor = [UIColor clearColor];
        
        self.backgroundBlackView = [[UIView alloc] initWithFrame:frame];
        self.backgroundBlackView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:.1f];
        self.backgroundBlackView.userInteractionEnabled = YES;
        
       // UIGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hide)];
       // [self.backgroundBlackView addGestureRecognizer:gesture];
        
        self.screenBlurLevel = .6;
        _secondsLeft = 28;
        
        self.frame = CGRectMake(0, -175, 320, 175);
        
        NSLog(@"frame %@",NSStringFromCGRect(self.frame));
    }
    
    return self;
}
-(id)customView
{
    BookingNotificationView *customView = [[[NSBundle mainBundle] loadNibNamed:@"BookingNotificationView" owner:nil options:nil] lastObject];
    
    // make sure customView is not nil or the wrong class!
    if ([customView isKindOfClass:[BookingNotificationView class]])
        return customView;
    else
        return nil;
}

-(void)bookingTimerOver{
    if (_secondsLeft == 0) {
        [self hide];
        [_timer invalidate];
    }
    else {
        _secondsLeft--;
        self.labelTimer.text = [NSString stringWithFormat:@"%d",_secondsLeft];
    }
}

-(void)updateBookingInformation:(NSDictionary*)bookingDetail bookingNotificaiton:(NSDictionary*)notification{
    
    _timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(bookingTimerOver) userInfo:nil repeats:YES];
    
    if (bookingDetail == nil) {
        //self.address = @"hello";
       // [self showWithCompletion];
    }
    else {
        self.bookingNotification = notification;
        
        if ([bookingDetail[@"apptType"] integerValue] == 2) { //later Booking
            
            _isLaterBooking = YES;
            
            NSDateFormatter *df = [self serverformatter];
            
            NSDate *pickupTime = [df dateFromString:bookingDetail[@"apptDt"]];
            
            
            df = [self formatter];
            
            NSString *time = [df stringFromDate:pickupTime];
            
            self.time = time;
            
        }
        else {
            
            _isLaterBooking = NO;
            self.time = @"NOW";
        }
        
        self.address = bookingDetail[@"addr1"];
        
        self.distance = bookingDetail[@"apptDis"];
        
        
        [self showWithCompletion];
    }
    

    
}


-(IBAction)buttonAcceptRejectClicked:(UIButton*)sender{
    
    if (sender.tag == 100) {
        [self respondToNewBookingFor:0];
    }
    else if (sender.tag == 200)
    {
        
        PMDReachabilityWrapper * rechability = [PMDReachabilityWrapper sharedInstance];
        if ([rechability isNetworkAvailable]) {
        NetworkHandler * handler = [NetworkHandler sharedInstance];
        
        NSDictionary * dictP = [[NSUserDefaults standardUserDefaults] objectForKey:@"PUSH"];
        //NSDictionary * dictP = [dict objectForKey:@"aps"];
        
        NSDictionary *queryParams;
        queryParams = [NSDictionary dictionaryWithObjectsAndKeys:
                       [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],KDAcheckUserSessionToken,
                       [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],kSMPCommonDevideId,
                       [dictP objectForKey:@"e"],kSMPRespondPassengerEmail,
                       [dictP objectForKey:@"dt"], kSMPRespondBookingDateTime,
                       constkNotificationTypeBookingReject,kSMPRespondResponse,
                       constkNotificationTypeBookingType,kSMPRespondBookingType,
                       [Helper getCurrentDateTime],kSMPCommonUpDateTime, nil];
        
        NSLog(@"buttonAcceptRejectClicked>>>>>>>>>>%@",queryParams);
        [handler composeRequestWithMethod:MethodRespondToAppointMent
                                  paramas:queryParams
                             onComplition:^(BOOL succeeded, NSDictionary *response) {
                          
                                if (succeeded) { //handle success response
                                    
                                    [self hide];
                                }
                                else{//error
                                    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
                                    [pi hideProgressIndicator];
                                }
                            }];
        }
        else
        {
            ProgressIndicator * pi = [ProgressIndicator sharedInstance];
            [pi showMessage:kNetworkErrormessage On:self.view];
        }
    }
}
-(void)respondToNewBookingFor:(int)bookingAction{
    
    
    
   // NSDictionary * dictP = [[NSUserDefaults standardUserDefaults] objectForKey:@"PUSH"];
   
    
    NSDictionary *queryParams;
    if (_isLaterBooking) {
        
        queryParams = [NSDictionary dictionaryWithObjectsAndKeys:
                       [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],KDAcheckUserSessionToken,
                       [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],kSMPCommonDevideId,
                       [self.bookingNotification objectForKey:@"e"],kSMPRespondPassengerEmail,
                       [self.bookingNotification objectForKey:@"dt"], kSMPRespondBookingDateTime,
                       constkNotificationTypeBookingAccept,kSMPRespondResponse,
                       constkNotificationTypeBookingTypeLater,kSMPRespondBookingType,
                       [Helper getCurrentDateTime],kSMPCommonUpDateTime, nil];
    }
    else {
        queryParams = [NSDictionary dictionaryWithObjectsAndKeys:
                       [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],KDAcheckUserSessionToken,
                       [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],kSMPCommonDevideId,
                       [self.bookingNotification objectForKey:@"e"],kSMPRespondPassengerEmail,
                       [self.bookingNotification objectForKey:@"dt"], kSMPRespondBookingDateTime,
                       constkNotificationTypeBookingAccept,kSMPRespondResponse,
                       constkNotificationTypeBookingType,kSMPRespondBookingType,
                       [Helper getCurrentDateTime],kSMPCommonUpDateTime, nil];
    }
    
    
    
    
    
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:MethodRespondToAppointMent
                                    paramas:queryParams
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   
                                   if (success) { //handle success response
                                       
                                       
                                       
                                       if (_isLaterBooking) {
                                           
                                           
                                           [Helper showAlertWithTitle:@"Message" Message:@"Your Booking is Confirmed"];
                                           
                                           
                                       }
                                       else {
                                           [self sendRequestForUpdateStatus];
                                       }
                                       
                                   }
                                   else{//error
                                       ProgressIndicator *pi = [ProgressIndicator sharedInstance];
                                       [pi hideProgressIndicator];
                                   }
                               }];
    
    
}
-(void)sendRequestForUpdateStatus
{
    PMDReachabilityWrapper * rechability = [PMDReachabilityWrapper sharedInstance];
    if ([rechability isNetworkAvailable])
    {
        NetworkHandler * handler = [NetworkHandler sharedInstance];
        
        
        NSDictionary *queryParams;
    queryParams = [NSDictionary dictionaryWithObjectsAndKeys:
                   [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],KDAcheckUserSessionToken,
                   [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],kSMPCommonDevideId,
                   self.bookingNotification[@"e"],kSMPRespondPassengerEmail,
                   self.bookingNotification[@"dt"], kSMPRespondBookingDateTime,
                   constkNotificationTypeBookingOnTheWay,kSMPRespondResponse,
                   @"testing",kSMPRespondDocNotes,
                   [Helper getCurrentDateTime],kSMPCommonUpDateTime, nil];
    
   [handler composeRequestWithMethod:MethodupdateApptStatus
                             paramas:queryParams
                        onComplition:^(BOOL succeeded, NSDictionary *response) {
                      
    
                                  if (succeeded) { //handle success response
                                      [Helper showAlertWithTitle:@"Message" Message:@"Your booking is Confirmed"];
                                      [self hide];
                                  }
                                  else{//error
                                      ProgressIndicator *pi = [ProgressIndicator sharedInstance];
                                      [pi hideProgressIndicator];
                                  }
                              }];
    
    }
    else{
        ProgressIndicator * pi = [ProgressIndicator sharedInstance];
        [pi showMessage:kNetworkErrormessage On:self.view];
    }
    
}

- (void)showWithCompletion
{
    if (IsPresenting) {
        return;
    }
    
    IsPresenting = YES;
    
    self.labelPickupAddress.text = self.address;
    self.labelBookingTime.text = self.time;
    
    //[self adjustLayout];
    
    
    UIApplication *application = [UIApplication sharedApplication];
    
    CGRect frame = self.view.frame;
    frame.origin.y = -([self originY] + CGRectGetHeight(self.view.frame));
    frame.origin.x = -self.view.frame.size.width/2;
   // self.frame = frame;
    
    if (self.screenBlurLevel > 0) {
        UIImage *screenshot = [UIImage screenshot];
        NSData *imageData = UIImageJPEGRepresentation(screenshot, .0001);
        UIImage *blurredSnapshot = [[UIImage imageWithData:imageData] blurredImage:_screenBlurLevel];
        
        self.backgroundView.image = blurredSnapshot;
    } else {
        self.backgroundView.image = nil;
    }
    
    self.backgroundView.alpha = 0;
    
    self.backgroundBlackView.alpha = 0;
    
    NSInteger index = [[application keyWindow].subviews count];
    
    //[[application keyWindow] addSubview:self];
    
    [[application keyWindow] insertSubview:self atIndex:index];
    [[application keyWindow] insertSubview:self.backgroundBlackView atIndex:index];
    [[application keyWindow] insertSubview:self.backgroundView atIndex:index];
    
    CGRect viewFrame = self.frame;
    viewFrame.origin.y = 0;
//    viewFrame.origin.x = -self.view.frame.size.width/2;
    
    //self.view.backgroundColor = [UIColor redColor];
    [UIView animateWithDuration:.5 animations:^{
        self.frame = viewFrame;
        self.backgroundView.alpha = 1;
        self.backgroundBlackView.alpha = 1;
    } completion:^(BOOL finished) {
        if (finished) {
//            if ([self.delegate respondsToSelector:@selector(didPresentNZAlertView:)]) {
//                [self.delegate didPresentNZAlertView:self];
//            }
        }
        //[self performSelector:@selector(hide) withObject:nil afterDelay:30];
    }];
}
- (CGFloat)originY
{
    CGFloat originY = 0;
    
    UIApplication *application = [UIApplication sharedApplication];
    
    if (!application.statusBarHidden) {
        originY = [application statusBarFrame].size.height;
    }
    
    return originY;
}

- (void)hide
{
   
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y = -400;//-(CGRectGetHeight(self.view.frame)*2 + [self originY]);
    viewFrame.origin.x = -160;
    
    [UIView animateWithDuration:.2 animations:^{
        self.frame = viewFrame;
        self.backgroundView.alpha = 0;
        self.backgroundBlackView.alpha = 0;
    } completion:^(BOOL finished) {
        if (finished) {
            [self.backgroundBlackView removeFromSuperview];
            [self.backgroundView removeFromSuperview];
            [self removeFromSuperview];
            
            IsPresenting = NO;
            
//            if ([self.delegate respondsToSelector:@selector(NZAlertViewDidDismiss:)]) {
//                [self.delegate NZAlertViewDidDismiss:self];
//            }
            
//            if (self.completion) {
//                self.completion();
//                self.completion = nil;
//            }
        }
    }];
}


- (NSDateFormatter *)serverformatter {
    
    //EEE - day(eg: Thu)
    //MMM - month (eg: Nov)
    // dd - date (eg 01)
    // z - timeZone
    
    //eg : @"EEE MMM dd HH:mm:ss z yyyy"
    
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"YYYY-MM-dd HH:mm:ss";
    });
    return formatter;
}
- (NSDateFormatter *)formatter {
    
    //EEE - day(eg: Thu)
    //MMM - month (eg: Nov)
    // dd - date (eg 01)
    // z - timeZone
    
    //eg : @"EEE MMM dd HH:mm:ss z yyyy"
    
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"dd MMM HH:mm";
    });
    return formatter;
}


@end
