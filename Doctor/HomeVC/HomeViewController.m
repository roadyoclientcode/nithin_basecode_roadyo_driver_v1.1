
//  HomeViewController.m
//  Doctor
//
//  Created by Rahul Sharma on 17/04/14.
//  Copyright (c) 2014 3Embed. All rights reserved.
//

#import "HomeViewController.h"
#import "DAPagesContainer.h"
#import "CustomNavigationBar.h"
#import "CustomMarkerView.h"
#import "XDKAirMenuController.h"
#import "PMDReachabilityWrapper.h"
#import "SliderViewController.h"
#import "WildcardGestureRecognizer.h"
#import "ContentViewController.h"
#import "ProgressIndicator.h"
#import "Helper.h"
#import "WebServiceConstants.h"
#import "AppConstants.h"
#import "passDetail.h"
#import "OnBookingViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "DirectionService.h"
#import <CoreLocation/CoreLocation.h>
#import "DoctorDetailView.h"
#import "AppointedDoctor.h"
#import "HelpViewController.h"
#import "VNHPubNubWrapper.h"
#import "LocationTracker.h"
#import "UploadFiles.h"
#import "BookingHistoryViewController.h"
#import "CheckBookingStatus.h"
#import "BookingDetail.h"
#import "UpdateBookingStatus.h"
#import "MathController.h"
#import "SplashViewController.h"
#import "MenuViewController.h"
#import <UIImageView+WebCache.h>
#import "UIImageView+WebCache.h"
#import "LocationServicesViewController.h"
#import "NewBookingHistoryViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "acceptRejectView.h"

#define mapZoomLevel 18


@interface HomeViewController ()<CustomNavigationBarDelegate,XDKAirMenuDelegate,CLLocationManagerDelegate,ViewPagerDelegate,VNHPubNubWrapperDelegate,UploadFileDelegate,UserDelegate,UIGestureRecognizerDelegate,DismissDelegate>
{
    GMSMapView *mapView_;
    NSMutableArray *waypoints_;
    NSMutableArray *waypointStrings_;
    GMSGeocoder *geocoder_;
    IBOutlet  UIView *customMapView;
    GMSMarker *markerImage;
    // RoadyoPubNubWrapper *pubNub;
    AVAudioPlayer *audioPlayer;
    NSDictionary *dict;
    BOOL isAlreadyOpened;
    BOOL isFirstBoookingCame;

}

@property(nonatomic,strong)NSString *startLocation;
@property(nonatomic,strong)NSString *destinationLocation;
@property(nonatomic,strong)NSString *pubnubChannelName;
@property(nonatomic,strong)NSString *patientPubNubChannel;

@property(nonatomic,strong)CLLocationManager *locationManager;
@property(nonatomic,assign)CLLocationCoordinate2D previouCoord;

@property(nonatomic,strong)GMSMutablePath *mutablePath;
@property(nonatomic,strong)GMSMarker *destinationMarker;
@property(nonatomic,strong)GMSMarker *currentLocationMarker;
@property(nonatomic,strong)GMSMarker *bookingMarker;
@property (weak, nonatomic) IBOutlet UIView *tripDetailsBottomView;

@property(nonatomic,assign)BOOL isPathPlotted;
@property(nonatomic,assign)BOOL isUpdatedLocation;
@property(nonatomic,assign)BOOL isNewBookingInProcess;
@property (weak, nonatomic) IBOutlet UIView *walletView;

@property(nonatomic,assign)BOOL isLaterBooking;
@property (weak, nonatomic) IBOutlet UILabel *todaysTripLbl;
@property (weak, nonatomic) IBOutlet UILabel *lastTripPriceLbl;
@property (weak, nonatomic) IBOutlet UILabel *lastTripTimeLbl;
@property (weak, nonatomic) IBOutlet UILabel *typeLbl;


@property (nonatomic, strong) NSMutableArray *arrBooking;
@property(nonatomic,strong)NSMutableDictionary *allMarkers;
@property(nonatomic,strong)WildcardGestureRecognizer * tapInterceptor;

@property(nonatomic,assign) float currentLatitude;
@property(nonatomic,assign) float currentLongitude;

@property (weak, nonatomic) IBOutlet UILabel *earningsTodayLbl;
@property (weak, nonatomic) IBOutlet UILabel *walletLbl;

@property (strong,nonatomic)IBOutlet  UIView *acceptRejectBtnView;
@property (strong,nonatomic)IBOutlet  UIView *PickUpAndDropView;
@property (strong,nonatomic)IBOutlet  UILabel *labelBookingId;

@property (nonatomic) NSUInteger numberOfTabs;
@property(nonatomic,assign) BookingNotificationType bookingStatus;
@property LocationTracker * locationTracker;
@property (nonatomic) NSTimer* locationUpdateTimer;

@property (nonatomic,strong) CustomNavigationBar *customNavigationBarView;

@property (nonatomic, weak) IBOutlet UIView *laterBookingView;
@property (weak, nonatomic) IBOutlet UIButton *availablePauseButtonClickOutlet;
@property (weak, nonatomic) IBOutlet UIView *bottomLastBookingView;


@property(nonatomic,weak) IBOutlet UILabel *laterBookingTimeDisplay;

@property (strong, nonatomic) NSDictionary *bookingResponse;
@property (assign, nonatomic) float expireTime;
@property (strong, nonatomic) UIImage *carImage;


-(IBAction)btnAcceptRejectClicked:(id)sender;

@end


@implementation HomeViewController
@synthesize arrBooking;
@synthesize acceptRejectBtnView;
@synthesize PickUpAndDropView;
@synthesize dictPush;
@synthesize timer;
@synthesize counter;
@synthesize dropLocationbackgroundImage;
@synthesize walletView;
@synthesize walletLbl;
@synthesize todaysTripLbl;
@synthesize earningsTodayLbl;
@synthesize lastTripPriceLbl;
@synthesize lastTripTimeLbl;
@synthesize typeLbl;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


#pragma mark-view life cycle

- (void)viewDidLoad
{
    counter = 0;
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    waypoints_ = [[NSMutableArray alloc]init];
    waypointStrings_ = [[NSMutableArray alloc]initWithCapacity:2];
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:37.778376 longitude:-122.409853 zoom:mapZoomLevel];
    customMapView.backgroundColor = [UIColor clearColor];
    CGRect rectMap = CGRectMake(0, 0, 320, self.view.bounds.size.height-122);
    mapView_ = [GMSMapView mapWithFrame:rectMap camera:camera];
    mapView_.delegate = self;
    mapView_.myLocationEnabled = YES;
    mapView_.settings.myLocationButton = YES;
    mapView_.settings.compassButton = YES;
    [self addShadowToView:walletView];
    [self addShadowToViewOne:self.tripDetailsBottomView];
    customMapView = mapView_;
    [self checkLocationServices];
    markerImage = [[GMSMarker alloc] init];
    [self.view addSubview:customMapView];
    [self.view bringSubviewToFront:PickUpAndDropView];
    [self.view bringSubviewToFront:acceptRejectBtnView];
    [self.view bringSubviewToFront:walletView];
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeXDKMenu)];
    gesture.delegate = self;
    isAlreadyOpened = NO;
    isFirstBoookingCame = NO;
    [self.view addGestureRecognizer:gesture];
    _driverStatus = KDriverStatusOnline;
    [self.view bringSubviewToFront:_bottomLastBookingView];
    [Helper setToLabel:labelPickupTitle Text:NSLocalizedString(@"Pickup Location", @"Pickup Location") WithFont:Robot_CondensedRegular FSize:11 Color:UIColorFromRGB(0x6e6e6e)];
    [Helper setToLabel:labelDropOffTitle Text:NSLocalizedString(@"DropOff Location", @"DropOff Location") WithFont:Robot_CondensedRegular FSize:11 Color:UIColorFromRGB(0x6e6e6e)];
    [Helper setToLabel:labelPickeup  Text:nil WithFont:Robot_Regular FSize:13 Color:UIColorFromRGB(0x000000)];
    [Helper setToLabel:labelDropOff Text:nil WithFont:Robot_Regular FSize:13 Color:UIColorFromRGB(0x000000)];
    [Helper setToLabel:labelPickUpdistance Text:@"" WithFont:Robot_CondensedRegular FSize:16 Color:UIColorFromRGB(0x333333)];
    [Helper setToLabel:labelDropOffdistance Text:@"" WithFont:Robot_CondensedRegular FSize:16 Color:UIColorFromRGB(0x333333)];
    [Helper setToLabel:labelPickUpMile Text:[NSString stringWithFormat:@"%@",[Helper getDistanceUnit]] WithFont:Robot_Regular FSize:11 Color:UIColorFromRGB(0x000000)];
    [Helper setToLabel:labelDropOffMile Text:[NSString stringWithFormat:@"%@",[Helper getDistanceUnit]] WithFont:Robot_Regular FSize:11 Color:UIColorFromRGB(0x000000)];
    [self addCustomNavigationBar];
    
    //
    [self driverStateChanges];
    
    CLLocationCoordinate2D coordinate = [self getLocation];
    self.currentLatitude = coordinate.latitude;
    self.currentLongitude = coordinate.longitude;
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isBooked"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[Helper sharedInstance] setCurrencyUnit];
}

-(void)closeXDKMenu
{
    
}
-(void)subscribeToPubnubPresenceChannel
{
    VNHPubNubWrapper *pubNub = [VNHPubNubWrapper sharedInstance];
    [pubNub subscribeToMyPresenceChannel];
}
-(void)navigateToBookingScreen{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                @"Main" bundle:[NSBundle mainBundle]];
    OnBookingViewController *booking = [storyboard instantiateViewControllerWithIdentifier:@"OnBookingViewControllerID"];
    booking.bookingStatus = _bookingStatus;
    booking.passDetail = dictpassDetail;
    
    
    [self.navigationController pushViewController:booking animated:YES];
}

- (void) viewDidAppear:(BOOL)animated
{
    CheckBookingStatus *status = [CheckBookingStatus sharedInstance];
    [status checkOngoingAppointmentStatus:nil];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    PriveMdAppDelegate *appDelegate = (PriveMdAppDelegate*)[UIApplication sharedApplication].delegate;
    
    if (IS_SIMULATOR)
    {
        _currentLatitude = 13.028866;
        _currentLongitude = 77.589760;
        
        
        CLLocationCoordinate2D locationCord = CLLocationCoordinate2DMake(_currentLatitude,_currentLongitude);
        mapView_.camera = [GMSCameraPosition cameraWithTarget:locationCord
                                                         zoom:14];
        GMSMarker *marker = [[GMSMarker alloc] init];
        marker.position = CLLocationCoordinate2DMake(_currentLatitude, _currentLongitude);
        marker.title = NSLocalizedString(@"It's Me", @"It's Me");
        marker.snippet = NSLocalizedString(@"Current Location", @"Current Location");
        marker.map = mapView_;
    }
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0,0,10,20)];
    UIImageView *carIcon = [[UIImageView alloc] init];
    NSString *strImageUrl = [NSString stringWithFormat:@"%@%@",baseUrlForOriginalImage,[[NSUserDefaults standardUserDefaults]objectForKey:kNSURoadyoCarImage]];
    [carIcon sd_setImageWithURL:[NSURL URLWithString:strImageUrl]
               placeholderImage:nil
                      completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
                          
                          if(image)
                          {
                              image = [self imageWithImage:image scaledToSize:[self makeSize:image.size fitInSize:CGSizeMake(50, 50)]];
                              markerImage.icon = image;
                              [view addSubview:carIcon];
                              _carImage = image;
                              return;
                          }
                      }];
    markerImage.position = CLLocationCoordinate2DMake(_currentLatitude, _currentLongitude);
    markerImage.icon = _carImage;//[UIImage imageNamed:@"car_type_two.png"];
    markerImage.map = mapView_;
    markerImage.groundAnchor = CGPointMake(0.5, 0.5);
    _previouCoord = CLLocationCoordinate2DMake(_currentLatitude, _currentLongitude);
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [appDelegate subscribeToPubnubChannel];
        [self startUpdatingLocationToServer];
    });
    // Ask for My Location data after the map has already been added to the UI.
    dispatch_async(dispatch_get_main_queue(), ^{
        mapView_.myLocationEnabled = YES;
    });
    if (!appDelegate.isNewBooking)
    {
        PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
        if ( [reachability isNetworkAvailable])
        {
            ProgressIndicator *pi = [ProgressIndicator sharedInstance];
            [pi showPIOnView:self.view withMessage:NSLocalizedString(@"Checking booking status..", @"Checking booking status..")];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"bid"];
            CheckBookingStatus *status = [CheckBookingStatus sharedInstance];
            status.callblock = ^(int status, NSDictionary *dictionary)
            {
                if (dictionary != nil)
                {
                earningsTodayLbl.text = [NSString stringWithFormat:@"%0.002f",[dictionary[@"earningsToday"] floatValue]];
                todaysTripLbl.text = [NSString stringWithFormat:@"%d",[dictionary[@"tripsToday"] integerValue]];
                lastTripPriceLbl.text = [NSString stringWithFormat:@"%0.002f",[dictionary[@"lastTripPrice"] floatValue]];
                    if ([dictionary[@"lastTripTime"] length])
                    {
                        NSString * lastTripTime = flStrForStr([self changeDateFormateToTime:flStrForStr(dictionary[@"lastTripTime"])]);
                        lastTripTimeLbl.text = lastTripTime;
                    }
                    else
                    {
                        lastTripTimeLbl.text = @"NA";
                    }
                    typeLbl.text = flStrForStr(dictionary[@"carType"]);
                }
                ProgressIndicator *pi = [ProgressIndicator sharedInstance];
                [pi hideProgressIndicator];
                
                if (status == kNotificationTypeBookingOnTheWay || status == kNotificationTypeBookingArrived || status == kNotificationTypeBookingBeginTrip)
                {
                    //storing the current booking status in NSUserDefaults
                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isBooked"];
                    [[NSUserDefaults standardUserDefaults] setObject:dictionary[@"bid"] forKey:@"bid"];
                     [[NSUserDefaults standardUserDefaults] synchronize];
                    _bookingStatus = status;
                    [[NSUserDefaults standardUserDefaults] setObject:dictionary[@"pasChn"] forKey:kNSUPassengerChannelKey];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    dictpassDetail = [[NSMutableDictionary alloc] initWithDictionary:dictionary];
                    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
                    if ([ud objectForKey:@"passengerDroppedPressed"])
                    {
//                        [self performSegueWithIdentifier:@"homeToInvoice" sender:self];
                    }
                    else
                    {
                        if (!isAlreadyOpened)
                        {
                        [self performSegueWithIdentifier:@"GotoDriverDetailController" sender:self];
                        }
                        
                    }
                }
                else
                {
                    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isBooked"];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kNSUDriverDistanceTravalled];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"onTheWayPressed"];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"iHaveArrivedPressed"];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"beginTripPressed"];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"passengerDroppedPressed"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                }
            };
            [status checkOngoingAppointmentStatus:nil];
        }
        else
        {
            ProgressIndicator * pi = [ProgressIndicator sharedInstance];
            [pi showMessage:kNetworkErrormessage On:self.view];
        }
    }
    else
    {
        appDelegate.isNewBooking  = NO;
    }
    [appDelegate checkDriverStatus];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(60 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    });
    [self checkDriverOnlineOffline];
}

- (NSString *)changeDateFormateToTime:(NSString *)originalDate{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];//2012-11-22 10:19:04
    NSDate *date = [dateFormatter dateFromString:originalDate];
    
    [dateFormatter setDateFormat:@"hh:mm a"];//05:30 AM
    NSString *formattedTimeString = [dateFormatter stringFromDate:date];
    
    NSLog(@"Time in new format : %@", formattedTimeString);
    return formattedTimeString;
}

- (CGSize)makeSize:(CGSize)originalSize fitInSize:(CGSize)boxSize
{
    float widthScale = 0;
    float heightScale = 0;
    widthScale = boxSize.width/originalSize.width;
    heightScale = boxSize.height/originalSize.height;
    float scale = MIN(widthScale, heightScale);
    CGSize newSize = CGSizeMake(originalSize.width *scale, originalSize.height *scale);
    return newSize;
}

- (void) addShadowToView:(UIView *)view
{
    // border radius
    [view.layer setCornerRadius:10.0f];
    
    // border
    [view.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [view.layer setBorderWidth:1.0f];
    
    // drop shadow
    [view.layer setShadowColor:[UIColor blackColor].CGColor];
    [view.layer setShadowOpacity:0.5];
    [view.layer setShadowRadius:2.0];
    [view.layer setShadowOffset:CGSizeMake(1.0, 2.0)];
}
- (void) addShadowToViewOne:(UIView *)view
{
    // border radius
    //    [view.layer setCornerRadius:10.0f];
    
    // border
    [view.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [view.layer setBorderWidth:1.0f];
    
    // drop shadow
    [view.layer setShadowColor:[UIColor blackColor].CGColor];
    [view.layer setShadowOpacity:0.5];
    [view.layer setShadowRadius:2.0];
    [view.layer setShadowOffset:CGSizeMake(1.0, 2.0)];
}
-(UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}
-(void)getAllBookingsForToday{
    
    
    
    PMDReachabilityWrapper * reachability = [PMDReachabilityWrapper sharedInstance];
    if ([reachability isNetworkAvailable])
    {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi showPIOnView:self.view withMessage:NSLocalizedString(@"Please wait..", @"Please wait..")];
        
        //setup parameters
        NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
        NSString  *deviceID = [[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey];
        NSString *currentDate = [Helper getCurrentDateTime];
        
        
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        [params setObject:sessionToken forKey:@"ent_sess_token"];
        [params setObject:deviceID forKey:@"ent_dev_id"];
        [params setObject:currentDate forKey:@"ent_date_time"];
        
        
        NetworkHandler *networHandler = [NetworkHandler sharedInstance];
        [networHandler composeRequestWithMethod:MethodPendingAppointmentRequests
                                        paramas:params
                                   onComplition:^(BOOL success, NSDictionary *response){
                                       
                                       
                                       
                                       [pi hideProgressIndicator];
                                       
                                       if (success) { //handle success response
                                           NSLog(@"response");
                                           
                                           if ([response[@"errFlag"] integerValue] == 0) { //success
                                               _laterBookings = [[NSMutableArray alloc] initWithArray:response[@"appointments"]];
                                               
                                               [mapView_ clear];
                                               
                                               for (int i = 0;i < _laterBookings.count ; i++ )
                                               {
                                                   
                                               }
                                           }
                                           
                                       }
                                       else {
                                           
                                           
                                       }
                                   }];
    }
    else{
        ProgressIndicator * pi = [ProgressIndicator sharedInstance];
        [pi showMessage:kNetworkErrormessage On:self.view];
    }
}

- (UIImage *)imageFromView:(UIView *) view
{
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(view.frame.size, NO, [[UIScreen mainScreen] scale]);
    } else {
        UIGraphicsBeginImageContext(view.frame.size);
    }
    [view.layer renderInContext: UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}


-(void)gotoBookingInfo{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                @"Main" bundle:[NSBundle mainBundle]];
    
    BookingHistoryViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"history"];
    
    
    
    [self.navigationController pushViewController:viewcontrolelr animated:YES];
}

-(void)uploadFile:(UploadFiles *)uploadfile didUploadSuccessfullyWithUrl:(NSArray *)imageUrls{
}


- (void)viewWillAppear:(BOOL)animated
{
    CLLocationCoordinate2D coordinate = [self getLocation];
    self.currentLatitude = coordinate.latitude;
    self.currentLongitude = coordinate.longitude;
    _bookingMarker = [GMSMarker new];
    self.navigationController.navigationBarHidden = YES;
    XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
    [menu disablePanGesture:NO];
    
    [mapView_ addObserver:self
               forKeyPath:@"myLocation"
                  options:NSKeyValueObservingOptionNew
                  context:NULL];
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(checkDriverOnlineOffline)
                                                name:UIApplicationDidBecomeActiveNotification
                                              object:nil];
    
    isFirstBoookingCame = NO;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(driverStateChanges) name:@"DriverState" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkLocationServices) name:@"LocationServices" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(newBookingCame:) name:@"NewBooking" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(accountDeactivated) name:@"accountDeactivated" object:nil];
    [[NSUserDefaults standardUserDefaults] setObject:@"4" forKey:@"currentBookingStatus"];
}

/**
 *  Driver Online/Offline check from server
 */
-(void)checkDriverOnlineOffline
{
    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
    if ( [reachability isNetworkAvailable])
    {
        PriveMdAppDelegate *appDelegate = (PriveMdAppDelegate*)[UIApplication sharedApplication].delegate;
        if ([[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken]) {
            
            if ([[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken]) {
                NSDictionary *dict = @{@"ent_sess_token":[[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],
                                       @"ent_dev_id":[[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey]};
                NSLog(@"Driver online Params: %@", dict);
                NetworkHandler *handler = [NetworkHandler sharedInstance];
                [handler composeRequestWithMethod:@"getMasterStatus" paramas:dict onComplition:^(BOOL succeeded, NSDictionary *response) {
                    NSLog(@"Check driver online: %@", response);
                    [[ProgressIndicator sharedInstance] hideProgressIndicator];
                    if (succeeded) {
                        if ([response[@"errFlag"] integerValue] == 1) {
                            if ([response[@"errNum"] integerValue] == 7) {
                                [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Your session has expired", @"Your session has expired")];
                                [[NSUserDefaults standardUserDefaults] removeObjectForKey:kNSUDriverDistanceTravalledOnOff];
                                User *logout = [User sharedInstance];
                                logout.delegate = self;
                                [logout logout];
                                
                            }else if ([response[@"errNum"] integerValue] == 96 || [response[@"errNum"] integerValue] == 94) {
                                [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response[@"errMsg"]];
                                User *logout = [User sharedInstance];
                                logout.delegate = self;
                                [logout logout];
                            }else  {
                                [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response[@"errMsg"]];
                            }
                        }else {
                            if ([response[@"status"] integerValue] == 3) {
                                
                                static dispatch_once_t onceToken;
                                dispatch_once(&onceToken, ^{
                                    [appDelegate subscribeToPubnubChannel];
                                    [self startUpdatingLocationToServer];
                                });
                                [_locationManager startUpdatingLocation];
                                [self.locationUpdateTimer invalidate];
                                self.locationUpdateTimer = nil;
                                [self startUpdatingLocationToServer];
                                [self.switchOnOff setOn:YES];
                                _availablePauseButtonClickOutlet.selected = YES;
                                [self.availableLabel setText:NSLocalizedString(@"ON THE JOB", @"ON THE JOB")];
                                [self.onTheJobButtonOutlet setBackgroundImage:[UIImage imageNamed:@"on_the_job_active.png"] forState:UIControlStateNormal];
                                [self.offTheJobButtonOutlet setBackgroundImage:[UIImage imageNamed:@"off_the_job_inactive.png"] forState:UIControlStateNormal];
                                [self.onTheJobButtonOutlet setEnabled:NO];
                                [self.offTheJobButtonOutlet setEnabled:YES];
                                [self subscribeToPubnubPresenceChannel];
                                
                                NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
                                LocationTracker *tracker = [LocationTracker sharedInstance];
                                if ([ud objectForKey:kNSUDriverDistanceTravalledOnOff]) {
                                    tracker.distanceOnOff = [[ud objectForKey:kNSUDriverDistanceTravalledOnOff] floatValue];
                                }
                                
                                
                                tracker.distanceCallbackOnOff = ^(float totalDistance, CLLocation *location){
                                    [ud setObject:[NSNumber numberWithFloat:totalDistance] forKey:kNSUDriverDistanceTravalledOnOff];
                                    [ud synchronize];
                                };
                                
                            }
                            else
                            {
                                _availablePauseButtonClickOutlet.selected = NO;
                                [self.switchOnOff setOn:NO];
                                [self.availableLabel setText:NSLocalizedString(@"OFF THE JOB", @"OFF THE JOB")];
                                [self.onTheJobButtonOutlet setBackgroundImage:[UIImage imageNamed:@"on_the_job_inactive.png"] forState:UIControlStateNormal];
                                [self.offTheJobButtonOutlet setBackgroundImage:[UIImage imageNamed:@"roadyo_reject_btn_off.png"] forState:UIControlStateNormal];
                                [self.onTheJobButtonOutlet setEnabled:YES];
                                [self.offTheJobButtonOutlet setEnabled:NO];
                                [[NSUserDefaults standardUserDefaults] removeObjectForKey:kNSUDriverDistanceTravalledOnOff];
                                [_locationManager stopUpdatingLocation];
                                [_locationTracker stopLocationTracking];
                                [self.locationUpdateTimer invalidate];
                                self.locationUpdateTimer = nil;
                                
                            }
                        }
                    }
                }];
            }
        }
    }
    
}
-(void)startUpdatingLocationToServer
{
    self.locationTracker = [LocationTracker sharedInstance];
    
    [self.locationTracker startLocationTracking];
    
    //Send the best location to server every 60 seconds
    //You may adjust the time interval depends on the need of your app.
    NSTimeInterval time = 4.0;
    self.locationUpdateTimer =
    [NSTimer scheduledTimerWithTimeInterval:time
                                     target:self
                                   selector:@selector(updateLocation)
                                   userInfo:nil
                                    repeats:YES];
}

-(void)updateLocation
{
    [self.locationTracker updateLocationToServer];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [_allMarkers removeAllObjects];
    _isUpdatedLocation = NO;
    @try {
        [mapView_ removeObserver:self
                      forKeyPath:@"myLocation"
                         context:NULL];
    }
    @catch (NSException *exception) {
    }
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark NSNotification Mehods

-(void)newBookingCame:(NSNotification*)notification
{
    if (!isFirstBoookingCame)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"removePopUp" object:nil];
        NSLog(@"userinfo : %@",notification.userInfo);
        PMDReachabilityWrapper * reachbility = [PMDReachabilityWrapper sharedInstance];
        if ([reachbility isNetworkAvailable])
        {
            XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
            if (menu.isMenuOpened)
            {
                [menu closeMenuAnimated];
            }
            NSDictionary *bookingInfo = notification.userInfo;
            BOOL isFirstBooking = NO;
            
            if (![[NSUserDefaults standardUserDefaults] objectForKey:@"lastBID"])
            {
                [[NSUserDefaults standardUserDefaults] setObject:bookingInfo[@"bid"] forKey:@"lastBID"];
                isFirstBooking = YES;
            }
            if ([notification.userInfo[@"nt"] integerValue] == 51) {
                [[NSUserDefaults standardUserDefaults] setObject:bookingInfo[@"bid"] forKey:@"lastBID"];
                isFirstBooking = YES;
            }
            //app should not take action for new booking notificaiton if driver is alreay booked
            if (![[NSUserDefaults standardUserDefaults] boolForKey:@"isBooked"] && ([bookingInfo[@"bid"] integerValue] > [[[NSUserDefaults standardUserDefaults] objectForKey:@"lastBID"] integerValue] || isFirstBooking))
            {
                isFirstBooking = NO;
                [self updateToServerForNewBooking:[bookingInfo[@"bid"] integerValue]];
                [[NSUserDefaults standardUserDefaults] setObject:bookingInfo[@"bid"] forKey:@"lastBID"];
                if (!_isNewBookingInProcess)
                {
                    _isNewBookingInProcess = YES;
                    NSLog(@"Got Booking");
                    [[NSUserDefaults standardUserDefaults] setObject:bookingInfo forKey:@"PUSH"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    labelPickeup.text = @"Getting Location";
                    labelDropOff.text = @"Getting Location";
                    labelPickUpdistance.text = @"0";
                    CGRect frame = self.PickUpAndDropView.frame;
                    frame.size.height = 59;
                    [dropLocationbackgroundImage setHidden:YES];
                    self.PickUpAndDropView.frame = frame;
                    [labelDropOffTitle setHidden:YES];
                    [labelDropOff setHidden:YES];
                    [labelDropOffdistance setHidden:YES];
                    [labelDropOffMile setHidden:YES];
                    [self.dropIconImage setHidden:YES];
                    NSString *pewPewPath = [[NSBundle mainBundle]
                                            pathForResource:@"taxina" ofType:@"wav"];
                    NSURL *pewPewURL = [NSURL fileURLWithPath:pewPewPath];
                    if (!audioPlayer)
                    {
                        audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:pewPewURL error:nil];
                    }
                    audioPlayer.numberOfLoops = -1;
                    [audioPlayer prepareToPlay];
                    [audioPlayer play];
                    NSArray *latLong = [bookingInfo[@"ltg"] componentsSeparatedByString:@","];
                    float latitude = [latLong[0] floatValue];
                    float longitude = [latLong[1] floatValue];//[bookingInfo[@"pickLong"] floatValue];
                    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:latitude
                                                                            longitude:longitude
                                                                                 zoom:18];
                    [mapView_ setCamera:camera];
                    _bookingMarker.position = CLLocationCoordinate2DMake(latitude, longitude);
                    _bookingMarker.appearAnimation = kGMSMarkerAnimationPop;
                    _bookingMarker.title = NSLocalizedString(@"Pick From Here", @"Pick From Here");
                    _bookingMarker.map = mapView_;
                    [mapView_ setCamera:camera];
                    [mapView_ setSelectedMarker:_bookingMarker];
                    [_bookingMarker.map setHidden:NO];
                    labelPickeup.text = bookingInfo[@"adr2"];
                    _labelBookingId.text = [NSString stringWithFormat:@"BOOKING ID:%ld",[bookingInfo[@"bid"] integerValue]];
                    dict = bookingInfo;
                    [[NSUserDefaults standardUserDefaults] setObject:bookingInfo[@"chn"] forKey:kNSUPassengerChannelKey];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    _expireTime = 30;//- timeInterval;
                    _isNewBookingInProcess = YES;
                    acceptRejectView* acceptView = [[acceptRejectView alloc]init];
                    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
                    [acceptView showPopUpWithDetailedDict:bookingInfo Onwindow:window];
                    acceptView.delegate = self;
                    isFirstBoookingCame = YES;
                }
            }
        }
        else
        {
            ProgressIndicator * pi = [ProgressIndicator sharedInstance];
            [pi showMessage:kNetworkErrormessage On:self.view];
        }
    }
}
/**
 *  Check if driver is on the job/off the job
 */

-(void)driverStateChanges
{
    
    if ([[NSUserDefaults standardUserDefaults] integerForKey:kNSUDriverStatekey]) {
        _driverStatus = (DriverStatus)[[NSUserDefaults standardUserDefaults] integerForKey:kNSUDriverStatekey];
        if (_driverStatus == kDriverStatusOffline) {
            [self.switchOnOff setOn:NO];
            [self.availableLabel setText:NSLocalizedString(@"OFF THE JOB", @"OFF THE JOB")];
            [self.onTheJobButtonOutlet setBackgroundImage:[UIImage imageNamed:@"on_the_job_inactive.png"] forState:UIControlStateNormal];
            [self.offTheJobButtonOutlet setBackgroundImage:[UIImage imageNamed:@"roadyo_reject_btn_off.png"] forState:UIControlStateNormal];
            [self.onTheJobButtonOutlet setEnabled:YES];
            [self.offTheJobButtonOutlet setEnabled:NO];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:kNSUDriverDistanceTravalledOnOff];
        }else if (_driverStatus == KDriverStatusOnline){
            [self.switchOnOff setOn:YES];
            [self.availableLabel setText:NSLocalizedString(@"ON THE JOB", @"ON THE JOB")];
            [self.onTheJobButtonOutlet setBackgroundImage:[UIImage imageNamed:@"on_the_job_active.png"] forState:UIControlStateNormal];
            [self.offTheJobButtonOutlet setBackgroundImage:[UIImage imageNamed:@"off_the_job_inactive.png"] forState:UIControlStateNormal];
            [self.onTheJobButtonOutlet setEnabled:NO];
            [self.offTheJobButtonOutlet setEnabled:YES];
        }
    }
    else {
        _driverStatus = KDriverStatusOnline;
    }
    if (_driverStatus == KDriverStatusOnline)
    {
        // [_customNavigationBarView.rightbarButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        //[_customNavigationBarView setRightBarButtonTitle:@"GO OFFLINE"];
        
    }
    else {
        // [_customNavigationBarView setRightBarButtonTitle:@"GO ONLINE"];
    }
}


#pragma mark BookingInfoView Animation

/**
 *  Hide Accept/Reject Views
 */

-(void)hideBookingInfoViews{
    
    audioPlayer.numberOfLoops = 1;
    [audioPlayer stop];
    _isNewBookingInProcess = NO;
    _bookingMarker.map = nil;
    [mapView_ clear];
    markerImage.map = mapView_;
    mapView_.camera = [GMSCameraPosition cameraWithLatitude:_previouCoord.latitude longitude:_previouCoord.longitude zoom:18];
    markerImage.groundAnchor = CGPointMake(0.5, 0.5);
    [self startAnimationDownForBottom];
    [self startAnimationDownForTop];
    [_progressView removeFromSuperview];
    if (timer) {
        [timer invalidate];
    }
    XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
    [menu disablePanGesture:NO];
    CustomNavigationBar *navigationBar = (CustomNavigationBar *)[self.view viewWithTag:78];
    [navigationBar rightBarButtonEnabled:YES];
    [navigationBar leftBarButtonEnabled:YES];
}

/**
 *  Show Accept/Reject Views
 */

-(void)showBookingInfoView
{
    _isNewBookingInProcess = YES;
    [self startCoundownTimer];
    [self startAnimationUpForBottom];
    [self startAnimationUpForTop];
    XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
    [menu disablePanGesture:YES];
    CustomNavigationBar *navigationBar = (CustomNavigationBar *)[self.view viewWithTag:78];
    [navigationBar rightBarButtonEnabled:NO];
    [navigationBar leftBarButtonEnabled:NO];
}
-(void)startAnimationDownForBottom
{
    CGRect frame = acceptRejectBtnView.frame;
    frame.origin.y = [UIScreen mainScreen].bounds.size.height;
    frame.origin.x = 0;
    
    [UIView animateWithDuration:0.2
                          delay:0.1
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^
     {
         
         
         acceptRejectBtnView.frame = frame;
     }
                     completion:^(BOOL finished)
     {
         [acceptRejectBtnView setHidden:YES];
         
     }];
}

-(void)startAnimationDownForTop
{
    CGRect frame = PickUpAndDropView.frame;
    frame.origin.y = -99;
    frame.origin.x = 0;
    [UIView animateWithDuration:0.2
                          delay:0.1
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^(){
                         
                         PickUpAndDropView.frame = frame;
                     }
                     completion:^(BOOL finished){
                         [PickUpAndDropView setHidden:YES];
                         
                     }];
}


-(void)startAnimationUpForBottom
{
    
    
    [acceptRejectBtnView setHidden:NO];
    
    
    CGRect frame = acceptRejectBtnView.frame;
    frame.origin.y = self.view.frame.size.height - 104;
    frame.origin.x = 0;
    
    [UIView animateWithDuration:0.2
                          delay:0.0
                        options: UIViewAnimationOptionCurveEaseOut
                     animations:^(){
                         
                         
                         acceptRejectBtnView.frame = frame;
                     }
                     completion:^(BOOL finished){
                         
                     }];
    
    
}

-(void)startAnimationUpForTop
{
    [PickUpAndDropView setHidden:NO];
    
    //PickUpAndDropView.frame = CGRectMake(0, -64-91, 320, 91);
    //   PickUpAndDropView.frame = CGRectMake(0, 0, 320, 89);
    
    [UIView animateWithDuration:0.1
                          delay:0.0
                        options: UIViewAnimationOptionCurveEaseOut
                     animations:^
     {
         CGRect frame = PickUpAndDropView.frame;
         frame.origin.y = 64;
         frame.origin.x = 0;
         PickUpAndDropView.frame = frame;
     }
                     completion:^(BOOL finished)
     {
         
     }];
    
    
    
    
}






-(void)addgestureToMap
{
    _tapInterceptor = [[WildcardGestureRecognizer alloc] init];
    //  CLLocation *location = mapView_.myLocation;
    
    __weak typeof(self) weakSelf = self;
    _tapInterceptor.touchesBeganCallback = ^(NSSet * touches, UIEvent * event , int touchtype)
    {
        if (touchtype == 1)
        {
            //[weakSelf startAnimation];
        }
        else {
            // [weakSelf endAnimation];
        }
        
    };
    [mapView_  addGestureRecognizer:_tapInterceptor];
    
    
}


#pragma Circula Progress View


/**
 *  Starting count down timer
 */

-(void)startCoundownTimer{
    
    if (![Helper isIphone5]) {
        if (!_progressView) {
            _progressView = [[PICircularProgressView alloc]initWithFrame:CGRectMake(320/2-50,250, 100, 100)];
        }
        
    }else {
        if (!_progressView) {
            _progressView = [[PICircularProgressView alloc]initWithFrame:CGRectMake(320/2-50,370, 100, 100)];
        }
    }
    [self.view addSubview:_progressView];
    self.progressView.thicknessRatio=0.5;
    self.progressView.progress = 1;
    self.progressView.showText =YES;
    self.progressView.roundedHead = YES;
    
    //    [self.progressView setProgressText:[NSString stringWithFormat:@"%@",_bookingResponse[@"expireSec"]]];
    [self.progressView setProgressText:[NSString stringWithFormat:@"%.0f",_expireTime]];
    //    _expireTime = [[NSString stringWithFormat:@"%@",_bookingResponse[@"expireSec"]] floatValue];
    
    if (![timer isValid]) {
        timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerTick) userInfo:nil repeats:YES];
    }
}


- (void)timerTick
{
    float Newvalue1 = _expireTime/30 - (0.033333333);
    float Newvalue = _expireTime - 1;
    [_progressView setProgressText:[NSString stringWithFormat:@"%.0f", Newvalue]];
    _progressView.progress = Newvalue1;
    _expireTime = Newvalue;
    if (Newvalue <= 0.0) {
        [timer invalidate];
        [_progressView removeFromSuperview];
        [self hideBookingInfoViews];
    }
}

-(void)stopAudioPlayer{
    [audioPlayer stop];
}
-(void)didTimerExpire
{
    [audioPlayer stop];
    _isNewBookingInProcess = NO;
    isFirstBoookingCame = NO;
}

#pragma mark- GMSMapviewDelegate


- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate
{
    
}
- (void) mapView:(GMSMapView *) mapView willMove:(BOOL)gesture{
    
}

- (void) mapView:(GMSMapView *) mapView didChangeCameraPosition:(GMSCameraPosition *) position{
    
}

- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position{
}
- (void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker{
    
    // [self performSegueWithIdentifier:@"LaterBookingSegue" sender:marker];
}

- (UIView *)mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker {
    
    
    
    return nil;
}
- (void)mapView:(GMSMapView *)mapView didBeginDraggingMarker:(GMSMarker *)marker{
}
- (void)mapView:(GMSMapView *)mapView didEndDraggingMarker:(GMSMarker *)marker{
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
    if (!self.isUpdatedLocation) {
        CLLocation *location = [change objectForKey:NSKeyValueChangeNewKey];
        CLLocationCoordinate2D locationLatLong = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude);
        markerImage.position = locationLatLong;
        markerImage.map = mapView_;
        CLLocationDirection heading = ((GMSGeometryHeading(locationLatLong, _previouCoord) + 180));
        
        [CATransaction begin];
        [CATransaction setAnimationDuration:1.0];
        markerImage.position = locationLatLong;
        
        if (markerImage.flat) {
        }
        [CATransaction commit];
        _previouCoord = locationLatLong;
        self.isUpdatedLocation = YES;
    }
    CLLocation *location = [change objectForKey:NSKeyValueChangeNewKey];
    CLLocationCoordinate2D locationLatLong = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude);
    
    markerImage.map = mapView_;
    markerImage.icon = _carImage;
    GMSCameraUpdate *update = [GMSCameraUpdate setTarget:location.coordinate zoom:18];
    [mapView_ animateWithCameraUpdate:update];
    
    [CATransaction begin];
    [CATransaction setAnimationDuration:1.0];
    markerImage.position = locationLatLong;
    markerImage.groundAnchor = CGPointMake(0.5f, 0.5f);
    [CATransaction commit];
    _previouCoord = locationLatLong;
    
}


#pragma mark - CLLocation manager deleagte method

-(void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading {
    
    float heading = 0;
    if(newHeading.headingAccuracy>0)
    {
        if (newHeading.trueHeading > 0) {
            heading = newHeading.magneticHeading;
        }else {
            //in degrees
            heading = newHeading.magneticHeading;
        }
        [mapView_ animateToBearing:heading];
    }
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    [locations lastObject];
    self.isUpdatedLocation = NO;
}



- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error{
}


- (void)addDirections:(NSDictionary *)json {
    
    if ([json[@"routes"] count]>0) {
        NSDictionary *routes = [json objectForKey:@"routes"][0];
        
        NSDictionary *route = [routes objectForKey:@"overview_polyline"];
        NSString *overview_route = [route objectForKey:@"points"];
        GMSPath *path = [GMSPath pathFromEncodedPath:overview_route];
        GMSPolyline *polyline = [GMSPolyline polylineWithPath:path];
        polyline.map = mapView_;
    }
    
    
}

-(void)updateDestinationLocationWithLatitude:(float)latitude Longitude:(float)longitude{
    
    if (!_isPathPlotted) {
        
        _isPathPlotted = YES;
        
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(
                                                                     latitude,
                                                                     longitude);
        
        _previouCoord = position;
        
        _destinationMarker = [GMSMarker markerWithPosition:position];
        _destinationMarker.map = mapView_;
        _destinationMarker.flat = YES;
        _destinationMarker.groundAnchor = CGPointMake(0.5f, 0.5f);
        //        _destinationMarker.icon = [UIImage imageNamed:@"arrive_caricon.png"];
        _destinationMarker.icon = [UIImage imageNamed:@"car"];
        [waypoints_ addObject:_destinationMarker];
        NSString *positionString = [[NSString alloc] initWithFormat:@"%f,%f",
                                    latitude,longitude];
        
        [waypointStrings_ addObject:positionString];
        
        if([waypoints_ count]>1){
            NSString *sensor = @"false";
            NSArray *parameters = [NSArray arrayWithObjects:sensor, waypointStrings_,
                                   nil];
            NSArray *keys = [NSArray arrayWithObjects:@"sensor", @"waypoints", nil];
            NSDictionary *query = [NSDictionary dictionaryWithObjects:parameters
                                                              forKeys:keys];
            DirectionService *mds=[[DirectionService alloc] init];
            SEL selector = @selector(addDirections:);
            [mds setDirectionsQuery:query
                       withSelector:selector
                       withDelegate:self];
        }
    }
    else {
        
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(latitude,longitude);
        CLLocationDirection heading = GMSGeometryHeading(_previouCoord, position);
        _previouCoord = position;
        
        [CATransaction begin];
        [CATransaction setAnimationDuration:2.0];
        _destinationMarker.position = position;
        [CATransaction commit];
        if (_destinationMarker.flat) {
            //            _destinationMarker.rotation = heading;
        }
        
    }
}


/**
 *  Accept/Reject New booking Actions
 */


#pragma mark -button accept and reject action;
-(IBAction)driverAvaiablePauseClickAction:(UIButton *)sender
{
    PMDReachabilityWrapper * reachbility = [PMDReachabilityWrapper sharedInstance];
    if ([reachbility isNetworkAvailable])
    {
    if ([sender isSelected])
    {
        sender.selected = NO;
        [self sendRequestToChagneDriverStatus:kDriverStatusOffline];
    }else {
        sender.selected = YES;
        [self sendRequestToChagneDriverStatus:KDriverStatusOnline];
    }
    }
    else{
        ProgressIndicator * pi = [ProgressIndicator sharedInstance];
        [pi showMessage:kNetworkErrormessage On:self.view];
    }
}

-(void) didButtonSelectWithTag:(int)tag
{
    [self stopAudioPlayer];
    _isNewBookingInProcess = NO;
    [self respondToNewBookingFor:tag];
}


-(IBAction)btnAcceptRejectClicked:(id)sender
{
    UIButton * button = (UIButton*)sender;
    
    switch (button.tag) {
        case 10://accept
        {
            ProgressIndicator *pi = [ProgressIndicator sharedInstance];
            [pi showPIOnView:self.view withMessage:NSLocalizedString(@"Please wait..", @"Please wait..")];
            [self respondToNewBookingFor:2];
            break;
        }
        case 11://reject
        {
            ProgressIndicator *pi = [ProgressIndicator sharedInstance];
            [pi showPIOnView:self.view withMessage:NSLocalizedString(@"Please wait..", @"Please wait..")];
            //            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"bid"];
            [self respondToNewBookingFor:3];
            break;
        }
        default:
            break;
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    
    
    if (alertView.tag==200)
    {
        
        if (buttonIndex==0) {
            
            ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
            [progressIndicator showPIOnView:self.view withMessage:NSLocalizedString(@"Please wait..", @"Please wait..")];
            
            NetworkHandler * handler = [NetworkHandler sharedInstance];
            
            NSDictionary * dictP = [[NSUserDefaults standardUserDefaults] objectForKey:@"PUSH"];
            //NSDictionary * dictP = [dict objectForKey:@"aps"];
            
            NSDictionary *queryParams;
            
            queryParams = [NSDictionary dictionaryWithObjectsAndKeys:
                           [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],KDAcheckUserSessionToken,
                           [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],kSMPCommonDevideId,
                           [dictP objectForKey:@"e"],kSMPRespondPassengerEmail,
                           [dictP objectForKey:@"dt"], kSMPRespondBookingDateTime,
                           constkNotificationTypeBookingReject,kSMPRespondResponse,
                           constkNotificationTypeBookingType,kSMPRespondBookingType,
                           [Helper getCurrentDateTime],kSMPCommonUpDateTime, nil];
            
            
            NSLog(@"respondToAppointment   :%@",queryParams);
            [handler composeRequestWithMethod:MethodRespondToAppointMent
                                      paramas:queryParams
                                 onComplition:^(BOOL succeeded, NSDictionary *response) {
                                     
                                     //            [restKit composeRequestForAccept:MethodRespondToAppointMent
                                     //                                     paramas:queryParams
                                     //                                onComplition:^(BOOL success, NSDictionary *response){
                                     //
                                     if (succeeded) { //handle success response
                                         [self RejectResponse:(NSArray*)response];
                                     }
                                     else{//error
                                         ProgressIndicator *pi = [ProgressIndicator sharedInstance];
                                         [pi hideProgressIndicator];
                                         [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error") Message:NSLocalizedString(@"Network Error", @"Network Error")];
                                     }
                                 }];
        }
    }
}

-(void)respondToNewBookingFor:(int)bookingAction{
    
    PMDReachabilityWrapper * reachbility = [PMDReachabilityWrapper sharedInstance];
    if ([reachbility isNetworkAvailable]) {
        [[ProgressIndicator sharedInstance] showPIOnView:self.view withMessage:@"Loading.."];
        
        NSDictionary * dictP = [[NSUserDefaults standardUserDefaults] objectForKey:@"PUSH"];
        //NSDictionary * dictP = [dict objectForKey:@"aps"];
        //        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"bid"];
        //        [[NSUserDefaults standardUserDefaults] synchronize]
        NSDictionary *queryParams;
        if (_isLaterBooking) {
            queryParams = [NSDictionary dictionaryWithObjectsAndKeys:
                           [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],KDAcheckUserSessionToken,
                           [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],kSMPCommonDevideId,
                           [dictP objectForKey:@"e"],kSMPRespondPassengerEmail,
                           [dictP objectForKey:@"dt"], kSMPRespondBookingDateTime,
                           [NSString stringWithFormat:@"%d",bookingAction],kSMPRespondResponse,
                           constkNotificationTypeBookingTypeLater,kSMPRespondBookingType,
                           [Helper getCurrentDateTime],kSMPCommonUpDateTime, nil];
        }
        else {
            queryParams = [NSDictionary dictionaryWithObjectsAndKeys:
                           [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],KDAcheckUserSessionToken,
                           [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],kSMPCommonDevideId,
                           [dictP objectForKey:@"e"],kSMPRespondPassengerEmail,
                           [dictP objectForKey:@"dt"], kSMPRespondBookingDateTime,
                           [NSString stringWithFormat:@"%d", bookingAction],kSMPRespondResponse,
                           constkNotificationTypeBookingType,kSMPRespondBookingType,
                           [Helper getCurrentDateTime],kSMPCommonUpDateTime, nil];
        }
        
        NetworkHandler *networHandler = [NetworkHandler sharedInstance];
        [networHandler composeRequestWithMethod:MethodRespondToAppointMent
                                        paramas:queryParams
                                   onComplition:^(BOOL success, NSDictionary *response)
         {
             if (success) { //handle success response
                 
                 if ([response[@"errFlag"] integerValue] == 0)
                 {
                     
                     if (_isLaterBooking)
                     {
                         /*  [[ProgressIndicator sharedInstance] hideProgressIndicator];
                          [self hideBookingInfoViews];
                          if (bookingAction == 3) {
                          [self RejectResponse:(NSArray*)response];
                          }else {
                          [[ProgressIndicator sharedInstance] hideProgressIndicator];
                          [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Your Booking is Confirmed", @"Your Booking is Confirmed")];
                          [self hideBookingInfoViews];
                          [self performSegueWithIdentifier:@"LaterBookingSegue" sender:self];
                          }
                          
                          
                          */
                         //                                           return ;
                     }
                     else if (bookingAction ==2)
                     {
                         [[NSUserDefaults standardUserDefaults] setObject:dict[@"bid"] forKey:@"bid"];
                         [self sendRequestForUpdateStatus];
                     }
                     else if (bookingAction == 3)
                     {
                         [self RejectResponse:(NSArray*)response];
                         isFirstBoookingCame = NO;
                     }
                     
                 }
                 else if ([response[@"errFlag"] integerValue] == 1) {
                     [[ProgressIndicator sharedInstance] hideProgressIndicator];
                     if([response[@"errNum"] isEqualToString:@"3"])
                     {
                         [Helper showAlertWithTitle:@"Error" Message:@"Booking Expired"];
                     }
                     else
                         
                         [Helper showAlertWithTitle:@"Error" Message:response[@"errMsg"]];
                     [_allMarkers removeAllObjects];
                     [self hideBookingInfoViews];
                     
                 }
             }
             else{//error
                 ProgressIndicator *pi = [ProgressIndicator sharedInstance];
                 [pi hideProgressIndicator];
             }
         }];
    }
    else
    {
        ProgressIndicator * pi = [ProgressIndicator sharedInstance];
        [pi showMessage:kNetworkErrormessage On:self.view];
    }
}


#pragma mark -AcceptReject Response
-(void)RejectResponse :(NSArray*)array
{
    ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
    [progressIndicator hideProgressIndicator];
    if (!array)
    {
        return;
    }
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"bid"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    // _bookingMarker = nil;
    [_allMarkers removeAllObjects];
    //_bookingMarker.map = nil;
    [self hideBookingInfoViews];
}
-(void)acceptResponse :(NSArray*)array
{
    //[self performSegueWithIdentifier:@"GotoDriverDetailController" sender:self];
    
    Errorhandler * handler = [array objectAtIndex:0];
    [handler description];
    
    if ([[handler errFlag] intValue] == 0) {
        
        [self sendRequestForUpdateStatus];
    }
    
    else
    {
        ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
        [progressIndicator hideProgressIndicator];
        
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:handler.errMsg];
        [self hideBookingInfoViews];
    }
    
}

#pragma mark - Web Servies Request

-(void)sendRequestForUpdateStatus
{
    PMDReachabilityWrapper * reachbility = [PMDReachabilityWrapper sharedInstance];
    if ([reachbility isNetworkAvailable]) {
        
        [[ProgressIndicator sharedInstance] showPIOnView:self.view withMessage:@"Loading.."];
        NSDictionary * dictP = [[NSUserDefaults standardUserDefaults] objectForKey:@"PUSH"];
        //NSDictionary * dictP = [dict objectForKey:@"aps"];
        NetworkHandler * handler = [NetworkHandler sharedInstance];
        NSDictionary *queryParams;
        queryParams = [NSDictionary dictionaryWithObjectsAndKeys:
                       [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],KDAcheckUserSessionToken,
                       [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],kSMPCommonDevideId,
                       dictP[@"e"],kSMPRespondPassengerEmail,
                       dictP[@"dt"], kSMPRespondBookingDateTime,
                       constkNotificationTypeBookingOnTheWay,kSMPRespondResponse,
                       @"testing",kSMPRespondDocNotes,
                       [Helper getCurrentDateTime],kSMPCommonUpDateTime, nil];
        
        NSLog(@"MethodupdateApptStatus..............%@",queryParams);
        [handler composeRequestWithMethod:MethodupdateApptStatus
                                  paramas:queryParams
                             onComplition:^(BOOL succeeded, NSDictionary *response) {
                                 
                                 if (succeeded) {
                                     NSLog(@"MethodupdateApptStatus.....................%@",response);
                                     //handle success response
                                     [self updateStstusResponse:response];
                                 }
                                 else{//error
                                     ProgressIndicator *pi = [ProgressIndicator sharedInstance];
                                     [pi hideProgressIndicator];
                                 }
                             }];
        
    }
    else{
        ProgressIndicator * pi = [ProgressIndicator sharedInstance];
        [pi showMessage:kNetworkErrormessage On:self.view];
    }
    
}

-(void)sendRequestToChagneDriverStatus:(DriverStatus)driverStatus{
    
    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
    if ( [reachability isNetworkAvailable]) {
        
        VNHPubNubWrapper * pubnub = [VNHPubNubWrapper sharedInstance];
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        if (driverStatus == KDriverStatusOnline) {
            [pi showPIOnView:self.view withMessage:NSLocalizedString(@"Going online...", @"Going online...")];
        }
        else{
            [pi showPIOnView:self.view withMessage:NSLocalizedString(@"Going offline...", @"Going offline...")];
        }
        
        
        NSString *sesstionToken = [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken];
        float distance;
        NSString *deviceId = [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey];
        if ([[NSUserDefaults standardUserDefaults] objectForKey:kNSUDriverDistanceTravalledOnOff]) {
            if (driverStatus == kDriverStatusOffline) {
                LocationTracker *tracker = [LocationTracker sharedInstance];
                distance = tracker.distance;
                if (distance < 0) {
                    distance = 0;
                }
            }
        }else {
            distance = 0.0f;
        }
        NSDictionary *queryParams;
        if (driverStatus == KDriverStatusOnline) {
            queryParams = @{KDAcheckUserSessionToken: sesstionToken,
                            kSMPCommonDevideId:deviceId,
                            @"ent_status":[NSNumber numberWithInt:driverStatus],
                            kSMPCommonUpDateTime:[Helper getCurrentDateTime]
                            };
        }
        else
        {
            queryParams = @{KDAcheckUserSessionToken: sesstionToken,
                            kSMPCommonDevideId:deviceId,
                            @"ent_status":[NSNumber numberWithInt:driverStatus],
                            kSMPCommonUpDateTime:[Helper getCurrentDateTime],
                            @"ent_distance":[NSString stringWithFormat:@"%.2f",distance]
                            };
        }
        NSLog(@"Params ON/OFF: %@", queryParams);
        NetworkHandler *networHandler = [NetworkHandler sharedInstance];
        [networHandler composeRequestWithMethod:MethodUpdateMasterStatus
                                        paramas:queryParams
                                   onComplition:^(BOOL success, NSDictionary *response){
                                       
                                       if (success) { //handle success response
                                           [pi hideProgressIndicator];
                                           NSLog(@"RESPONSE DRIVER STATUS: %@", response);
                                           UILabel *label;
                                           if ([[response objectForKey:@"errNum"] intValue] == 79 || [response[@"errNum"] integerValue] == 94)
                                           {
                                               [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response[@"errMsg"]];
                                               User *user = [User sharedInstance];
                                               user.delegate = self;
                                               [user logout];
                                           }
                                           else {
                                               [label setHidden:YES];
                                               [self updateDriverStstusResponse:response];
                                               //chagne status
                                               
                                                if (_driverStatus == kDriverStatusOffline)// KDriverStatusOnline
                                                   {
                                                       //                                                   _driverStatus = kDriverStatusOffline;
                                                       _driverStatus = KDriverStatusOnline;
                                                       [[NSUserDefaults standardUserDefaults] removeObjectForKey:kNSUDriverDistanceTravalledOnOff];
                                                 
                                                       [pubnub unSubscribeToMyPresenceChannel];
                                                       [_locationManager stopUpdatingLocation];
                                                       [_locationTracker stopLocationTracking];
                                                       [self.locationUpdateTimer invalidate];
                                                       self.locationUpdateTimer = nil;
                                                   }
                                                   else
                                                   {
                                                       [_locationManager startUpdatingLocation];
//                                                       [self.locationUpdateTimer invalidate];
//                                                       self.locationUpdateTimer = nil;
                                                       [self startUpdatingLocationToServer];
                                                       //                                                   _driverStatus = KDriverStatusOnline;
                                                       _driverStatus = kDriverStatusOffline;
                                                       [self subscribeToPubnubPresenceChannel];
                                                       NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
                                                       LocationTracker *tracker = [LocationTracker sharedInstance];
                                                       if ([ud objectForKey:kNSUDriverDistanceTravalledOnOff]) {
                                                           tracker.distanceOnOff = [[ud objectForKey:kNSUDriverDistanceTravalledOnOff] floatValue];
                                                       }
                                                       tracker.distanceCallbackOnOff = ^(float totalDistance, CLLocation *location)
                                                       {
                                                           [ud setObject:[NSNumber numberWithFloat:totalDistance] forKey:kNSUDriverDistanceTravalledOnOff];
                                                           [ud synchronize];
                                                       };
                                                   }
                                            [[NSUserDefaults standardUserDefaults] setInteger:_driverStatus forKey:kNSUDriverStatekey];
                                               [[NSUserDefaults standardUserDefaults] synchronize];
                                           }
                                       }
                                       else
                                       {
                                           if (_driverStatus == kDriverStatusOffline)
                                           {
                                               _availablePauseButtonClickOutlet.selected = NO;
                                               [self.switchOnOff setOn:NO];
                                               [self.availableLabel setText:NSLocalizedString(@"OFF THE JOB", @"OFF THE JOB")];
                                               [self.onTheJobButtonOutlet setBackgroundImage:[UIImage imageNamed:@"on_the_job_inactive.png"] forState:UIControlStateNormal];
                                               [self.offTheJobButtonOutlet setBackgroundImage:[UIImage imageNamed:@"roadyo_reject_btn_off.png"] forState:UIControlStateNormal];
                                               [self.onTheJobButtonOutlet setEnabled:YES];
                                               [self.offTheJobButtonOutlet setEnabled:NO];
                                               
                                           }else if (_driverStatus == KDriverStatusOnline) {
                                               _availablePauseButtonClickOutlet.selected = YES;
                                               [self.switchOnOff setOn:YES];
                                               [self.availableLabel setText:NSLocalizedString(@"ON THE JOB", @"ON THE JOB")];
                                               [self.onTheJobButtonOutlet setBackgroundImage:[UIImage imageNamed:@"on_the_job_active.png"] forState:UIControlStateNormal];
                                               [self.offTheJobButtonOutlet setBackgroundImage:[UIImage imageNamed:@"off_the_job_inactive.png"] forState:UIControlStateNormal];
                                               [self.onTheJobButtonOutlet setEnabled:NO];
                                               [self.offTheJobButtonOutlet setEnabled:YES];
                                           }
                                           [[ProgressIndicator sharedInstance] hideProgressIndicator];
                                       }
                                   }];
    }
    else {
        if (_driverStatus == kDriverStatusOffline) {
            [self.switchOnOff setOn:NO];
            [self.availableLabel setText:NSLocalizedString(@"OFF THE JOB", @"OFF THE JOB")];
            [self.onTheJobButtonOutlet setBackgroundImage:[UIImage imageNamed:@"on_the_job_inactive.png"] forState:UIControlStateNormal];
            [self.offTheJobButtonOutlet setBackgroundImage:[UIImage imageNamed:@"roadyo_reject_btn_off.png"] forState:UIControlStateNormal];
            [self.onTheJobButtonOutlet setEnabled:YES];
            [self.offTheJobButtonOutlet setEnabled:NO];
            
        }else if (_driverStatus == KDriverStatusOnline) {
            [self.switchOnOff setOn:YES];
            [self.availableLabel setText:NSLocalizedString(@"ON THE JOB", @"ON THE JOB")];
            [self.onTheJobButtonOutlet setBackgroundImage:[UIImage imageNamed:@"on_the_job_active.png"] forState:UIControlStateNormal];
            [self.offTheJobButtonOutlet setBackgroundImage:[UIImage imageNamed:@"off_the_job_inactive.png"] forState:UIControlStateNormal];
            [self.onTheJobButtonOutlet setEnabled:NO];
            [self.offTheJobButtonOutlet setEnabled:YES];
            
        }
        
        [[ProgressIndicator sharedInstance] showMessage:kNetworkErrormessage On:self.view];
    }
}
-(void)updateStstusResponse :response
{
    
    //    Errorhandler * handler = [response objectAtIndex:0];
    if ([response[@"errFlag"] intValue] == kResponseFlagSuccess) {
        [self hideBookingInfoViews];
        //    }
        //
        ////    dispatch_async(dispatch_get_main_queue(), ^{
        //
        ////    });
        //
        //
        //
        //    if ([[handler errFlag] intValue] == kResponseFlagSuccess)
        //    {
        //start updating for on the way status
        
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isBooked"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self updateToPassengerForBookingConfirmation];
        [[ProgressIndicator sharedInstance] hideProgressIndicator];
        _bookingStatus = kNotificationTypeBookingOnTheWay;
        [self getAppointmentDetails];
    }
    else{
        // [self performSegueWithIdentifier:@"GotoDriverDetailController" sender:self];
        [[ProgressIndicator sharedInstance] hideProgressIndicator];
        [self checkDriverOnlineOffline];
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response[@"errMsg"]];
        //[self performSegueWithIdentifier:@"GotoDriverDetailController" sender:self];
    }
    
}
-(void)updateDriverStstusResponse:(NSDictionary*)response{
    if (response == nil) {
        return;
    }
    else {
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [_locationTracker stopLocationTracking];
    [self.locationUpdateTimer invalidate];
    self.locationUpdateTimer = nil;
    if ([[segue identifier] isEqualToString:@"GotoDriverDetailController"])
    {
        OnBookingViewController *booking = (OnBookingViewController*)[segue destinationViewController];
        booking.bookingStatus = _bookingStatus;
        if ([[dictpassDetail allKeys] containsObject:@"data"])
        {
            dictpassDetail = [dictpassDetail[@"data"][0] mutableCopy];
        }
        booking.passDetail = dictpassDetail;
        isAlreadyOpened = YES;

    }
    else if ([[segue identifier] isEqualToString:@"homeToInvoice"])
    {
//        NewBookingHistoryViewController *controller = (NewBookingHistoryViewController*)[segue destinationViewController];
//        controller.passDetails = dictpassDetail;
    }
}
-(void)updateToPassengerForBookingConfirmation{
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isBooked"]) {
        
        UpdateBookingStatus *updateBookingStatus = [UpdateBookingStatus sharedInstance];
        updateBookingStatus.driverState = kPubNubStartDoctorLocationStreamAction;
        [updateBookingStatus updateToPassengerForDriverState];
        [updateBookingStatus startUpdatingStatus];
    }
    
}

/**
 *  Update server that new booking came to the driver
 *
 *  @param bookingID current booking ID
 */
-(void)updateToServerForNewBooking:(NSInteger)bookingID {
    
    UpdateBookingStatus *updateBookingStatus = [UpdateBookingStatus sharedInstance];
    
    [updateBookingStatus updateToServerForBookingID:bookingID];
}
#pragma mark- Custom Methods


- (void) addCustomNavigationBar{
    
    _customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, 320, 64)];
    _customNavigationBarView.tag = 78;
    _customNavigationBarView.delegate = self;
    [_customNavigationBarView addButtonCenter];
    [_customNavigationBarView setTitle:@""];
    UIImageView *imageView= [[UIImageView alloc] initWithFrame:CGRectMake(_customNavigationBarView.frame.size.width/2 - 50, 29.5, 100, 25)];
    imageView.image = [UIImage imageNamed:@"RoadYO.png"];
    [_customNavigationBarView addSubview:imageView];
    [_customNavigationBarView setRightBarButtonTitle:NSLocalizedString(@"SCHEDULE", @"SCHEDULE")];
    [self.view addSubview:_customNavigationBarView];
    
    
    //--------------****-----------***********-------------*******--------------*****--------------------//
    
    
}

#pragma mark -leftbar and righbar action

-(void)rightBarButtonClicked:(UIButton *)sender {
    //    [self performSegueWithIdentifier:@"LaterBookingSegue" sender:self];
}


-(void)leftBarButtonClicked:(UIButton *)sender{
    
    if (_isNewBookingInProcess) {
        
    }
    else
    {
        [self menuButtonPressed:sender];
    }
}

-(void)centerButtonClicked: (UIButton*)sender {
    
    // [self performSegueWithIdentifier:@"LaterBookingSegue" sender:self];
}

#pragma mark - menu slider method



- (IBAction)menuButtonPressed:(id)sender
{
    XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
    if (menu.isMenuOpened){
        [menu closeMenuAnimated];
    }
    else{
        [menu openMenuAnimated];
        
    }
    
}


- (NSDateFormatter *)serverformatter {
    
    //EEE - day(eg: Thu)
    //MMM - month (eg: Nov)
    // dd - date (eg 01)
    // z - timeZone
    
    //eg : @"EEE MMM dd HH:mm:ss z yyyy"
    
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"YYYY-MM-dd HH:mm:ss";
    });
    return formatter;
}

- (NSDateFormatter *)formatter {
    
    //EEE - day(eg: Thu)
    //MMM - month (eg: Nov)
    // dd - date (eg 01)
    // z - timeZone
    
    //eg : @"EEE MMM dd HH:mm:ss z yyyy"
    
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"dd MMM HH:mm";
    });
    return formatter;
}

-(IBAction)driverOfflineOnline:(UISwitch *)sender
{
    VNHPubNubWrapper * delegate = [VNHPubNubWrapper sharedInstance];
    if (sender.on) {
        [self sendRequestToChagneDriverStatus:KDriverStatusOnline];
        [self.availableLabel setText:NSLocalizedString(@"ON THE JOB", @"ON THE JOB")];
        // [self.onTheJobButtonOutlet setImage:[UIImage imageNamed:@"on_the_job_inactive.png"] forState:
        [self.onTheJobButtonOutlet setBackgroundImage:[UIImage imageNamed:@"on_the_job_active.png"] forState:UIControlStateNormal];
        [self.offTheJobButtonOutlet setBackgroundImage:[UIImage imageNamed:@"off_the_job_inactive.png"] forState:UIControlStateNormal];
        [self.onTheJobButtonOutlet setEnabled:NO];
        [self.offTheJobButtonOutlet setEnabled:YES];
        [delegate subscribeToMyChannel];
    }
    else
    {
        [self sendRequestToChagneDriverStatus:kDriverStatusOffline];
        [self.availableLabel setText:NSLocalizedString(@"OFF THE JOB", @"OFF THE JOB")];
        [self.onTheJobButtonOutlet setBackgroundImage:[UIImage imageNamed:@"on_the_job_inactive.png"] forState:UIControlStateNormal];
        [self.offTheJobButtonOutlet setBackgroundImage:[UIImage imageNamed:@"roadyo_reject_btn_off.png"] forState:UIControlStateNormal];
        [self.onTheJobButtonOutlet setEnabled:YES];
        [self.offTheJobButtonOutlet setEnabled:NO];
        CLLocationManager *locationManager = [LocationTracker sharedLocationManager];
        [locationManager stopUpdatingLocation];
        [delegate unsubscribeFromMyChannel];
    }
}


/**
 *  Get current location
 *
 *  @return latitude and longitude of the current location
 */

-(CLLocationCoordinate2D)getLocation
{
    
    if ([CLLocationManager locationServicesEnabled]) {
        
        if (!_locationManager) {
            _locationManager = [[CLLocationManager alloc] init];
            _locationManager.delegate = self;
            if ([_locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
                
                [_locationManager requestAlwaysAuthorization];
            }
            if ([_locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
                [_locationManager requestWhenInUseAuthorization];
            }
            
            if([self.locationManager respondsToSelector:@selector(allowsBackgroundLocationUpdates)]){
                [self.locationManager setAllowsBackgroundLocationUpdates:YES];
            }
            
        }
    }
    _locationManager.delegate = self;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    _locationManager.distanceFilter = 50;
    _locationManager.headingFilter = 1;
    [_locationManager startUpdatingLocation];
    [_locationManager startUpdatingHeading];
    
    CLLocation *location = [_locationManager location];
    CLLocationCoordinate2D coordinate = [location coordinate];
    
    return coordinate;
    
}


-(void)checkNetworkRechability
{
    Reachability *reach = [Reachability reachabilityWithHostName:@"google.com"];
    if (reach.currentReachabilityStatus == ReachableViaWiFi)
    {
        
    }
    else if (reach.currentReachabilityStatus ==ReachableViaWWAN)
    {
    }
    else
    {
    }
}



#pragma mark- User Logout Delegate methods


-(void)userDidLogoutSucessfully:(BOOL)sucess {
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    self.locationTracker = [LocationTracker sharedInstance];
    [self.locationTracker stopLocationTracking];
    [[[XDKAirMenuController sharedMenu] view] removeFromSuperview];
    
    if ([XDKAirMenuController relese]) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                    @"Main" bundle:[NSBundle mainBundle]];
        SplashViewController *splah = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
        UINavigationController *controller = (UINavigationController *)[[[[UIApplication sharedApplication]delegate] window] rootViewController];
        [controller setViewControllers:@[splah] animated:NO];
    }
}
-(void)userDidFailedToLogout:(NSError *)error
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    self.locationTracker = [LocationTracker sharedInstance];
    [self.locationTracker stopLocationTracking];
    
    [[[XDKAirMenuController sharedMenu] view] removeFromSuperview];
    
    if ([XDKAirMenuController relese]) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                    @"Main" bundle:[NSBundle mainBundle]];
        SplashViewController *splah = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
        
        UINavigationController *controller = (UINavigationController *)[[[[UIApplication sharedApplication]delegate] window] rootViewController];
        [controller setViewControllers:@[splah] animated:NO];
    }
}

/**
 *  On the job/ off the job button action
 *
 *  @param sender UIButton
 */
- (IBAction)offTheJobOnTheJobButton:(id)sender {
    if ((int)sender == 50)
    {
        [self.availableLabel setText:NSLocalizedString(@"ON THE JOB", @"ON THE JOB")];
        [self.onTheJobButtonOutlet setBackgroundImage:[UIImage imageNamed:@"on_the_job_active.png"] forState:UIControlStateNormal];
        [self.offTheJobButtonOutlet setBackgroundImage:[UIImage imageNamed:@"off_the_job_inactive.png"] forState:UIControlStateNormal];
        [self.onTheJobButtonOutlet setEnabled:NO];
        [self.offTheJobButtonOutlet setEnabled:YES];
        [self sendRequestToChagneDriverStatus:KDriverStatusOnline];
    }else {
        [self.availableLabel setText:NSLocalizedString(@"OFF THE JOB", @"OFF THE JOB")];
        [self.onTheJobButtonOutlet setBackgroundImage:[UIImage imageNamed:@"on_the_job_inactive.png"] forState:UIControlStateNormal];
        [self.offTheJobButtonOutlet setBackgroundImage:[UIImage imageNamed:@"roadyo_reject_btn_off.png"] forState:UIControlStateNormal];
        [self.onTheJobButtonOutlet setEnabled:YES];
        [self.offTheJobButtonOutlet setEnabled:NO];
        [self sendRequestToChagneDriverStatus:kDriverStatusOffline];
        
    }
    
}


-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    
    XDKAirMenuController *controller = [XDKAirMenuController sharedMenu];
    if (controller.isMenuOpened)
    {
        [controller closeMenuAnimated];
    }
    return YES;
}

/**
 *  Check if location services are enabled or not
 */

-(void)checkLocationServices
{
    if(![CLLocationManager locationServicesEnabled] || [CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorizedAlways)
    {
        LocationServicesViewController *locationVC = [self.storyboard instantiateViewControllerWithIdentifier:@"locationVC"];
        UINavigationController *navigationVC  = [[UINavigationController alloc] initWithRootViewController:locationVC];
        [self.navigationController presentViewController:navigationVC animated:YES completion:nil];
    }
}
/**
 *  Logging out the user
 */

-(void)accountDeactivated {
    
    User *user = [User sharedInstance];
    user.delegate = self;
    [user logout];
}


/**
 *  Details of the booking
 */
-(void)getAppointmentDetails {
    
    
    PMDReachabilityWrapper * reachability = [PMDReachabilityWrapper sharedInstance];
    if ([reachability isNetworkAvailable]) {
        NSDictionary *queryParams= nil;
        NSDictionary * dictP = [[NSUserDefaults standardUserDefaults] objectForKey:@"PUSH"];
        // NSDictionary * dictP = [dict objectForKey:@"aps"];
        queryParams = [NSDictionary dictionaryWithObjectsAndKeys:
                       [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],KDAcheckUserSessionToken,
                       [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],kSMPCommonDevideId,
                       [dictP objectForKey:@"e"],kSMPSignUpEmail,
                       [dictP objectForKey:@"dt"], kSMPRespondBookingDateTime,
                       constkNotificationTypeBookingType,kSMPPassengerUserType,
                       [Helper getCurrentDateTime],kSMPCommonUpDateTime, nil];
        
        NetworkHandler *handler  = [NetworkHandler sharedInstance];
        [[ProgressIndicator sharedInstance] showPIOnView:self.view withMessage:@"Loading.."];
        [handler composeRequestWithMethod:MethodAppointmentDetail paramas:queryParams onComplition:^(BOOL succeeded, NSDictionary *response) {
            [[ProgressIndicator sharedInstance] hideProgressIndicator];
            if (succeeded && [response[@"errFlag"] intValue] == 0) {
                NSLog(@"getAppointment Details>>>>>>>>>>>>>%@",response);
                [[NSUserDefaults standardUserDefaults] setObject:response[@"data"][@"pasChn"] forKey:kNSUPassengerChannelKeyFromAPPTStatus];
                dictpassDetail = [[NSMutableDictionary alloc] initWithDictionary:response[@"data"]];
                [self performSegueWithIdentifier:@"GotoDriverDetailController" sender:self];
            }
        }];
    }
    else
    {
        ProgressIndicator * pi = [ProgressIndicator sharedInstance];
        [pi showMessage:kNetworkErrormessage On:self.view];
    }
}

@end
