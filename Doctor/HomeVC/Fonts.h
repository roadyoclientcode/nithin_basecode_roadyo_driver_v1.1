//
//  Fonts.h
//  Anypic
//
//  Created by rahul Sharma on 20/08/13.
//
//

#import <Foundation/Foundation.h>

@protocol Constant <NSObject>




//Fonts

#define     Robot_Black             @"Roboto-Black"
#define     Robot_Bold              @"Roboto-Bold"
#define     Robot_Light             @"Roboto-Light"
#define     Robot_Medium            @"Roboto-Medium"
#define     Robot_Regular           @"Roboto-Regular"
#define     Robot_Thin              @"Roboto-Thin"
#define     Robot_CondensedLight    @"RobotoCondensed-Light"
#define     Robot_CondensedRegular  @"RobotoCondensed-Regular"
#define     lato_regular_webfont    @"lato-regular-webfont"
#define     lato_bold               @"lato-bold-webfont"
#define     lato_semibold           @"lato-semibold"
#define     lato_light              @"lato-light-webfont"



#define CLEAR_COLOR     [UIColor clearColor]
#define WHITE_COLOR     [UIColor whiteColor]
#define BLACK_COLOR     [UIColor blackColor]
#define GREEN_COLOR     [UIColor greenColor]

@end