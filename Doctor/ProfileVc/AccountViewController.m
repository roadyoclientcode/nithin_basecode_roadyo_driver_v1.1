//
//  AccountViewController.m
//  privMD
//
//  Created by Rahul Sharma on 19/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "AccountViewController.h"
#import "XDKAirMenuController.h"
//#import "RoadyoPubNubWrapper.h"
#import "UIImageView+WebCache.h"
#import "SDWebImageDownloader.h"
#import "ANBlurredImageView.h"
#import "CustomNavigationBar.h"
#import "SplashViewController.h"
#import "profile.h"
#import "HomeViewController.h"
#import "User.h"
#import "LocationTracker.h"


@interface AccountViewController () <CustomNavigationBarDelegate,UITableViewDataSource,UITableViewDelegate, UserDelegate>{
    NSDictionary * profileDict;
}
@property (strong) IBOutlet ANBlurredImageView *imageView;
@property(nonatomic,strong)profile *user;
@property(nonatomic,weak) IBOutlet UITableView *tableView;
@property(nonatomic,strong) UIImageView *profileImage;
@property(nonatomic,strong) UIImage *pickedImage;
@property(nonatomic,assign)BOOL isEditingModeOn;
@end




@implementation AccountViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


#pragma mark - ViewController LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    // self.view.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.7];
    self.navigationController.navigationBarHidden = YES;
    
    _tableView.backgroundColor = UIColorFromRGB(0xe9e9e8);
    self.view.backgroundColor = UIColorFromRGB(0xe9e9e8);
    
    
    
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tapGesture];
    
    
    
    
    // ProfileImageView
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(3, 32, 75, 75)];
    
    _profileImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 75, 75)];
    _profileImage.image = [UIImage imageNamed:@"signup_profile_image"];
    [view addSubview:_profileImage];
    
    UIButton *buttonSelectPicture = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonSelectPicture.frame = CGRectMake(0, 0, 75, 75);
    [buttonSelectPicture addTarget:self action:@selector(profilePicButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    //    [view addSubview:buttonSelectPicture];
    
    [self.tableView addSubview:view];
    
    [self addCustomNavigationBar];
    [self sendServiceForegetProfileData];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}
-(void)viewDidAppear:(BOOL)animated{
    
    //   XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
    //    NSIndexPath *indexpath = [NSIndexPath indexPathForRow:0 inSection:0];
    //    [menu openMenuAnimated];
    //    dispatch_after(0.2, dispatch_get_main_queue(), ^(void){
    //        [menu openViewControllerAtIndexPath:indexpath];
    //    });
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(accountDeactivated) name:@"accountDeactivated" object:nil];
}

-(void)viewDidDisappear:(BOOL)animated
{
    //self.navigationController.navigationBarHidden = YES;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}


#pragma mark - WebService call



-(void)updateProfileResponse:(NSDictionary *)response
{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    
    NSLog(@"response:%@",response);
    if (!response)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error") message:[response objectForKey:NSLocalizedString(@"Message", @"Message")] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
        [alertView show];
        
    }
    else if ([response objectForKey:@"Error"])
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error") Message:[response objectForKey:NSLocalizedString(@"Error", @"Error")]];
        
    }
    else
    {
        
        
        NSDictionary *dictResponse=[response objectForKey:@"ItemsList"];
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
        {
            [[NSUserDefaults standardUserDefaults] setObject:_user.fName forKey:KDAFirstName];
            [[NSUserDefaults standardUserDefaults] setObject:_user.lName forKey:KDALastName];
            [[NSUserDefaults standardUserDefaults] setObject:_user.email forKey:KDAEmail];
            [[NSUserDefaults standardUserDefaults] setObject:_user.mobile forKey:KDAPhoneNo];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
        }
    }
    
}

-(void)sendServiceForegetProfileData
{
    PMDReachabilityWrapper * reachability = [PMDReachabilityWrapper sharedInstance];
    if ([reachability isNetworkAvailable]) {
        [[ProgressIndicator sharedInstance] showPIOnView:self.view withMessage:@"Loading.."];
        
        NetworkHandler * handler = [NetworkHandler sharedInstance];
        NSDictionary *queryParams;
        queryParams = [NSDictionary dictionaryWithObjectsAndKeys:
                       [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],KDAcheckUserSessionToken,
                       [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],kSMPCommonDevideId,
                       [Helper getCurrentDateTime],kSMPCommonUpDateTime, nil];
        
        NSLog(@"param%@",queryParams);
        [handler composeRequestWithMethod:MethodGetMasterProfile
                                  paramas:queryParams onComplition:^(BOOL succeeded, NSDictionary *response) {
                                      if (succeeded) { //handle success response
                                          NSLog(@"Response Profile.<>>>>>>>>>>>>>>>>>>>>>>>>: %@", response);
                                          [self getProfileResponse:response];
                                      }
                                      else{//error
                                          UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error") message:NSLocalizedString(@"Network Error", @"Network Error") delegate:nil cancelButtonTitle:NSLocalizedString(@"Ok", @"Ok") otherButtonTitles:nil, nil];
                                          [alertView show];
                                          [[ProgressIndicator sharedInstance]hideProgressIndicator];
                                          
                                      }
                                  }];
        
    }
    else{
        ProgressIndicator * pi = [ProgressIndicator sharedInstance];
        [pi showMessage:kNetworkErrormessage On:self.view];
    }
}

-(void)getProfileResponse:(NSDictionary *)response
{
    
    ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
    [progressIndicator hideProgressIndicator];
    
    if ([response[@"errFlag"] intValue] ==0) {
        
        profileDict = response[@"data"];
        
        NSString *strImageUrl = [NSString stringWithFormat:@"%@%@",baseUrlForOriginalImage,profileDict[@"pPic"]];
        
        [_profileImage setImageWithURL:[NSURL URLWithString:strImageUrl]
                      placeholderImage:[UIImage imageNamed:@"doctor_image_thumbnail.png"]
                             completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
                             }];
        [self.tableView reloadData];
    }
    else if ([response[@"errFlag"] intValue] == 1)
    {
        if ([response[@"errNum"] intValue] == 7) {
            // UIWindow *window = [[[UIApplication sharedApplication]delegate] window];
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Your session has expired", @"Your session has expired")];
            User *logout = [User sharedInstance];
            logout.delegate = self;
            [logout logout];
        }
        else if ([response[@"errNum"] intValue] == 96 || [response[@"errNum"] intValue] == 94) {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response[@"errMsg"]];
            User *logout = [User sharedInstance];
            logout.delegate = self;
            [logout logout];
        }
        else {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response[@"errMsg"]];
        }
    }
    else{
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response[@"errMsg"]];
    }
}



-(void)dismissKeyboard
{
    //[resignFirstResponder];
    //[passwordloginText resignFirstResponder];
    
    [self.view endEditing:YES];
    
    
}
#pragma mark ButtonAction Methods -

- (void)menuButtonPressedAccount
{
    XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
    
    if (menu.isMenuOpened)
        [menu closeMenuAnimated];
    else
        [menu openMenuAnimated];
}

- (void)setSelectedButtonByIndex:(NSInteger)index
{
    
}
#pragma mark Custom Methods -

- (void) addCustomNavigationBar
{
    CustomNavigationBar *customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, 320, 64)];
    customNavigationBarView.tag = 78;
    customNavigationBarView.delegate = self;
    [customNavigationBarView setTitle:NSLocalizedString(@"PROFILE", @"PROFILE")];
    [self.view addSubview:customNavigationBarView];
    
}

-(void)leftBarButtonClicked:(UIButton *)sender{
    [self menuButtonPressedAccount];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    SplashViewController *SVC = [segue destinationViewController];
    
    
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
}
- (IBAction)profilePicButtonClicked:(id)sender
{
    
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Edit Picture", @"Edit Picture") delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", @"Cancel") destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Take Photo", @"Take Photo"),NSLocalizedString(@"Choose From Library", @"Choose From Library"), nil];
    actionSheet.tag = 1;
    [actionSheet showInView:self.view];
}

- (IBAction)passwordButtonClicked:(id)sender
{
    NSString *pleaseChange = [NSString stringWithFormat:@"Please visit our website"];
    NSString *changePassword = NSLocalizedString(@"to change your password.", @"to change your password.");
    [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:[NSString stringWithFormat:@"%@ %@ %@",pleaseChange,APP_WEBSITE,changePassword]];
    
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag == 1)
    {
        switch (buttonIndex)
        {
            case 0:
            {
                [self cameraButtonClicked:nil];
                break;
            }
            case 1:
            {
                [self libraryButtonClicked:nil];
                break;
            }
            default:
                break;
        }
    }
}


-(void)cameraButtonClicked:(id)sender
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc]init];
    imagePicker.delegate =self;
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.allowsEditing = YES;
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Message", @"Message") message: NSLocalizedString(@"Camera is not available", @"Camera is not available") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil, nil];
        [alert show];
        
    }
}
-(void)libraryButtonClicked:(id)sender
{
    UIImagePickerController * picker = [[UIImagePickerController alloc] init];
    picker.delegate =self;
    picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    picker.allowsEditing = YES;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        // popover = [[UIPopoverController alloc] initWithContentViewController:picker];
        
        
        
        //[popover presentPopoverFromBarButtonItem:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    } else {
        
        [self presentViewController:picker animated:YES completion:nil];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
    [progressIndicator showPIOnView:self.view withMessage:NSLocalizedString(@"Uploading Profile Pic...", @"Uploading Profile Pic...")];
    //  _flagCheckSnap = YES;
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    NSLog(@"Image Info : %@",info);
    
    _pickedImage = [info objectForKey:UIImagePickerControllerEditedImage];
    
    
    UploadFiles *upload = [[UploadFiles alloc] init];
    upload.delegate = self;
    [upload uploadImageFile:_pickedImage];
}

#pragma UploadFileDelegate

-(void)uploadFile:(UploadFiles *)uploadfile didUploadSuccessfullyWithUrl:(NSArray *)imageUrls
{
    ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
    [progressIndicator hideProgressIndicator];
    _profileImage.image = _pickedImage;
    
}
-(void)uploadFile:(UploadFiles *)uploadfile didFailedWithError:(NSError *)error{
    NSLog(@"upload file  error %@",[error localizedDescription]);
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
}


#pragma mark - UITextFeildDelegate

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (_isEditingModeOn) {
        if (textField.tag >= 200 && textField.tag < 204) {
            return YES;
        }
        return NO;
    }
    return NO;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField.tag == 200) {
        _user.fName = textField.text;
    }
    else if (textField.tag == 201) {
        _user.lName = textField.text;
        
    }
    else if (textField.tag == 202) {
        _user.email = textField.text;
    }
    else if (textField.tag == 203) {
        _user.mobile = textField.text;
    }
    
}
- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return YES;
}
-(void)moveViewUpToPoint:(int)point
{
    CGRect rect = self.view.frame;
    rect.origin.y = point;
    [UIView animateWithDuration:0.4
                     animations:^(void){
                         self.view.frame = rect;
                     }
                     completion:^(BOOL finished){
                     }];
}

-(void)moveViewDown
{
    CGRect rect = self.view.frame;
    rect.origin.y = 0;
    
    
    [UIView animateWithDuration:0.4
                     animations:^(void){
                         self.view.frame = rect;
                     }
                     completion:^(BOOL finished){
                     }];
}


#pragma mark -
#pragma mark UITableViewDelegate Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (section == 0) {
        return 6;
    }
    else if(section == 1){
        return 6;
    }
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = nil;//[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        
        
        UIImageView *line = [[UIImageView alloc] initWithFrame:CGRectMake(0, 39, 320, 1)];
        line.image = [UIImage imageNamed:@"signup_bg_namedetails_divider"];
        line.tag = 200;
        [cell.contentView addSubview:line];
        
    }
    
    UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(105, 5, 200, 30)];
    textField.borderStyle = UITextBorderStyleNone;
    textField.font = [UIFont fontWithName:Robot_CondensedRegular size:15];
    //textField.placeholder = @"enter text";
    textField.autocorrectionType = UITextAutocorrectionTypeNo;
    textField.keyboardType = UIKeyboardTypeDefault;
    textField.returnKeyType = UIReturnKeyDone;
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    textField.delegate = self;
    [textField setTintColor:[UIColor blueColor]];
    [cell.contentView addSubview:textField];
    
    
    
    UILabel *labelHelperText = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, 100, 30)];
    [Helper setToLabel:labelHelperText Text:@"" WithFont:Robot_CondensedLight FSize:13 Color:BLACK_COLOR];
    [cell.contentView addSubview:labelHelperText];
    
    
    
    
    if (indexPath.section == 0) {
        
        if (indexPath.row == 0) {
            
            UIImageView *lineImageView = (UIImageView*)[cell.contentView viewWithTag:200];
            lineImageView.frame = CGRectMake(100, 39, 220, 1);
            textField.text = flStrForStr(profileDict[@"fName"]);
            textField.tag = 200;
        }
        if (indexPath.row == 1) {
            
            
            textField.tag = 201;
            textField.text = flStrForStr(profileDict[@"lName"]);
        }
        if (indexPath.row == 2) {
            
            textField.tag = 202;
            labelHelperText.text = NSLocalizedString(@"Email:", @"Email:");
            textField.text = flStrForStr(profileDict[@"email"]);
            
        }
        if (indexPath.row == 3) {
            
            textField.tag = 203;
            labelHelperText.text = NSLocalizedString(@"Mobile:", @"Mobile:");
            textField.text = flStrForStr(profileDict[@"mobile"]);
        }
        if (indexPath.row == 4) {
            
            labelHelperText.text = NSLocalizedString(@"Driver License:", @"Driver License:");
            textField.text = flStrForStr(profileDict[@"licNo"]);
        }
        if (indexPath.row == 5) {
            labelHelperText.text = NSLocalizedString(@"Expiry Date:", @"Expiry Date:");
            textField.text = flStrForStr(profileDict[@"licExp"]);
        }
    }
    else if (indexPath.section == 1){
        
        if (indexPath.row == 0) {
            labelHelperText.text = NSLocalizedString(@"Car Make:", @"Car Make:");
            textField.text = flStrForStr(profileDict[@"vehMake"]);
        }
        if (indexPath.row == 1) {
            labelHelperText.text = NSLocalizedString(@"Car Type:", @"Car Type:");
            
            textField.text = flStrForStr(profileDict[@"vehicleType"]);
        }
        if (indexPath.row == 2) {
            labelHelperText.text = NSLocalizedString(@"Seating Capacity:", @"Seating Capacity:");
            
            textField.text = [NSString stringWithFormat:@"%ld",(long)[flStrForStr(profileDict[@"seatCapacity"])integerValue]];
        }
        if (indexPath.row == 3) {
            labelHelperText.text = NSLocalizedString(@"Licence Plate:", @"Licence Plate:");
            textField.text = flStrForStr(profileDict[@"licPlateNum"]);
        }
        if (indexPath.row == 4) {
            labelHelperText.text = NSLocalizedString(@"Insurance", @"Insurance");
            textField.text = flStrForStr(profileDict[@"vehicleInsuranceNum"]);
        }
        if (indexPath.row == 5) {
            labelHelperText.text = NSLocalizedString(@"Expiry Date:", @"Expiry Date:");
            textField.text = flStrForStr(profileDict[@"vehicleInsuranceExp"]);
        }
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{

}



#pragma mark - UITableViewDataSource Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell* tableViewCell = [tableView cellForRowAtIndexPath:indexPath];
    [tableViewCell setSelected:NO animated:YES];
    
    
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 30)];
    
    UIImageView *icon = [[UIImageView alloc] initWithFrame:CGRectMake(5, 7.5, 15, 15)];
    
    [headerView addSubview:icon];
    
    UILabel *labelTitle = [[UILabel alloc] initWithFrame:CGRectMake(25, 0, 200, 30)];
    [Helper setToLabel:labelTitle Text:@"" WithFont:Robot_Regular FSize:13 Color:BLACK_COLOR];
    [headerView addSubview:labelTitle];
    
    if (section == 0) {
        icon.image = [UIImage imageNamed:@"signup_driver_icon"];
        labelTitle.text = NSLocalizedString(@"DRIVER DETAILS", @"DRIVER DETAILS");
    }
    else if(section == 1){
        icon.image = [UIImage imageNamed:@"signup_car_icon"];
        labelTitle.text = NSLocalizedString(@"CAR DETAILS", @"CAR DETAILS");
    }
    else if(section == 2){
        icon.image = [UIImage imageNamed:@"signup_car_icon"];
        
        labelTitle.text = NSLocalizedString(@"STATISTICS", @"STATISTICS");
    }
    return headerView;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 30;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40;
}
-(void)userDidLogoutSucessfully:(BOOL)sucess {
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    [[LocationTracker sharedInstance] stopLocationTracking];
    [[[XDKAirMenuController sharedMenu] view] removeFromSuperview];
    
    if ([XDKAirMenuController relese]) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                    @"Main" bundle:[NSBundle mainBundle]];
        
        SplashViewController *splah = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
        
        self.navigationController.viewControllers = [NSArray arrayWithObjects:splah, nil];
    }
}
-(void)userDidFailedToLogout:(NSError *)error {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    [[LocationTracker sharedInstance] stopLocationTracking];
    [[[XDKAirMenuController sharedMenu] view] removeFromSuperview];
    
    if ([XDKAirMenuController relese]) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                    @"Main" bundle:[NSBundle mainBundle]];
        
        SplashViewController *splah = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
        
        self.navigationController.viewControllers = [NSArray arrayWithObjects:splah, nil];
    }
}

-(void)accountDeactivated {
    
    User *user = [User sharedInstance];
    user.delegate = self;
    [user logout];
}

@end
